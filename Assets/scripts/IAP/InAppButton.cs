﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class InAppButton : MonoBehaviour {

    #region PRIVATE VARIABLES

    IAPButton iapButton;
    Button button;

    #endregion

	// Use this for initialization
	void Start () {

        // Get reference
        iapButton = GetComponent<IAPButton>();

        iapButton.onPurchaseComplete.AddListener(MechShopCredits.ins.Purchase);
        iapButton.onPurchaseFailed.AddListener(MechShopCredits.ins.PurchaseFailed);
	}
	
	// Update is called once per frame	
}
