﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parts : MonoBehaviour {

		[SerializeField]
		int level;
		public int durability;

		public virtual int GetLevel()
		{
				return level;
		}
}
