﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // Load data
        print(gameObject.name);
       // GetComponent<MechDataController>().Load();

        // call leaderboard authentication
#if UNITY_ANDROID
        LeaderboardManager.InitilializeGoogleLeadeboard();
#endif
        LeaderboardManager.AuthenticateSocial();
       

        Invoke("Menu", 1f);
        
    }

   

    void Menu()
    {
       LoadingProgress.ins.LoadOperation(UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Menu"));
    }
}
