﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class Profile : MonoBehaviour {

    public static Profile Instance;

    [SerializeField]
    TextMeshProUGUI txtProfile;

    private string playerProfile;

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () 
    {
        uint min, sec;
        min = DataController.ins.PlayerData.playerData.longestTime / 60;
        sec = DataController.ins.PlayerData.playerData.longestTime % 60;

        playerProfile = "<align=center><size=200%><color=#FEB300FF>" + DataController.ins.PlayerData.playerData.playerName + "</color></size></align>\n\n" +

            "<align=center><size=125%><color=#FEB300FF>Mech Stats</color></size></align>\n\n" +

            "Mech(s) Owned: <color=#6CFD00FF>" + DataController.ins.MechData.mechList.Count + "</color></line-indent>\n" +
            "Best Mech (Upgrade):\n <color=#6CFD00FF>" + GetBestLevelMech() + "</color>\n\n" +

            "<align=center><size=125%><color=#FEB300FF>Quick Game</color></size></align>\n\n" +

            "Highest Gold Earned: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.quickHighestGoldEarned + "</color>\n" +
            "Number of Mechs Assembled: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.quickMechAssembled + "</color>\n" +
            "Highest number of Mech: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.quickNumberOfMech + "</color>\n\n" +

            "<align=center><size=125%><color=#FEB300FF>Endless Game</color></size></align>\n\n" +

            "Highest Gold Earned: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.endlessHighestGoldEarned + "</color>\n" +
            "Number of Mechs Assembled: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.endlessMechAssembled + "</color>\n" +
            "Highest number of Mech: <color=#6CFD00FF>" + DataController.ins.PlayerData.playerData.endlessNumberOfMech + "</color>\n" +
            "Longest Time: <color=#6CFD00FF>" + min + "m" + sec + "s" + "</color>";
        
        txtProfile.text = playerProfile;
	}

    public string GetBestLevelMech()
    {
        //List<Mech> _lstMech = DataController.ins.MechData.mechList;
        //List<string> lstMechName = new List<string>();

        //for (int i=0; i< _lstMech.Count; i++)
        //{
        //    lstMechName.Add(_lstMech[i].mechName);
        //}

        //for (int a = 0; a < lstMechName.Count; a++)
        //{
        //    for (int b = 0; b < lstMechName.Count; b++)
        //    {
        //        string _temp = lstMechName[a];
        //        if (_lstMech[a].level < _lstMech[b].level)
        //        {
        //            lstMechName[a] = lstMechName[b];
        //            lstMechName[b] = _temp;
        //        }
        //    }
        //}

        //GetLevel();
        //return lstMechName[0];
        Mech mech = DataController.ins.MechData.mechList.First(_mech => _mech.level == DataController.ins.MechData.mechList.Max(_x => _x.level));
        return mech.mechName;

    }

    //public void GetLevel()
    //{
    //    //var results = DataController.ins.MechData.mechList.Where(n => n.level >= 1).Select(n => n);
    //    ////var results =
    //    ////    from n in DataController.ins.MechData.mechList
    //    ////    where n.level >= 1
    //    ////    select n;
    //    //byte result = DataController.ins.MechData.mechList.Max(n => n.level);

    //    Mech mech = DataController.ins.MechData.mechList.First(_mech => _mech.level == DataController.ins.MechData.mechList.Max(_x => _x.level));
    //    Debug.Log(mech.mechName);
    //    //foreach (Mech m in result)
    //    //{
    //    //    print(m);
    //    //}
    //}

    #region METHODS

    public void Close()
    {
        SoundManager.Instance.PlaySFX("seTap");
        Destroy(gameObject);
    }

    #endregion
}
