﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class HangarItem : MonoBehaviour {

    public int mechId;
    public TextMeshProUGUI txtMechName;
    public Image mechImage;

    public void View(MechStore m)
    {
        HangarManager.ins.ViewMech(m);
    }

    public void View()
    {
        SoundManager.Instance.PlaySFX("seMech");

        if(!DataController.ins.MechData.mechList.Exists(mech => mech.id == mechId) && mechId == 2)
        {            
            MessageSystem.ins.Show("Hangar", "Finish the tutorial to obtain Gareth.\nYou may repeat the tutorial in the Settings Menu", false, null, null);
        }
        else
        {
            HangarManager.ins.ViewMech(HangarManager.ins.mechStore.mechItems[mechId]);
        }
    }
}
