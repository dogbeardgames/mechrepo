﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HangarManager : MonoBehaviour {
    public static HangarManager ins;
    [SerializeField]
    ScrollRect scrollView;

    [SerializeField]
    GameObject item;

    public GameObject mechView;

    public MechScriptable mechStore;

    void Awake()
    {
        ins = this;
    }

    void Start()
    {
        HangarItems();
    }

    #region METHODS

    public void LoadHangarItem()
    {
        HangarItem[] hangarItems = scrollView.content.GetComponentsInChildren<HangarItem>();
        // detach children
        scrollView.content.DetachChildren();
        // delete children
        for (int i = 0; i < hangarItems.Length; i++)
        {
            Destroy(hangarItems[i].gameObject);
        }

        HangarItems();
    }

    #endregion

    public void ViewMech(MechStore m)
    {
        //HangarMechView
        //SoundManager.Instance.PlaySFX("seTap");
        mechView.GetComponent<HangarMechView>().LoadInfo(m);
        mechView.SetActive(true);
    }

    public void Back()
    {
        SoundManager.Instance.PlaySFX("seTap");
        Destroy(gameObject);
    }

    public void Back(GameObject obj)
    {
        SoundManager.Instance.PlaySFX("seTap");
        Destroy(obj);
    }

    #region Private Methods

    private void HangarItems()
    {
        for (int i = 0; i < mechStore.mechItems.Length; i++)
        {
            GameObject o = (GameObject)Instantiate(item, scrollView.content);
            o.name = "HangarItem";
            o.GetComponent<HangarItem>().mechId = mechStore.mechItems[i].id;
            o.GetComponent<HangarItem>().txtMechName.text = mechStore.mechItems[i].mechName;
            o.GetComponent<HangarItem>().mechImage.sprite = mechStore.mechItems[i].mechIcon;
            // not yet bought
            if(!DataController.ins.MechData.mechList.Exists(mechitemCollection => mechitemCollection.id == mechStore.mechItems[i].id))
            {                
                o.GetComponent<HangarItem>().mechImage.color = new Color32(255, 255, 255, 124);
            }
            else
            {
                o.GetComponent<HangarItem>().mechImage.color = new Color32(255, 255, 255, 255);
            }
        }
    }

    #endregion
}
