﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class HangarMechView : MonoBehaviour {
   
    [SerializeField]
    ScrollRect scrollView;

    [SerializeField]
    GameObject item;

    [SerializeField]
    TMPro.TextMeshProUGUI mechInfo, mechName, mechLevel;

    [SerializeField]
    Button btnBuyOrUpgrade;

    [SerializeField]
    Image mechFullImage;

    // computed upgrade cost based in level
    [SerializeField]
    uint upgradeCost;

    private bool isOwned = false;

    void Start()
    {
//        for (int i = 0; i < 10; i++)
//        {
//            GameObject o = (GameObject)Instantiate(item, scrollView.content);
//        }
    }

    #region METHODS

    MechStore mechStore;
    Mech currentMech;

    public void LoadInfo(MechStore m)
    {        
        mechStore = m;

        Debug.Log("Hangar Mech level " + m.level);

        string caption = "";

        if(DataController.ins.MechData.mechList.Exists(mech => mech.id == m.id))
        {            
            isOwned = true;
            // mech in mech collection
            currentMech = DataController.ins.MechData.mechList.First(mech => mech.id == m.id);

            // upgrade cost
            upgradeCost = ((uint)currentMech.level + 1) * m.upgradeCost;

            // info
//            caption = "Stats\nBuild Reward\nQuick Mode: " + currentMech.buildRewardQuick + " (+" + currentMech.goldRewardIncrement + ")\n" +
//                "\nEndless Mode: " + currentMech.buildRewardEndless + " (+" + currentMech.goldRewardIncrement + ")\n" + 
//                "Upgrade cost:" + upgradeCost.ToString("#,###,###") + " gold";

            caption = "Stats:\nBuild Reward: <color=#6CFD00FF> " + currentMech.buildRewardQuick  + "(+" + currentMech.goldRewardIncrement + ")" + "</color>";

            // button bahaviour and caption
            btnBuyOrUpgrade.GetComponentInChildren<TextMeshProUGUI>().text = "Upgrade";
            btnBuyOrUpgrade.onClick.RemoveAllListeners();
            if (currentMech.level <= 9)
            {
                btnBuyOrUpgrade.onClick.AddListener(Upgrade);
                btnBuyOrUpgrade.GetComponent<Button>().interactable = true;
            }
            else
            {
                btnBuyOrUpgrade.GetComponentInChildren<TextMeshProUGUI>().text = "MAX";
                btnBuyOrUpgrade.GetComponent<Button>().interactable = false;
            }

            mechLevel.text = currentMech.level.ToString();
        }
        else
        {
            // info
//            caption = "Stats\nBuild Reward\nQuick Mode: " + m.buildRewardQuick;// +
////                "\nEndless Mode: " + m.buildRewardEndless;

            caption = "Stats:\nBuild Reward: <color=#6CFD00FF> " + m.buildRewardQuick + "</color>";

            // button bahaviour and caption
            btnBuyOrUpgrade.GetComponentInChildren<TextMeshProUGUI>().text = "Go to Workshop";
            btnBuyOrUpgrade.onClick.RemoveAllListeners();
            btnBuyOrUpgrade.onClick.AddListener(OpenStore);

            mechLevel.text = m.level.ToString();
        }

        Image[] mechPart = scrollView.content.GetComponentsInChildren<Image>();

        scrollView.content.DetachChildren();

        for (int i = 0; i < mechPart.Length; i++)
        {            
            Destroy(mechPart[i].gameObject);
        }

        for (int i = 0; i < m.numOfParts; i++)
        {
            GameObject objPart = Instantiate(item, scrollView.content);
            objPart.transform.GetChild(0).GetComponent<Image>().sprite = m.sprtMechParts[i];
            if(i == m.numOfParts - 1)
            {
                // disable blue arrow
                //objPart.transform.GetChild(1).gameObject.SetActive(false);
                objPart.transform.GetChild(1).GetComponent<Image>().enabled = false;
            }
        }                                                               

        mechName.text = m.mechName; //currentMech == null ? m.mechName : currentMech.mechName;
//        mechLevel.text = currentMech == null ? m.level.ToString() : currentMech.level.ToString();
        mechInfo.text = caption;

        mechFullImage.sprite = m.mechImage;
    }

    public void Upgrade()
    {
        // achhievements
        DataController.ins.AchievementsDataManager.TotalUpgrades++;
        DataController.ins.AchievementsDataManager.QualityBuilderChecker(DataController.ins.AchievementsDataManager.TotalUpgrades);
        //

        Debug.Log("Upgrade");
        SoundManager.Instance.PlaySFX("sePurchase");
        UnityEngine.Events.UnityAction hangarUpgradeYes = () =>
            {
                MessageSystem.ins.Show("Hangar", "Mech upgraded!", false, null, null);
//                DataController.ins.PlayerData.playerData.gold -= upgradeCost;
                GameCurrencyEventManager.Instance.ReduceGoldValue(upgradeCost);

                // apply upgrade changes

                currentMech.level += 1;
                currentMech.buildRewardQuick += currentMech.goldRewardIncrement;
                currentMech.buildRewardEndless += currentMech.goldRewardIncrement;

                LoadInfo(mechStore);
            };

//        if (TutorialManager.Instance != null)
//        {
            // not enough gold
            if (upgradeCost > DataController.ins.PlayerData.playerData.gold)
            {
                MessageSystem.ins.Show("Hangar", "Not enough gold!", false, null, null);
            }
            else
            {
                MessageSystem.ins.Show("Hangar", "Upgrade " + mechStore.mechName + " for " + upgradeCost.ToString("#,###,###") + " gold", true, hangarUpgradeYes, null);
            }
//        }

        // update once upgrade is complete
//        MechStore mechStore = HangarManager.ins.mechStore.mechItems[_mechID];
//        LoadInfo(mechStore);
    }

    // called from mech shop
    public void UpdateMechViewItem(int mechId)
    {        
        LoadInfo(HangarManager.ins.mechStore.mechItems[mechId]);
    }

    private void OpenStore()
    {
        SoundManager.Instance.PlaySFX("seTap");
        MenuManager.Instance.SelectStore();
    }

    #endregion
}
