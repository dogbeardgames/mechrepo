﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MechShopUpgrades : Tab {
    
    public GameObject item;

    [SerializeField]
    ScrollRect scrollView;

    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject o = (GameObject)Instantiate(item, scrollView.content);
        }
    }

    public override void Initialize()
    {
        base.Initialize();
    }

    public void TabSelected(bool isSelected)
    {
        SoundManager.Instance.PlaySFX("seTap");
    }

}
