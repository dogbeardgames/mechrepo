﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Item : MonoBehaviour {

    [SerializeField]
    TextMeshProUGUI txtMechName, txtMechInfo;   

    [SerializeField]
    Image mechImage, imgCurrency;

    [SerializeField]
    Button btnBuy;

    public TMPro.TextMeshProUGUI TxtMechName
    {
        get { return txtMechName; }
    }

    public TextMeshProUGUI TxtMechInfo
    {
        get { return txtMechInfo; }   
    }

    public Image MechImage
    {
        get { return mechImage; }
    }
        
    public Image ImgCurrency
    {
        get { return imgCurrency; }
    }

    public Button BtnBuy
    {
        get { return btnBuy; }
    }

    public virtual void View()
    {
        //sa achievement lang to
        SoundManager.Instance.PlaySFX("seZoomIn");
    }

    public virtual void Click()
    {
        //general ?
        SoundManager.Instance.PlaySFX("seTap");
    }

    public virtual void Buy()
    {
        //nararapat para sa item na to...?
        SoundManager.Instance.PlaySFX("sePurchase");
    }

   
}
