﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MechBluePrint : MonoBehaviour {

    public int mechId;


    Button btnMechBluePrinInfo;

    [SerializeField]
    GameObject mechInfoPanel;

	// Use this for initialization
	void Start () {
        btnMechBluePrinInfo = GetComponent<Button>();
        btnMechBluePrinInfo.onClick.AddListener(ShowBluePrintInfo);
	}	

    #region METHODS

    void ShowBluePrintInfo()
    {
        SoundManager.Instance.PlaySFX("seTap");
        Debug.Log("Blue Print Info");
//        GameObject objMechInfoPanel = Instantiate(mechInfoPanel, MechShopManager.Instance.mechShopBluePrint.gameObject.transform);
        GameObject objMechInfoPanel = Instantiate(mechInfoPanel, MechShopManager.Instance.gameObject.transform);
        objMechInfoPanel.GetComponent<SelectedMechUIController>().LoadInfo(MechShopManager.Instance.mechStore.mechItems[mechId]);
        
    }

    #endregion
}
