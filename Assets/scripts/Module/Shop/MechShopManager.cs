﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class MechShopManager : MonoBehaviour{

    public MechShopBluePrints mechShopBluePrint;
    public MechShopCredits mechShopCredits;
//    public MechShopCredits mechShopCredits;

    public MechScriptable mechStore;

    public ShopTransactionManager shopTransactionManager;

    public static MechShopManager Instance;

   

    [SerializeField]
    ToggleGroup toggleGroup;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Debug.Log("Open tab");
       
    }


    void OnEnable()
    {
        //if (DataController.ins.MechData.mechList.Count > 0)
        //{
        //    if(mechShopBluePrint.GetScrollView().content.childCount == 0)
        //        mechShopBluePrint.SpawnComingSoon();

        //    OpenTab(1);
            
        //}
        //else
        //{
        //    OpenTab(0);
        //}
        OpenTab(0);
    }        

    public void OpenTab(int index)
    {        
        if (index == 0)
        {
            toggleGroup.transform.GetChild(0).GetComponent<Toggle>().isOn = true;
            toggleGroup.transform.GetChild(1).GetComponent<Toggle>().isOn = false;
            mechShopBluePrint.TabSelected(true);
            mechShopBluePrint.gameObject.SetActive(true);
            mechShopCredits.gameObject.SetActive(false);
        }
        else
        {
            toggleGroup.transform.GetChild(0).GetComponent<Toggle>().isOn = false;
            toggleGroup.transform.GetChild(1).GetComponent<Toggle>().isOn = true;
            mechShopCredits.gameObject.SetActive(true);
            mechShopBluePrint.gameObject.SetActive(false);
        }
        //toggleGroup.transform.GetChild(index).GetComponent<Toggle>().isOn = true;
      
       
    }

    public void OpenCreditsTab()
    {
        toggleGroup.transform.GetChild(0).GetComponent<Toggle>().isOn = false;
        toggleGroup.transform.GetChild(1).GetComponent<Toggle>().isOn = true;
    }

    public void Back()
    {
        SoundManager.Instance.PlaySFX("seTap");
        gameObject.SetActive(false);
        //Destroy(gameObject);
    }

    public void BuySFX()
    {
        SoundManager.Instance.PlaySFX("sePopup");
    }

    public void Tab(Toggle toggle)
    {
        if(toggle.isOn)
            SoundManager.Instance.PlaySFX("seTap");
    }
}
