﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectedMechUIController : MonoBehaviour {

    [SerializeField]
    Button btnBuy;
    [SerializeField]
    Image mechImage, imgCurrency;
    [SerializeField]
    ScrollRect scrollRect;
    [SerializeField]
    TextMeshProUGUI txtMechName, txtMechInfo, txtMechPrice, txtPieces;
    [SerializeField]
    GameObject mechPart;

    Button button;

	// Use this for initialization
	void Start () 
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Close);
	}

    #region METHODS
    // Load Info
    public void LoadInfo(MechStore mechStore)
    {
        // create mech image
        mechImage.sprite = mechStore.mechIconBluePrint;

        // mech parts image
        for (int i = 0; i < mechStore.sprtMechParts.Length; i++)
        {            
            // create mech parts contents
            GameObject objMechPart = Instantiate(mechPart, scrollRect.content);           
            objMechPart.transform.GetChild(0).GetComponent<Image>().sprite = mechStore.sprtMechParts[i];

            if(i == mechStore.sprtMechParts.Length - 1)
            {
                objMechPart.transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        // text information
//        string caption = mechStore.mechName + "\n" +
//            "Build Reward\n" +
//            "Quick Mode: " + mechStore.buildRewardQuick + "\n" +
//            "Endless Mode: " + mechStore.buildRewardEndless + "\n" +
//            "Pieces: " + mechStore.numOfParts + "\n" +
//            "Price:" + mechStore.Price.ToString("#,###,### ") + mechStore.storeCurrency.ToString();
        string mechName = mechStore.mechName;
        string mechInfo = "Build Reward: <color=#7DFF00FF>" + mechStore.buildRewardQuick + "</color>";
        string mechPrice = mechStore.Price.ToString("#,###,###");
        string mechPieces = "Pieces: <color=#7DFF00FF>" + mechStore.numOfParts + "</color>";        

        // currency image
        imgCurrency.sprite = mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold ? MechShopManager.Instance.mechShopBluePrint.Gold : MechShopManager.Instance.mechShopBluePrint.Credits;

        txtMechName.text = mechName;
        txtMechInfo.text = mechInfo;
        txtMechPrice.text = mechPrice;
        txtPieces.text = mechPieces;

        UnityEngine.Events.UnityAction yesAction = () =>
        {
            MechShopManager.Instance.shopTransactionManager.Buy(mechStore);
            Destroy(gameObject);
            MechShopManager.Instance.mechShopBluePrint.TabSelected(true);

                if(mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold)
                {
//                    DataController.ins.PlayerData.playerData.gold -= mechStore.Price;
                    GameCurrencyEventManager.Instance.ReduceGoldValue(mechStore.Price);
                }
                else if(mechStore.storeCurrency == EnumCollection.StoreCurrency.Credits)
                {
//                    DataController.ins.PlayerData.playerData.credits -= mechStore.Price;
                    GameCurrencyEventManager.Instance.ReduceCreditValue(mechStore.Price);
                }
        };
                
        btnBuy.onClick.RemoveAllListeners();
        btnBuy.onClick.AddListener(() => 
            {
                SoundManager.Instance.PlaySFX("sePurchase");
                if (mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold)
                {
                    if(mechStore.Price > DataController.ins.PlayerData.playerData.gold)
                    {
                        MessageSystem.ins.Show("Workshop", "Not enough gold!", false, null, null);
                    }
                    else
                    {
                        MessageSystem.ins.Show("Workshop", "Buy " + mechStore.mechName + " for " + mechStore.Price + " " + mechStore.storeCurrency.ToString() + "?", true,
                            yesAction, null);

                    }
                }
                else if(mechStore.storeCurrency == EnumCollection.StoreCurrency.Credits)
                {
                    if(mechStore.Price > DataController.ins.PlayerData.playerData.credits)
                    {
                        MessageSystem.ins.Show("Workshop", "Not enough credits!", false, null, null);
                    }
                    else
                    {
                        MessageSystem.ins.Show("Workshop", "Buy " + mechStore.mechName + " for " + mechStore.Price + " " + mechStore.storeCurrency.ToString() + "?", true,
                            yesAction, null);
                    }
                }
            });
    }

    void Close()
    {
        SoundManager.Instance.PlaySFX("seTap");
        Destroy(gameObject);
    }        

    #endregion
}
