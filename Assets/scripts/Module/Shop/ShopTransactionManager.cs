﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopTransactionManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}	

    #region METHODS

    public void Buy(MechStore mechStoreItem)
    {        
        Mech mech = new Mech();
        mech.buildRewardEndless = mechStoreItem.buildRewardEndless;
        mech.buildRewardQuick = mechStoreItem.buildRewardQuick;
        mech.description = mechStoreItem.description;
        mech.goldRewardIncrement = mechStoreItem.goldRewardIncrement;
        mech.id = mechStoreItem.id;
        mech.level = mechStoreItem.level;
        mech.mech = mechStoreItem.mech;
        mech.mechName = mechStoreItem.mechName;
        mech.numOfParts = mechStoreItem.numOfParts;
        mech.upgradeCost = mechStoreItem.upgradeCost;

        DataController.ins.MechData.mechList.Add(mech);
        MessageSystem.ins.Show("Workshop", "Acquired " + mech.mechName, false, null, null);

        // achievements
        DataController.ins.AchievementsDataManager.CollectorChecker(DataController.ins.AchievementsDataManager.MechBluePrints);

        // if hangar is open
        if (GameObject.Find("HangarPanel(Clone)") != null)
        {
            Debug.Log("Hangar is open");
            HangarManager.ins.LoadHangarItem();
            HangarManager.ins.mechView.GetComponent<HangarMechView>().UpdateMechViewItem(mechStoreItem.id);
        }
      
    }

    #endregion
}
