﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MechShopBluePrints : Tab {
    public GameObject item;

    [SerializeField]
    ScrollRect scrollView;
    [SerializeField]
    GameObject btnMoreInfo;

    [SerializeField]
    Sprite gold, credits;
    [SerializeField]
    GameObject comingsoon;
   
	#region MONO



    public ScrollRect GetScrollView()
    {
        return scrollView;
    }

    public Sprite Gold
    {
        get { return gold; }
    }

    public Sprite Credits
    {
        get { return credits; }
    }

    public void TabSelected(bool isSelected)
    {
        SoundManager.Instance.PlaySFX("seTap");

        //destroy
        if (scrollView.content.childCount > 0)
        {
            Item[] items = scrollView.content.GetComponentsInChildren<Item>();
            scrollView.content.DetachChildren();
            for (int i = 0; i < items.Length; i++)
            {
                Destroy(items[i].gameObject);
            }

            print("DESTROY");
        }

        Debug.Log("Selected: " + isSelected);
        
        //spawn new for update
        if (isSelected)
        {            
            MechStore[] mechItems = MechShopManager.Instance.mechStore.mechItems;

            for (int i = 0; i < MechShopManager.Instance.mechStore.mechItems.Length; i++)
            {
                // Check if item is in collection
                if(!DataController.ins.MechData.mechList.Exists(mechitemCollection => mechitemCollection.id == mechItems[i].id) && i != 2)
                {
                    MechStore mechStore = mechItems[i];
                    GameObject o = (GameObject)Instantiate(item, scrollView.content);
                    Item itemMono = o.GetComponent<Item>();

//                    string caption = mechStore.mechName + "\n" +
//                        "Price: " + mechStore.Price.ToString("#,###,###") + " " + mechStore.storeCurrency.ToString(); 

                    string mechName = mechStore.mechName;
                    string mechInfo = mechStore.Price.ToString("#,###,###") + " " + mechStore.storeCurrency.ToString(); 


                    // item currency
                    itemMono.ImgCurrency.sprite = mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold ? gold : credits;

                    // item
                    itemMono.TxtMechName.text = mechName;
                    itemMono.TxtMechInfo.text = mechInfo;
                    itemMono.MechImage.sprite = mechStore.mechIcon;

                    //more info button
                    GameObject objMoreInfoButton = Instantiate(btnMoreInfo, o.transform);
                    objMoreInfoButton.transform.SetAsFirstSibling();
                    objMoreInfoButton.GetComponent<MechBluePrint>().mechId = mechStore.id;

                    UnityEngine.Events.UnityAction yesAction = () =>
                    {
                        MechShopManager.Instance.shopTransactionManager.Buy(mechStore);
                        MechShopManager.Instance.mechShopBluePrint.TabSelected(true);

                        if (mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold)
                        {
//                            DataController.ins.PlayerData.playerData.gold -= mechStore.Price;
                            GameCurrencyEventManager.Instance.ReduceGoldValue(mechStore.Price);
                        }
                        else if (mechStore.storeCurrency == EnumCollection.StoreCurrency.Credits)
                        {
//                            DataController.ins.PlayerData.playerData.credits -= mechStore.Price;
                            GameCurrencyEventManager.Instance.ReduceCreditValue(mechStore.Price);
                        }                                                        
                    };

                    itemMono.GetComponent<Item>().BtnBuy.onClick.AddListener(() =>
                        {
                            if(mechStore.storeCurrency == EnumCollection.StoreCurrency.Gold)
                            {
                                if(mechStore.Price > DataController.ins.PlayerData.playerData.gold)
                                {
                                    MessageSystem.ins.Show("Workshop", "Not enough gold!", false, null, null);
                                }
                                else
                                {
                                    MessageSystem.ins.Show("Workshop", "Buy " + mechStore.mechName + " for " + mechStore.Price + " " + mechStore.storeCurrency.ToString() + "?", true,
                                        yesAction, null);

                                }
                            }
                            else if(mechStore.storeCurrency == EnumCollection.StoreCurrency.Credits)
                            {
                                if(mechStore.Price > DataController.ins.PlayerData.playerData.credits)
                                {
                                    MessageSystem.ins.Show("Workshop", "Not enough credits!", false, null, null);
                                }
                                else
                                {
                                    MessageSystem.ins.Show("Workshop", "Buy " + mechStore.mechName + " for " + mechStore.Price + " " + mechStore.storeCurrency.ToString() + "?", true,
                                        yesAction, null);
                                }
                            }
                        });
                }
            }
        }


        Invoke("Close", 0.1f);

    }


    void Close()
    {
        //  if(!DataController.ins.MechData.mechList.Exists(mechitemCollection => mechitemCollection.id == mechItems[i].id) && i != 2)
        int _cntr = 0;
        for (int i=0; i<DataController.ins.MechData.mechList.Count; i++)
        {

            if (DataController.ins.MechData.mechList[i].id != 2)
            {
                _cntr++;
                print(DataController.ins.MechData.mechList[i].mechName);
            }
            //if (DataController.ins.MechData.mechList.Exists(mechitemCollection => mechitemCollection.id == i) && i != 2)
            //{
              
                
            //    print(DataController.ins.MechData.mechList.Where(m => m.id == i));
            //}
            print(_cntr);
        }
        print("count " + _cntr);
        if (_cntr == 9 )
        {

            if (scrollView.content.childCount == 0)
                MechShopManager.Instance.mechShopBluePrint.SpawnComingSoon();

            MechShopManager.Instance.OpenTab(1);
            print("waaaaaaaaaaaaaaaaaaaaaaa");
        }
    }
    public void SpawnComingSoon()
    {
        Instantiate(comingsoon, scrollView.content);
    }

    #endregion
}
