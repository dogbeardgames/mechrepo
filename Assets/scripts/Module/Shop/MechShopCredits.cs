﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class MechShopCredits : Tab {
    public static MechShopCredits ins;
    public GameObject item;

    [SerializeField]
    ScrollRect scrollView;

    Dictionary<string, uint> productDict;

    void Awake()
    {
        ins = this;
    }

    void Start()
    {
//        for (int i = 0; i < 10; i++)
//        {
//            GameObject o = (GameObject)Instantiate(item, scrollView.content);
//        }

        productDict = new Dictionary<string, uint>();

        productDict.Add("creditpack1", 300);
        productDict.Add("creditpack2", 700);
        productDict.Add("creditpack3", 900);
    }

    public override void Initialize()
    {
        base.Initialize();
    }

    #region IAP

    public void Purchase(Product product)
    {
        if(product != null)
        {
            // add credit
            GameCurrencyEventManager.Instance.AddCreditValue(productDict[product.definition.id]);
            // show message box
            MessageSystem.ins.Show("Credits", string.Format("Acquired {0} credits!", productDict[product.definition.id]), false, null, null);
        }
    }

    public void PurchaseFailed(Product product, PurchaseFailureReason reason)
    {
        Debug.Log("Failes to purchase " + product.definition.id + ", reason: " + reason.ToString());
    }           

    #endregion
}
