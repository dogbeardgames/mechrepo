﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrencyUIManager : MonoBehaviour {

    #region Private Variables

    [SerializeField]
    EnumCollection.StoreCurrency storeCurrency;

    TextMeshProUGUI text;

    #endregion    		

    #region Mono

    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Use this for initialization
    void Start () {

    }   

    void OnEnable()
    {
        if(storeCurrency == EnumCollection.StoreCurrency.Gold)
        {
            GameCurrencyEventManager.OnGoldUpdate += GoldUI;
            GoldUI();
        }
        else
        {
            GameCurrencyEventManager.OnCreditUpdate += CreditUI;
            CreditUI();
        }            
    }

    void OnDisable()
    {
        if(storeCurrency == EnumCollection.StoreCurrency.Gold)
        {
            GameCurrencyEventManager.OnGoldUpdate -= GoldUI;
        }
        else
        {
            GameCurrencyEventManager.OnCreditUpdate -= CreditUI;
        }  
    }

    #endregion

    #region Private Methods

    void GoldUI()
    {
        text.text = DataController.ins.PlayerData.playerData.gold.ToString();
    }

    void CreditUI()
    {
        text.text = DataController.ins.PlayerData.playerData.credits.ToString();
    }

    #endregion
}
