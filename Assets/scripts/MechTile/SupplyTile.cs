﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SupplyTile : Tile {

    RushHour rush = new RushHour();
    SlowTime slow = new SlowTime();
    MagicAssemble magic = new MagicAssemble();
    //GoldMultiplier golder = new GoldMultiplier();
    CheapPart cheap = new CheapPart();
    Repair repair = new Repair();

    SpriteRenderer rndr, boxRndr;
    Sprite defaultSprte;

    bool activated = false;
    sbyte supplyIndex = -1;
    Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
        DontDestroyOnLoad(gameObject);
    }

    void OnEnable()
    {
        activated = false;
        supplyIndex = -1;

        if (rndr == null)
        {
            rndr = transform.GetChild(0).GetComponent<SpriteRenderer>();
            boxRndr = GetComponent<SpriteRenderer>();
            defaultSprte = rndr.sprite;
        }
        else
        {
            boxRndr.color = new Color(1,1,1,1);
            rndr.sprite = defaultSprte;
        }
    }

    public void ResetTrigger()
    {
        anim.SetBool("open",false);
    }

    public override void Move()
    {
        base.Move();
        if (transform.position.y < GameManager.BotScreen() && move)
        {
            //set position to top for reuse
            UI_FX_Manager.ins.SpawnBottomSpark(transform.position);
            transform.position = new Vector2(transform.position.x, GameManager.TopScreen());
            gameObject.SetActive(false);

        }
    }

    void FixedUpdate()
    {
        if (!GameManager.paused || !GameManager.gameOver)
            Move();
    }

    //indexer = rush=0, slow=1, magic=2, repair=3, cheap = 4;
    void Reveal()
    {
        boxRndr.color = new Color(1, 1, 1, 0);
        int rnd = Random.Range(0,100);
        SoundManager.Instance.PlaySFX("seOkPiece");
        if (!SupplyBoxManager.isSpeedBoxActive)
        {
            if (rnd >= 0 && rnd < 20)
            {
                supplyIndex = 0;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(0);
            }
            else if (rnd >= 20 && rnd < 60)
            {
                supplyIndex = 1;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(1);
            }
            else if (rnd >= 60 && rnd < 70)
            {
                supplyIndex = 2;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(2);
            }
            else if (rnd >= 70 && rnd < 80)
            {
                supplyIndex = 3;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(3);
            }
            else
            {
                supplyIndex = 4;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(4);
            }


        }
        else
        {
            if (rnd >= 0 && rnd < 30)
            {
                supplyIndex = 2;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(2);
            }
            else if (rnd >= 30 && rnd < 60)
            {
                supplyIndex = 3;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(3);
            }
            else
            {
                supplyIndex = 4;
                rndr.sprite = SupplyBoxManager.ins.GetSprite(4);
            }

        }
       
    }

    void Activate()
    {
       
     
        switch (supplyIndex) {
            case 0:
                SoundManager.Instance.PlaySFX("seZoomIn");
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.red);
                rush.Activate();
            
                print("FAST");
                break;
            case 1:
                SoundManager.Instance.PlaySFX("seZoomOut");
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
                slow.Activate();
              
                print("SLOW");
                break;
            case 2:
                SoundManager.Instance.PlaySFX("seWildFull");
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
                magic.Activate();
                magic.SpawnScore(transform.position);
                break;
            case 3:
                SoundManager.Instance.PlaySFX("sePurchase");
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
                //repair.FreePassTile(transform);
                repair.Activate();
                repair.SpawnScore(transform.position);
                //golder.Activate();
                break;
            case 4:
                SoundManager.Instance.PlaySFX("seCheap");
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.red);
                cheap.Activate();
                cheap.SpawnScore(transform.position);
                break;
            default: break;
        }
        activated = true;
        supplyIndex = -1;
     
        gameObject.SetActive(false);
    }

    public void OnMouseDown()
    {
        //indexer = rush=0, slow=1, magic=2, repair=3, cheap = 4;
        //// ?: conditional operator.  classify = (input > 0) ? "positive" : "negative";
        if (!GameManager.paused && !GameManager.gameOver && !activated)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (supplyIndex < 0)
                {
                    anim.SetBool("open", true);
                    //Reveal();
                }
                else
                {
                    Activate();
                }
            }
        }

    }

    void Hide()
    {
       
        gameObject.SetActive(false);
    }


    abstract class Supply{
        public virtual void Activate()
        { }
     }

    sealed class RushHour : Supply {

        public override void Activate()
        {
            base.Activate();
            SupplyBoxManager.ins.SpeedUp(); //swiched this might be the cause

        }
       
    }

    sealed class SlowTime : Supply
    {

        public override void Activate()
        {
            base.Activate();
           

            SupplyBoxManager.ins.SlownDown();

        }

    }

   sealed class MagicAssemble : Supply {
        public override void Activate()
        {
            base.Activate();

            PlayerGameModeData.numOfCombo++;
            PlayerGameModeData.numOfMechCompleted++;

            if (PlayerGameModeData.numOfCombo > 1)
            {
                if (PlayerGameModeData.numOfCombo >= PlayerGameModeData.numOfMaxComboMade)
                {
                    PlayerGameModeData.numOfMaxComboMade = PlayerGameModeData.numOfCombo;
                }

                ModeUI.ins.SetComboText(PlayerGameModeData.numOfCombo.ToString() + " build combo");
            }
            ModeUI.ins.BuildComplete();
            MechTapTileManager.ins.DestroyCurrentMech();
            print("Magic!");
        }

        public void SpawnScore(Vector3 pos)
        {
            ModeUI.ins.SetGoldTextIncreased(MechTapTileManager.ins.GetScore(MechTapTileManager.ins.mechToAssemble));
            UI_FX_Manager.ins.SpawnScore(pos, "+" + MechTapTileManager.ins.GetScore(MechTapTileManager.ins.mechToAssemble).ToString(), Color.blue);
        }
    }

    sealed class CheapPart : Supply
    {

        public override void Activate()
        {
            base.Activate();
            // deduct gold in game
            ModeUI.ins.SetGoldTextDescreased(40);
        }

        public void SpawnScore(Vector3 pos)
        {
           
            UI_FX_Manager.ins.SpawnScore(pos,"-40", Color.red);
        }
    }

    sealed class Repair : Supply
    {
        //public void FreePassTile(Transform trans)
        //{
        //    MechTapTileManager.ins.FreePassTiles(trans);
        //}

        public override void Activate()
        {
            base.Activate();
            ModeUI.ins.SetGoldTextIncreased(100);
            
            //MechTapTileManager.ins.SetTappedMechPart(MechTapTileManager.ins.GetMechPartToTap(), MechTapTileManager.ins.mechToAssemble);
        }

        public void SpawnScore(Vector3 pos)
        {
            UI_FX_Manager.ins.SpawnScore(pos, "+100", Color.blue);
        }
    }
}
