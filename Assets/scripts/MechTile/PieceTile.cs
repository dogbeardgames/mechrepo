﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

interface ITappable{
	void Tap ();
}

public class PieceTile : Tile, ITappable {

	public EnumMech mechType;
	public EnumPartType partType;

    public static bool isScoreDeducted = false;

	public bool freePass = false; //if this tile is below or equal to Tapped Part y position this will not be added to missed part list

    static int missPenalty = 50;

    // finger id for touch overlap
    int fingerID = -1;

    void Awake()
    {
        #if !UNITY_EDITOR
        fingerID = 0;
        #endif
    }

	void Start ()
    {
        DontDestroyOnLoad(gameObject);      
    }

	public override void Move ()
	{
		base.Move ();
		if(transform.position.y < GameManager.BotScreen() && move)
		{
            UI_FX_Manager.ins.SpawnBottomSpark(transform.position);

            if (MechTapTileManager.ins.GetMechPartToTap().Equals(partType) && MechTapTileManager.ins.mechToAssemble.Equals(mechType) && !freePass) //free if "part to type" appeared behind the mech to assemble part
            {
                
                #region Deduct Score
                // deduct score
               // Debug.Log("Deduct score");
               // print("part to tap=" + MechTapTileManager.ins.GetMechPartToTap() + " this=" + partType);
                //print("mech to assemble=" + MechTapTileManager.ins.mechToAssemble + " this=" + mechType);
                ModeUI.ins.SetGoldTextDescreased(missPenalty);
                UI_FX_Manager.ins.SpawnScore(transform.position, "-" + missPenalty.ToString(), Color.red);
                PieceTile.isScoreDeducted = true;

                #endregion
				
			}

            //15072017 IF PLAYER MISSED TO TAP THIS TILE
            if (mechType == MechTapTileManager.ins.mechToAssemble && !freePass)
            {
                byte _partType = (byte)partType;
                byte _partToTap = (byte)MechTapTileManager.ins.GetMechPartToTap();

                // tile can only be missed in sequence. "if this tile is less than to tile to tap not missed"
                //if (_partType >= _partToTap && (_partType - MechTapTileManager.ins.currentMissedPartIndex) == 1 ||
                if (_partType >= _partToTap ||
                    _partType.Equals(0) && MechTapTileManager.ins.currentMissedPartIndex.Equals(0))
                {

                    if (!MechTapTileManager.ins.lstMissedPartIndex.Contains(_partType))
                    {
                        MechTapTileManager.ins.currentMissedPartIndex = _partType;

                       // print("MISSED! = " + (byte)partType);

                        //SAVE THE PART TYPE AND MECH TYPE 

                        MechTapTileManager.ins.lstMissedPartIndex.Add(_partType);

                        if (mechType == EnumMech.MECH_0 && _partType == (byte)EnumPartType.PIECE_3) //4
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_1 && _partType == (byte)EnumPartType.PIECE_3) //4
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_2 && _partType == (byte)EnumPartType.PIECE_4) //5
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_3 && _partType == (byte)EnumPartType.PIECE_3) //4
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_4 && _partType == (byte)EnumPartType.PIECE_6) //7
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_5 && _partType == (byte)EnumPartType.PIECE_3) //4
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_6 && _partType == (byte)EnumPartType.PIECE_5) //6
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_7 && _partType == (byte)EnumPartType.PIECE_3) //4
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_8 && _partType == (byte)EnumPartType.PIECE_4) //5
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_9 && _partType == (byte)EnumPartType.PIECE_5) //6
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                    }
                }

            }

            //set position to top for reuse
            //transform.position = new Vector2 (transform.position.x, GameManager.TopScreen());
            gameObject.SetActive (false);
		}
	}

	void FixedUpdate()
	{
        if(!GameManager.paused || !GameManager.gameOver)
		Move();
	}        

	public void Set(EnumMech type, EnumPartType part, Sprite sprt)
	{
		mechType = type;
		partType = part;
		//temporary
		GetComponent<SpriteRenderer>().sprite = sprt;
		transform.GetChild (0).gameObject.SetActive (false);
	}

	public void Tap()
	{
		MechTapTileManager.ins.SetTappedMechPart (partType, mechType, transform.position);
	}

	public void OnMouseDown()
	{                  
        if (!GameManager.paused && !GameManager.gameOver)
        {

            Vector2 vect2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D[] hit2D;
            hit2D = Physics2D.RaycastAll(vect2, Vector3.forward);
            Debug.Log("Hit 2D " + hit2D.Length);

            for (int i = 0; i < hit2D.Length; i++)
            {
                Debug.Log("Hit " + hit2D[i].collider.gameObject.name);
                if(hit2D[i].collider.gameObject.name == "bot" || hit2D[i].collider .gameObject.name == "filler")
                {
                    return;
                }
            }

            MechTapTileManager.ins.FreePassTiles(transform);
         
            if (partType == MechTapTileManager.ins.GetMechPartToTap() && mechType == MechTapTileManager.ins.mechToAssemble)
            {
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
            }
            else
            {
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.red);
            }
            Tap();
            transform.position = new Vector2(transform.position.x, GameManager.TopScreen());
            gameObject.SetActive(false);
        }            
	}        

    void Disable()
    {
        transform.position = new Vector2(transform.position.x, GameManager.TopScreen());
        gameObject.SetActive(false);
        freePass = false;
        PieceTile.isScoreDeducted = false;
    }

    void OnEnable()
	{
		freePass = false;
        PieceTile.isScoreDeducted = false;
	}

	bool IsLastPart()
	{
		return ((mechType == EnumMech.MECH_0 && (int)partType == 3) || (mechType == EnumMech.MECH_1 && (int)partType == 4) || (mechType == EnumMech.MECH_2 && (int)partType == 5)) ? true : false;
	}

	bool Missed()
	{
		//this only missed the first part of mech
		return (mechType == MechTapTileManager.ins.mechToAssemble && partType == MechTapTileManager.ins.GetMechPartToTap()) ? true : false;
	}
}
