﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OreTile : Tile {

    byte tapCount = 0;
    //static int orePenalty = 50;
   

    SpriteRenderer rndr;
    static Sprite defaultSprite;

    void Awake()
    {
        rndr = GetComponent<SpriteRenderer>();
        defaultSprite = rndr.sprite;
    }

    void Start()
    {
      
        DontDestroyOnLoad(gameObject);
    }

    public override void Move()
    {
        base.Move();
        if (transform.position.y < GameManager.BotScreen() && move)
        {
            //if (tapCount <= 1)
            //{
            //    ModeUI.ins.SetGoldTextDescreased(orePenalty);
            //    UI_FX_Manager.ins.SpawnExplosion(transform.position);
            //}
            //else
            //{
            //    UI_FX_Manager.ins.SpawnAcid(transform.position);
            //}
            UI_FX_Manager.ins.SpawnBottomSpark(transform.position);
            tapCount = 0;
            gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        tapCount = 0;
        rndr.sprite = defaultSprite;
    }

    void FixedUpdate()
    {
        if (!GameManager.paused || !GameManager.gameOver)
            Move();
    }

    public void OnMouseDown()
    {
        if (!GameManager.paused && !GameManager.gameOver)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                tapCount++;
                

                if (tapCount >= 2)
                {
                    SoundManager.Instance.PlaySFX("seReinforce");
                    SpecialTilesManager.isGoldMultiplierActive = true;
                    UI_FX_Manager.ins.SpawnSelect(transform.position, Color.yellow);
                    tapCount = 0;
                    gameObject.SetActive(false);

                    // achievements
                    DataController.ins.AchievementsDataManager.TitaniumOres++;
                    DataController.ins.AchievementsDataManager.MinerChecker(DataController.ins.AchievementsDataManager.TitaniumOres);
                }
                else if (tapCount == 1)
                {
                    UI_FX_Manager.ins.SpawnOreSpark(transform.position);
                    SoundManager.Instance.PlaySFX("seOre");
                    rndr.sprite = MechTapTileManager.ins.GetSpawner().SpecialTilesManager().GetRefinedOre();
                    ModeUI.ins.SetGoldTextIncreased(50);
                    UI_FX_Manager.ins.SpawnScore(transform.position, "+50", Color.blue);
                }
                
            }
        }

    }
}
