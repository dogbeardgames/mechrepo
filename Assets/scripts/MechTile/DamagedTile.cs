﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IRepair {
    void Repair();
}
public class DamagedTile :Tile, IRepair {

	SpriteRenderer rndr;
	[SerializeField]
	byte durability = 0;
    Color[] clrs = new Color[5] 
    {
        new Color(0.2f,0.2f,0.2f,1f),
        new Color(0.4f,0.4f,0.4f,1f),
        new Color(0.6f,0.6f,0.6f,1f),
        new Color(0.8f,0.8f,0.8f,1f),
        new Color(1f,1f,1f,1f)
    };
    byte clrIndex;

	void Awake()
	{
		rndr = GetComponent<SpriteRenderer> ();
	}
	void Start()
	{
		durability = 0;
        rndr.color = clrs[0];
    }

	public void Repair()
	{
		durability++;

        //repaired!
		//if (durability >= GameManager.GetPartMaxDurability()) {

  //          MechTapTileManager.ins.FreePassTiles(transform);
         
  //          ModeUI.ins.SetGoldTextInreased(GameManager.GetRepairReward());
		//	UI_FX_Manager.ins.SpawnAcid (transform.position);
		//	//transform.position = new Vector2 (transform.position.x, GameManager.TopScreen());
		//	gameObject.SetActive (false);
  //          MechTapTileManager.ins.SetTappedMechPart(MechTapTileManager.ins.GetMechPartToTap(), MechTapTileManager.ins.mechToAssemble);
  //      }
	}
		
	public override void Move ()
	{
		base.Move ();
        if (transform.position.y < GameManager.BotScreen() && move) {
           // ModeUI.ins.SetGoldTextDescreased(GameManager.GetFailRepairPenalty());
            UI_FX_Manager.ins.SpawnExplosion(transform.position);
            transform.position = new Vector2 (transform.position.x, GameManager.TopScreen());
           
            gameObject.SetActive (false);
		}
	}
	void FixedUpdate()
	{
        if (!GameManager.paused || !GameManager.gameOver)
            Move();
    }

	public void OnMouseDown()
	{
        if (!GameManager.paused && !GameManager.gameOver)
        {
            Repair();
            if (clrIndex++ < clrs.Length)
                rndr.color = clrs[clrIndex];
        }
          
	}

    void OnDisable()
    {
        rndr.color = clrs[0];
        clrIndex = 0;
        durability = 0;
    }
	
	void OnEnable()
	{
        rndr.color = clrs[0];
        clrIndex = 0;
        durability = 0;
    }

	void SetColor(float r, float g, float b, float a)
	{
		rndr.color = new Color (r, g, b, a);
	}
}
