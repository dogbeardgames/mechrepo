﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

interface IBomb {
    void Explode();
    void Defuse();
}

public class BombTile : Tile, IBomb {
    
    byte numOfTaps = 3;

    byte tempNumOfTaps;

    SpriteRenderer rndr;

    public static bool isScoreDeducted = false;

   static Color[] clrs = new Color[5]
   {
        new Color(1f,1f,1f,1f),
        new Color(0.8f,0.8f,0.8f,1f),
        new Color(0.6f,0.6f,0.6f,1f),
        new Color(0.4f,0.4f,0.4f,1f),
        new Color(0.2f,0.2f,0.2f,1f)
   };
    byte clrIndex;


    void Awake()
    {
        rndr = GetComponent<SpriteRenderer>();
        tempNumOfTaps = numOfTaps;
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
 
    public override void Move()
    {
        base.Move();
        if (transform.position.y < GameManager.BotScreen() && move)
        {
            //set position to top for reuse
            SoundManager.Instance.PlaySFX("seExplode");
            Explode();
            //transform.position = new Vector2(transform.position.x, GameManager.TopScreen());
            gameObject.SetActive(false);

        }
    }

    void FixedUpdate()
    {
        if (!GameManager.paused || !GameManager.gameOver)
            Move();
    }

    public void Explode()
    {

        #region Deduct score

        ModeUI.ins.SetGoldTextDescreased(150);
        UI_FX_Manager.ins.SpawnScore(transform.position, "-150", Color.red);
        BombTile.isScoreDeducted = true;

        #endregion

        //
        MechTapTileManager.ins.Reset();
       
        MechTapTileManager.ins.GetSpawner().Bomb();
        if (GameManager.mode == GAME_MODE.ENDLESS)
        {
            ModeUI.ins.SetLifeTextDecreased(1);
        }
        ModeUI.ins.BuildDestroy();

        foreach (GameObject tile in MechTapTileManager.ins.GetSpawner().GetTilePooler().objLst)
        {
            if (tile.activeInHierarchy)
            {
                UI_FX_Manager.ins.SpawnExplosion(tile.transform.position);
                tile.gameObject.SetActive(false);
            }
        }

        foreach (GameObject tile in MechTapTileManager.ins.GetSpawner().SpecialTilesManager().GetBombPooler().objLst)
        {
            if (tile.activeInHierarchy)
            {
                UI_FX_Manager.ins.SpawnExplosion(tile.transform.position);
                tile.gameObject.SetActive(false);
            }
        }

        foreach (GameObject tile in MechTapTileManager.ins.GetSpawner().SpecialTilesManager().GetOrePooler().objLst)
        {
            if (tile.activeInHierarchy)
            {
                UI_FX_Manager.ins.SpawnExplosion(tile.transform.position);
                tile.gameObject.SetActive(false);
            }
        }

        foreach (GameObject tile in MechTapTileManager.ins.GetSpawner().SupplyBoxManager().GetSupplyPooler().objLst)
        {
            if (tile.activeInHierarchy)
            {
                UI_FX_Manager.ins.SpawnExplosion(tile.transform.position);
                tile.gameObject.SetActive(false);
            }
        }


        
        UI_FX_Manager.ins.SpawnExplosion(transform.position);
    }

    public void Defuse()
    {
        ModeUI.ins.SetGoldTextIncreased(250);
        UI_FX_Manager.ins.SpawnScore(transform.position, "+250", Color.blue);
        gameObject.SetActive(false);

        // achievements
        DataController.ins.AchievementsDataManager.DefusedTimeBombs++;
        DataController.ins.AchievementsDataManager.ExplosivesExpertChecker(DataController.ins.AchievementsDataManager.DefusedTimeBombs);
    }
    public void OnMouseDown()
    {
        if (!GameManager.paused && !GameManager.gameOver)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                numOfTaps--;
                UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
                if (numOfTaps <= 0)
                {
                    Defuse();
                    SoundManager.Instance.PlaySFX("seDefused");
                }
                else
                {
                    SoundManager.Instance.PlaySFX("seDefuse");
                }
                if (clrIndex++ < clrs.Length - 1)
                    rndr.color = clrs[clrIndex];
            }
        }
       
    }

   

    void OnEnable()
    {
        numOfTaps = tempNumOfTaps;
        rndr.color = clrs[0];
        clrIndex = 0;
        BombTile.isScoreDeducted = false;
    }

    void SetColor(float r, float g, float b, float a)
    {
        rndr.color = new Color(r, g, b, a);
    }
}
