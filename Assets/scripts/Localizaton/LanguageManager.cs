﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LanguageManager : MonoBehaviour {

    #region Public Variable

    public static LanguageManager Instance;

    public Language language;

    #endregion

    #region MonoBehaviour

    void Awake()
    {
        Instance = this;
    }

    #endregion

	#region Public Methods

    // playerdata must be called before this
    public void Load(EnumCollection.Language _language)
    {        
        TextAsset textAsset = Resources.Load<TextAsset>("Language/" + _language.ToString());

        // path
        Debug.Log("Language/" + _language.ToString());

        Debug.Log(textAsset.text);

//        language = JsonUtility.FromJson<Language>(strJson);
        language = JsonUtility.FromJson<Language>(textAsset.text);
//        language = LitJson.JsonMapper.ToObject<Language>(strJson);

        if (language != null)
            Debug.Log("Language count " + language.localizedText.Count);
        else
            Debug.Log("Language null");
//        Debug.Log("Language selected " + language.ToString());
    }
        
    #endregion

}

[System.Serializable]
public class Language
{
    public List<LocalizedText> localizedText = new List<LocalizedText>();
//    public Dictionary<string, string> localizedText;
}

[System.Serializable]
public class LocalizedText
{
    [SerializeField]
    public string key;
    [SerializeField]
    public string value;
}