﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Caption : MonoBehaviour {

    #region Public Variable

    [SerializeField]
    string _key;

    #endregion

    #region Private Variable

    TMPro.TextMeshProUGUI text;

    #endregion

	// Use this for initialization
	void Start () 
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();
        
        SetCaption();
	}	

    #region Public Method

    public void SetCaption()
    {     
        Debug.Log("Font Asset " + text.font.name);

        // check current text mesh
        if(DataController.ins.PlayerData.playerData.language.ToString() != text.font.name)
        {            
            TMPro.TMP_FontAsset fontAsset;

            fontAsset = Resources.Load<TMPro.TMP_FontAsset>("Fonts/" + DataController.ins.PlayerData.playerData.language.ToString());

            Debug.Log("Change Font " + fontAsset.name);

            if (fontAsset != null)
                text.font = fontAsset;            
        }

        // check if key exist
        try
        {
            string value = LanguageManager.Instance.language.localizedText.FirstOrDefault(key => key.key == _key).value;
            if(value != null | value != "")
            {
                text.text = value;
            }

            Debug.Log("Try text value " + value);
        }
        catch(NullReferenceException ex)
        {
            text.text = "null";
        }
    }

    #endregion
}
