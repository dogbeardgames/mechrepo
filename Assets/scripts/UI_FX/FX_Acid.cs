﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_Acid : UI_FX {

	// Use this for initialization
	void Start () {
		FadeOut ();
        DontDestroyOnLoad(gameObject);
	}
	
	
	public override void FadeIn ()
	{
		base.FadeIn ();
	}

	public override void FadeOut ()
	{
		base.FadeOut ();
	}

	void OnEnable()
	{
		FadeOut ();
	}
}
