﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UI_FX_Manager : MonoBehaviour {
	public static UI_FX_Manager ins;

	
    [SerializeField]
    GameObject fx_explosion;
    [SerializeField]
    GameObject fx_botSpark;
    [SerializeField]
    GameObject fx_oreSpark;
    [SerializeField]
    GameObject fx_select;
    [SerializeField]
    GameObject fx_textScore;


    [SerializeField]
    ObjectPooler explodePooler;
    [SerializeField]
    ObjectPooler botPooler;
    [SerializeField]
    ObjectPooler oreSparkPooler;
    [SerializeField]
    ObjectPooler selectPooler;
    [SerializeField]
    TextObjectPooler textScorePooler;


    Color colorGreen = new Color32(108,253,0,255);
    Color colorYellow = new Color32(254, 179, 0, 255);
    Color colorRed = new Color32(255, 0, 0, 255);

    public Color GetGreen()
    {
        return colorGreen;
    }

    public Color GetYellow()
    {
        return colorYellow;
    }

    public Color GetRed()
    {
        return colorRed;
    }

    void Awake()
	{
		ins = this;
	}

    void Start()
    {
        botPooler.PreLoad(4, fx_botSpark, new Vector3(0, GameManager.TopScreen(), 0));
        oreSparkPooler.PreLoad(2, fx_oreSpark, new Vector3(0, GameManager.TopScreen(), 0));
        selectPooler.PreLoad(4, fx_select, new Vector3(0, GameManager.TopScreen(), 0));
        explodePooler.PreLoad(16, fx_explosion, new Vector3(0, GameManager.TopScreen(), 0));
        textScorePooler.PreLoad(4, fx_textScore, new Vector3(0, GameManager.TopScreen() * 10, 0));
    }

	

    public void SpawnExplosion(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = explodePooler.SpawnObject()) == null)
        {
            obj = Instantiate(fx_explosion, pos, Quaternion.identity) as GameObject;
            explodePooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

    public void SpawnBottomSpark(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = botPooler.SpawnObject()) == null)
        {
            obj = Instantiate(fx_botSpark, pos, Quaternion.identity) as GameObject;
            botPooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

    public void SpawnOreSpark(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = oreSparkPooler.SpawnObject()) == null)
        {
            obj = Instantiate(fx_oreSpark, pos, Quaternion.identity) as GameObject;
            oreSparkPooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

    public void SpawnScore(Vector2 pos, string text, Color clr)
    {
        GameObject obj = null;
        if ((obj = textScorePooler.SpawnObject(text, clr)) == null)
        {
            obj = Instantiate(fx_textScore, pos, Quaternion.identity) as GameObject;
            obj.GetComponent<TextMeshPro>().text = text;
            obj.GetComponent<TextMeshPro>().color = clr;
            textScorePooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }
    }

    public void SpawnSelect(Vector2 pos, Color clr)
    {
        if (clr == Color.green)
        {
            clr = colorGreen;
        }
        else if (clr == Color.yellow)
        {
            clr = colorYellow;
        }
        else if (clr == Color.red)
        {
            clr = colorRed;
        }

        GameObject obj = null;
        if ((obj = selectPooler.SpawnObject()) == null)
        {
            obj = Instantiate(fx_select, pos, Quaternion.identity) as GameObject;
            oreSparkPooler.Pool(obj);
            fx_select.GetComponent<SpriteRenderer>().color = clr;
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
            obj.GetComponent<SpriteRenderer>().color = clr;
        }

    }

}
