﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

interface IFade{
		void FadeOut();
		void FadeIn();
}

public abstract class UI_FX: MonoBehaviour, IFade{

	public float lifeTime;
	public SpriteRenderer rndr;

    void OnEnable()
    {
        if(lifeTime > 0)
        {
            Invoke("Disable", lifeTime);
        }
    }

	public virtual void FadeOut()
	{
    //rndr.DOFade (1f, 0f);
    //rndr.DOFade (0, lifeTime).OnComplete (()=>gameObject.SetActive(false));
       
	}

	public virtual void FadeIn()
	{

	}

	public void Disable()
	{
		gameObject.SetActive (false);
	}
}
