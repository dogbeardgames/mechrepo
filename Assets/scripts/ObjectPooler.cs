﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class ObjectPooler : MonoBehaviour {

	public List<GameObject> objLst;

	public void Pool(GameObject obj)
	{
		objLst.Add(obj);
	}

	public GameObject SpawnObject()
	{
		for(int i=0; i<objLst.Count; i++)
		{
			if(!objLst[i].activeInHierarchy)
			{
				
				return objLst [i];
			}
		}

		return null;
	}

    public void PreLoad(int number, GameObject prefab, Vector3 pos)
    {
        for(int i=0; i<number; i++)
        {
            GameObject obj = (GameObject)Instantiate(prefab, pos * 10f, Quaternion.identity);
            Pool(obj);
           // obj.SetActive(false);
        }
    }
}
