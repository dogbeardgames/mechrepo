﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AloneSFXPlay : MonoBehaviour {

    public void PlaySFX(string sfx)
    {
        if(sfx == "")
            SoundManager.Instance.PlaySFX("seTap");
        else
            SoundManager.Instance.PlaySFX(sfx);
    }

    public void PlaySFX()
    {
      
        SoundManager.Instance.PlaySFX("seTap");
     
    }
}
