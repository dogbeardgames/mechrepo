﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataController: MonoBehaviour {

    string playerDataFileName = "playerData";

    // Filename
    public string PlayerDataFileName
    {
        get { return playerDataFileName; }
    }

    public PlayerData playerData;

    public void Save()
    {
        Data<PlayerData>.Save(playerDataFileName, playerData);
    }

    public void Load()
    {
        playerData = Data<PlayerData>.Load(playerDataFileName, playerData);
    }

    // Temp
    public void Delete()
    {
        Data.DeleteFile(playerDataFileName);
    }

//    // temp save for mech data
//    private void OnDisable()
//    {
//        Data<PlayerData>.Save(playerDataFileName, playerData);
//        Debug.Log("Path " + Application.persistentDataPath);
//    }
}   