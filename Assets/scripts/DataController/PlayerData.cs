﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {

    public string playerName = "Builder Hero";
    public uint gold = 0;
    public uint credits = 0;

    // for quick mode;
    public float quickHighestGoldEarned;
    public uint quickMechAssembled;
    public uint quickNumberOfMech;
    public uint quickMaxCombo;
    // for endless mode
    public float endlessHighestGoldEarned;
    public uint endlessMechAssembled;
    public uint endlessNumberOfMech;
    public uint longestTime;
    public uint endlessMaxCombo;
    // settings
    public bool isBGMOn = true;
    public bool isSFXON = true;
    // languagee settings
    public EnumCollection.Language language = EnumCollection.Language.English;
}
