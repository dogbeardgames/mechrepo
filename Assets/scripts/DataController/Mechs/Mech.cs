﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Mech{

    public int id;
    public string mechName;
    public string description;
    public EnumMech mech;
    public byte level;
    public byte numOfParts;
    public uint buildRewardQuick;
    public uint buildRewardEndless;
    public uint goldRewardIncrement;
    public uint upgradeCost;
}    
