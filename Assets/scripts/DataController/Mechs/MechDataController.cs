﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechDataController : MonoBehaviour {

    // filename of the data
    string mechDataFileName = "dbgMechBuilder";

    public Dictionary<int, Mech> mechs;

    public List<Mech> mechList;

    void PrintData()
    {
        for (int i=0; i<mechList.Count; i++)
        {
            print(mechList[i].id);
        }
    }

    public void Load()
    {
        mechList = Data<Mech>.LoadList(mechDataFileName, mechList);               

        // check if list is not null
        if (mechList.Count == 0 || mechList == null)
        {
            // mech list is empty, add default mechs to collection
            Debug.Log("mechlist is empty");

            Mech mech = new Mech();
            mech.id = 0;
            mech.mechName = "Vestar";
            mech.description = "Huehuehue";
            mech.mech = EnumMech.MECH_0;
            mech.level = 1;
            mech.numOfParts = 4;
            mech.buildRewardQuick = 100;
            mech.buildRewardEndless = 50;
            mech.goldRewardIncrement = 10;
            mech.upgradeCost = 2250;

            mechList.Add(mech);

            Mech mech2 = new Mech();

            mech2.id = 1;
            mech2.mechName = "Iris";
            mech2.description = "Huehuehue2";
            mech2.mech = EnumMech.MECH_1;
            mech2.level = 1;
            mech2.numOfParts = 4;
            mech2.buildRewardQuick = 125;
            mech2.buildRewardEndless = 62;
            mech2.goldRewardIncrement = 12;
            mech2.upgradeCost = 1500;

            mechList.Add(mech2);

//            Mech mech3 = new Mech();
//
//            mech3.id = 2;
//            mech3.mechName = "Mech 3";
//            mech3.description = "Huehuehue3";
//            mech3.mech = EnumMech.MECH_1;
//            mech3.level = 1;
//            mech3.numOfParts = 5;
//            mech3.buildRewardQuick = 250;
//            mech3.buildRewardEndless = 125;
//            mech3.goldRewardIncrement = 25;
//            mech3.upgradeCost = 6250;
//
//            mechList.Add(mech3);
        }       

        for(int i=0; i<mechList.Count; i++)
        {
            if (mechList[i].id == i)
            {
                mechList[i].mech = (EnumMech)i;
            }
        }
    }        

    public void Save()
    {
        Data<Mech>.SaveList(mechDataFileName, mechList);
        
    }     

    //Temp
    public void Delete()
    {
        Data.DeleteFile(mechDataFileName);
    }
}
