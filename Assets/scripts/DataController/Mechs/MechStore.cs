﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MechStore : Mech 
{    
    public uint Price;
    public EnumCollection.StoreCurrency storeCurrency;
    public Sprite mechImage;
    public Sprite mechIcon;
    public Sprite mechIconBluePrint;
    public Sprite[] sprtMechParts;
}
