﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour {
    
    public static DataController ins;

    MechDataController mechData;
    PlayerDataController playerData;
    AchievementsManager achievementManager;

    public MechDataController MechData
    {
        get { return mechData; }
    }

    public PlayerDataController PlayerData
    {
        get { return playerData; }
    }
    public AchievementsManager AchievementsDataManager
    {
        get { return achievementManager; }
    }

    void Awake()
    {
        ins = this;
        DontDestroyOnLoad(gameObject);

        mechData = GetComponent<MechDataController>();
        mechData.Load();

        playerData = GetComponent<PlayerDataController>();
        playerData.Load();

        achievementManager = GetComponent<AchievementsManager>();
        achievementManager.Load();

        // load language
        GetComponent<LanguageManager>().Load(playerData.playerData.language);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if(pauseStatus)
            Save();
    }
    void OnApplicationQuit()
    {
        Save();
    }   

    void Save()
    {
        GetComponent<MechDataController>().Save();
        GetComponent<PlayerDataController>().Save();
        // We don't save language
        GetComponent<AchievementsManager>().Save();
    }
}
