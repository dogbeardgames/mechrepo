﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class MessageBox : MonoBehaviour {

    [SerializeField]
    TextMeshProUGUI txtTitle, txtMessage;
   
    public Button btnYes, btnNo;
    void Start()
    {
       // DontDestroyOnLoad(gameObject);
    }

    void OnEnable()
    {
        btnYes.onClick.RemoveAllListeners();
        btnNo.onClick.RemoveAllListeners();
    }
  
    public void SetTitle(string title)
    {
        //txtTitle.text = "color=FEB300FF" + title + "</color>";
    }

    public void SetMessage(string message)
    {
        txtMessage.text = message;
    }

    public void SetConfirmCaption(string caption)
    {
        btnYes.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = caption;
    }

    public void SetCancelCaption(string caption)
    {
        btnNo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = caption;
    }

    public void YesNo(bool b)
    {
        
        if (!b)
        {
            btnNo.gameObject.SetActive(false);
            btnYes.gameObject.SetActive(true);
            SetConfirmCaption("OK");
        }
        else
        {
            btnNo.gameObject.SetActive(true);
            btnYes.gameObject.SetActive(true);
            SetConfirmCaption("Yes");
            SetCancelCaption("No");
        }
       
    }
   

   

    

}
