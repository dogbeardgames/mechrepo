﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MessageSystem : MonoBehaviour {
    public static MessageSystem ins;

    [SerializeField]
    GameObject messageBox;

    ObjectPooler messageBoxPooler;
    public delegate void handler();
    public event handler OnYesEvent;
    public event handler OnNoEvent;

    MessageBox activeMsgBox;
   
    void Awake()
    {
        if (ins == null)
        {
            ins = this;
            messageBoxPooler = GetComponent<ObjectPooler>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {

        //Show("BLACKPINK", "As if it your last!", true,
        //    delegate { print("Lisa"); },
        //    delegate
        //    {
        //        print("Jennie");
        //        Show("TWICE", "Im like TT", false,
        //            delegate { print("Nayeon"); },
        //            delegate { print("Momo"); }
        //        );
        //    });
        print(gameObject.name + "asdasd");

    }
    public void Show(string title, string message, bool YesNO, UnityAction uaYes, UnityAction uaNo)
    {
        SoundManager.Instance.PlaySFX("sePopup");
        GameObject obj = null;
        if ((obj = messageBoxPooler.SpawnObject()) == null)
        {
            obj = Instantiate(messageBox, transform, false) as GameObject;
            messageBoxPooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
        }            
        activeMsgBox = obj.GetComponent<MessageBox>();
       
        //activeMsgBox.SetTitle(title);
        activeMsgBox.SetMessage("<color=#FEB300FF>" + title + "</color>\n" +message);
        activeMsgBox.YesNo(YesNO);

        // remove current listeners
        activeMsgBox.btnYes.onClick.RemoveAllListeners();
        activeMsgBox.btnNo.onClick.RemoveAllListeners();

        //activeMsgBox.btnYes.onClick.AddListener(ClickYesOK);
        //activeMsgBox.btnNo.onClick.AddListener(ClickNo);
        if (uaYes != null)
        {
            activeMsgBox.btnYes.onClick.AddListener(uaYes);
        }
        if (uaNo != null)
        {
            activeMsgBox.btnNo.onClick.AddListener(uaNo);
        }
        //activeMsgBox.btnYes.onClick.AddListener(()=>activeMsgBox.gameObject.SetActive(false));
        //activeMsgBox.btnNo.onClick.AddListener(()=> activeMsgBox.gameObject.SetActive(false));
        //setactive bool
    }

    void ClickYesOK()
    {
        if (OnYesEvent != null)
        {
            SoundManager.Instance.PlaySFX("seTap");
            activeMsgBox.gameObject.SetActive(false);
            OnYesEvent();
            OnYesEvent = delegate { };          
        }
    }

    void ClickNo()
    {
        if (OnNoEvent != null)
        {
            SoundManager.Instance.PlaySFX("seTap");
            activeMsgBox.gameObject.SetActive(false);
            OnNoEvent();
            OnNoEvent = delegate { };
            
        }
    }
    public void Hide()
    {
        activeMsgBox.btnNo.onClick.RemoveAllListeners();
        activeMsgBox.btnYes.onClick.RemoveAllListeners();
        activeMsgBox.gameObject.SetActive(false);
    }
}
