﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

[System.Serializable]
public class TutorialManager : MonoBehaviour {

    #region Public Variables

    public enum TutorialType
    {
        Game, Shop, Hangar, Done
    }

    public TutorialType tutorialType = TutorialType.Game;

    public static TutorialManager Instance;

    public static bool isInTutorial = false;
    #endregion

    #region Private Variable

    int currentTutorialSequence = 0;

    GameObject objTutorialCanvas;

    [SerializeField]
    GameObject tutorialCanvas, LanguageSelect;

    [SerializeField]
    TutorialText tutorialText = new TutorialText();
    // tutorial sequence
    Queue<System.Action> queueTutorialActions = new Queue<System.Action>();
    // tutorial text dictionary
    Queue<string> queueTutorialText = new Queue<string>();

    TextMeshProUGUI txtTutorialText;

    TutorialCanvas tutsCanvas;

    // transforms
    private Transform trShop, trHangar, trQuickGame;

    public static bool isReplay = false;

    #endregion

    #region Mono

    void Awake()
    {        
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () 
    {                

    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += SceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneLoaded;
    }   

    #endregion
	
    #region Private Methods

    void LanguageSelected(GameObject obj)
    {
        SoundManager.Instance.PlaySFX("seTap");
        //PlayerPrefs.SetInt("LanguageSelect", 1);

        // dropDown value
        EnumCollection.Language enumLanguage = (EnumCollection.Language)obj.GetComponent<LanguageSelect>().dropDownLanguage.value;

        DataController.ins.PlayerData.playerData.language = enumLanguage;

        // Load selected language
        LanguageManager.Instance.Load(enumLanguage);

        // Create Tutorial Manager
        TutorialManager.Instance.CreateGameTutorial();

        Destroy(obj);
    }

    // no localization yet
    void StartTutorial()
    {
        DataController.ins.PlayerData.playerData.language = EnumCollection.Language.English;

        // Load selected language
        LanguageManager.Instance.Load(EnumCollection.Language.English);

        // Create Tutorial Manager
        TutorialManager.Instance.CreateGameTutorial();
    }

    #endregion

    #region Public Methods

    public void CreateGameTutorial()
    {
        // Create Tutorial Canvas
        objTutorialCanvas = Instantiate(tutorialCanvas);
        objTutorialCanvas.GetComponent<Canvas>().worldCamera = Camera.main;
        // get canvas tutorial button and add AddTutorialSequence method
        objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialButton.onClick.AddListener(GameTutorialAction);
        // get canvas tutorial text
        txtTutorialText = objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialText;
        // Load text
        TextAsset textAsset = Resources.Load<TextAsset>("Tutorial/Game/" + DataController.ins.PlayerData.playerData.language.ToString());

        tutorialText = JsonUtility.FromJson<TutorialText>(textAsset.text);

        for (int i = 0; i < tutorialText.items.Count; i++)
        {            
            queueTutorialText.Enqueue(tutorialText.items[i].value);
        }

        Debug.Log("Tutorial json " + textAsset.text);

        // is now in tutorial
        isInTutorial = true;
        // Add tutorial sequence to queue
        AddGameTutorialSequence();
        // first sequence
        GameTutorialAction();
    }

    public void CreateShopTutorial()
    {
        // check is tutorial canvas is still available
        if(objTutorialCanvas == null)
        {
            // Create Tutorial Canvas
            objTutorialCanvas = Instantiate(tutorialCanvas);
        }

        objTutorialCanvas.GetComponent<Canvas>().worldCamera = Camera.main;
        // get canvas tutorial button and add ShopTutorialAction method
        objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialButton.onClick.AddListener(ShopTutorialAction);
        // get canvas tutorial text
        txtTutorialText = objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialText;
        // Load text
        TextAsset textAsset = Resources.Load<TextAsset>("Tutorial/Shop/" + DataController.ins.PlayerData.playerData.language.ToString());

        tutorialText = JsonUtility.FromJson<TutorialText>(textAsset.text);

        for (int i = 0; i < tutorialText.items.Count; i++)
        {            
            queueTutorialText.Enqueue(tutorialText.items[i].value);
        }

        Debug.Log("Tutorial json " + textAsset.text);

        // is now in tutorial
        isInTutorial = true;

        AddShopTutorialSequence();
        // first sequence
        ShopTutorialAction();
    }

    public void CreateHangarTutorial()
    {
        // check if tutorial canvas is still available
        if(objTutorialCanvas == null)
        {
            // Create Tutorial Canvas
            objTutorialCanvas = Instantiate(tutorialCanvas);
        }

        objTutorialCanvas.GetComponent<Canvas>().worldCamera = Camera.main;
        // get canvas tutorial button and add AddTutorialSequence method
        objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialButton.onClick.AddListener(HangarTutorialAction);
        // get canvas tutorial text
        txtTutorialText = objTutorialCanvas.GetComponent<TutorialCanvas>().tutorialText;
        // Load text
        TextAsset textAsset = Resources.Load<TextAsset>("Tutorial/Hangar/" + DataController.ins.PlayerData.playerData.language.ToString());

        tutorialText = JsonUtility.FromJson<TutorialText>(textAsset.text);

        for (int i = 0; i < tutorialText.items.Count; i++)
        {            
            queueTutorialText.Enqueue(tutorialText.items[i].value);
        }

        Debug.Log("Tutorial json " + textAsset.text);

        // is now in tutorial
        isInTutorial = true;

        AddHangarTutorialSequence();
        // first sequence
        HangarTutorialAction();
    }

    #endregion

    #region MyRegion

    [ContextMenu("Delete Player Prefs")]
    void DeletePlayerPref()
    {
        PlayerPrefs.DeleteAll();
    }

    #endregion

    #region Events

    // scene events

    void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Menu")
        {            
            trHangar = GameObject.Find("btn_HangarLayout").transform;
            trQuickGame = GameObject.Find("btn_QuickLayout").transform;
            trShop = GameObject.Find("btn_shopLayout").transform;
            Debug.Log("Menu Scene Loaded! " + trHangar.name + ", " + trQuickGame);

        }

        // transforms
        if (isReplay)
        {
            if(scene.name == "Menu" && tutorialType == TutorialType.Game)
            {
                CreateGameTutorial();
            }
            else if(scene.name == "Mech_test" && tutorialType == TutorialType.Game)
            {
                GameTutorialAction();
            }
            else if(scene.name == "Menu" && tutorialType == TutorialType.Shop)
            {
                CreateShopTutorial();
            }
            else if(scene.name == "Menu" && tutorialType == TutorialType.Hangar)                
            {
                CreateHangarTutorial();
            }
        }
        else
        {            
            if (scene.name == "Menu" && tutorialType == TutorialManager.TutorialType.Game && !PlayerPrefs.HasKey("LanguageSelect"))
            {
                // no localization yet
//                GameObject objLanguageSelect = Instantiate(LanguageSelect, GameObject.Find("Canvas").transform, false);
//                objLanguageSelect.GetComponent<LanguageSelect>().btnOk.onClick.AddListener(() => LanguageSelected(objLanguageSelect));

                // Discard if localization is available
                StartTutorial();
            }
            else if (scene.name == "Mech_test" && tutorialType == TutorialType.Game && !PlayerPrefs.HasKey("LanguageSelect"))
            {
//            if (tutsCanvas != null)
//            {
                GameTutorialAction();
//            }  
            }
            else if (scene.name == "Menu" && tutorialType == TutorialType.Shop && !PlayerPrefs.HasKey("LanguageSelect"))
            {
//            if(tutsCanvas != null)
//            {
                CreateShopTutorial();
//            }
            }
            else if (scene.name == "Menu" && tutorialType == TutorialType.Hangar && !PlayerPrefs.HasKey("LanguageSelect"))
            {
//            if(tutsCanvas != null)
//            {
                CreateHangarTutorial();
//            }
            }
            else if (PlayerPrefs.HasKey("LanguageSelect"))
            {
                Destroy(gameObject);
            }
        }
    }

    #endregion

    #region Tutorial Sequence

    public void GameTutorialAction()
    {
        // get action and do it
        System.Action action = queueTutorialActions.Dequeue();
        if (action != null)
            action();
        else
            Debug.Log("Game action is null");
    }

    public void ShopTutorialAction()
    {
        // get action and do it
        Debug.Log("Shop tutorial action");
        System.Action action = queueTutorialActions.Dequeue();
        if (action != null)
            action();
        else
            Debug.Log("Shop action is null");
    }

    public void HangarTutorialAction()
    {
        // get action and do it
        Debug.Log("Hangar tutorial action");
        System.Action action = queueTutorialActions.Dequeue();
        if (action != null)
            action();
    }

    public void RemoveTutorialPieceTile()
    {
        TutorialTile[] tile = Resources.FindObjectsOfTypeAll<TutorialTile>();
        // remove tutorial tile

        Debug.Log(string.Format("<color=red>Tutorial Tile Count: {0}</color>", tile.Length));
        for (int i = 0; i < tile.Length; i++)
        {
            tile[i].gameObject.AddComponent<PieceTile>();

            PieceTile.move = false;

            tile[i].gameObject.SetActive(false);
            Destroy(tile[i]);
        }
    }

    void AddGameTutorialSequence()
    {
        // dialog box default value: width:324.18, height: 220.92

        tutsCanvas = objTutorialCanvas.GetComponent<TutorialCanvas>();

        // enable skip button
        tutsCanvas.EnableSkipButton();

        if(queueTutorialActions != null)            
        {
            // first "welcome to mech build"
            queueTutorialActions.Enqueue(() => 
                {
                    Debug.Log("Welcome");
                    txtTutorialText.text = queueTutorialText.Dequeue();  
                });
            // second, tap "Quick game"
            queueTutorialActions.Enqueue(() => 
                {
                    if(objTutorialCanvas != null)
                    {        
                        Debug.Log("Quick Game");
                        tutsCanvas.dialogBox.SetActive(false);
                        tutsCanvas.mask.transform.localPosition = new Vector3(-2.9802e-08f, 22, 0);
                        tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(156.73f, 156.73f);
                        // arrow
                        tutsCanvas.arrowGuide.SetActive(true);
                        Vector3 viewPort = Camera.main.WorldToScreenPoint(trQuickGame.position);
                        tutsCanvas.arrowGuide.transform.position = Camera.main.ScreenToWorldPoint(viewPort);
                        tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToRight(); 

                        tutsCanvas.mask.GetComponent<Button>().onClick.AddListener(() => {
                            MenuManager.Instance.SelectQuickGame();
                            tutsCanvas.mask.gameObject.SetActive(false);
                        });
                    }
                });
            // third "In quick game, one minute"
            queueTutorialActions.Enqueue(() => 
                {   
                    Debug.Log("Quick Game Loaded, one minute");
                    tutsCanvas.mask.GetComponent<Button>().onClick.RemoveAllListeners();
                    tutsCanvas.dialogBox.SetActive(true);
                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // fourth "keep track gold"
            queueTutorialActions.Enqueue(() =>
                {                    
                    Debug.Log("keep track of gold");
                    tutsCanvas.SetMaskImageToDefault();
                    tutsCanvas.mask.transform.localPosition = new Vector3(-115.3f, 280.8f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(135.8f, 135.8f);
                    tutsCanvas.mask.gameObject.SetActive(true);

                    MechTapTileManager.ins.GetSpawner().DetermineFixedUpdate();

                    // arrow
                    tutsCanvas.gameObject.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(-44f, 275f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToLeft();

                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // fifth "Remaining time"
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("keep track of remaining time");
                    tutsCanvas.mask.transform.localPosition = new Vector3(115.3f, 280.8f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(135.8f, 135.8f);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(51f, 259f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToRight();

                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // sixth "current mech build"
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Current Mech Build");
                    tutsCanvas.mask.transform.localPosition = new Vector3(-0.97f, 280.8f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(114.3f, 150.46f);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(87f, 274f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToLeft();

                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // seventh "Conveyor"
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Conveyor");
                    tutsCanvas.mask.transform.localPosition = new Vector3(1.3f, -84.3f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(382.5f, 389.6f);
                    tutsCanvas.dialogBox.transform.localPosition = new Vector3(0, 233, 0);

                    // disable arrow
                    tutsCanvas.arrowGuide.SetActive(false);

                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // eighth "Build mech bottom to top"
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Build mech bottom to top");
                    tutsCanvas.mask.gameObject.SetActive(false);
                    tutsCanvas.dialogBox.transform.position = new Vector3(0, 0, 0);
                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // Ninth, Build mech in proper order
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Build mech in proper order");
                    tutsCanvas.dialogBox.SetActive(false);
                    tutsCanvas.mechParts.transform.Find("txtHeader").GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                    tutsCanvas.mechParts.transform.Find("btnTryNo").GetComponent<Button>().onClick.AddListener(GameTutorialAction);
                    tutsCanvas.mechParts.SetActive(true);
                });
            // Tenth, Start mech spawn
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Start mech spawn");
//                    tutsCanvas.gameObject.SetActive(true);
                    tutsCanvas.mechParts.SetActive(false);
                    ModeQuickGameManager.instance.StartQuickGameMode();
                });
            // 11th, All mech available, focus to first part
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("All mech available, focus to first part");
                    tutsCanvas.gameObject.SetActive(true);
//                    tutsCanvas.tutsPanel.GetComponent<Image>().raycastTarget = false;
                    tutsCanvas.mechParts.SetActive(false);
                    tutsCanvas.mask.gameObject.SetActive(true);
                    tutsCanvas.mask.GetComponent<Image>().raycastTarget = false;
                    tutsCanvas.mask.transform.localPosition = new Vector3(142.8f, -223.6f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(82, 111.1f);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(66.9f, -222.5f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToRight();
//                    tutsCanvas.arrowGuide.transform.localPosition = new C
                });
            // 12th, All mech available, focus to second part
            queueTutorialActions.Enqueue(() =>
                {                                 
                    Debug.Log("All mech available, focus to second part");
                    tutsCanvas.mask.transform.localPosition = new Vector3(-142.79f, -121f, 0);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(-66f, -127f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToLeft();
                });
            // 14th,  All mech available, focus to third part
            queueTutorialActions.Enqueue(() =>
                {             
                    Debug.Log("All mech available, focus to third part");
                    tutsCanvas.mask.transform.localPosition = new Vector3(-48f, -27f, 0);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(28f, -27f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToLeft();
                });
            // 15th, All mech available, focus to last part
            queueTutorialActions.Enqueue(() =>
                {       
                    Debug.Log("All mech available, focus to last part");
                    tutsCanvas.mask.transform.localPosition = new Vector3(49f, 67f, 0);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(true);
                    tutsCanvas.arrowGuide.transform.localPosition = new Vector3(123f, 67f, 0);
                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToLeft();
                });
            // 16th, Mech build
            queueTutorialActions.Enqueue(() =>
                {            
                    Debug.Log("Mech build");
                    tutsCanvas.mask.gameObject.SetActive(true);
                    tutsCanvas.mask.transform.localPosition = new Vector3(-59.9f, 268.4f, 0);
                    tutsCanvas.mask.GetComponent<RectTransform>().sizeDelta = new Vector2(248.4f, 170.2f);
                    tutsCanvas.tutsPanel.GetComponent<Image>().raycastTarget = true;
                    tutsCanvas.dialogBox.SetActive(true);
                    txtTutorialText.text = queueTutorialText.Dequeue();
                });
            // 17th, Special Tiles
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Special Tiles");
                    tutsCanvas.mask.gameObject.SetActive(false);
                    tutsCanvas.dialogBox.SetActive(false);
                    tutsCanvas.specialTiles.SetActive(true);
                    tutsCanvas.specialTiles.transform.Find("txtHeader").GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                    tutsCanvas.specialTiles.transform.Find("txtBomb").GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                    tutsCanvas.specialTiles.transform.Find("txtOre").GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                    tutsCanvas.specialTiles.transform.Find("txtSupplyBox").GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                    tutsCanvas.specialTiles.transform.Find("btnNext").GetComponent<Button>().onClick.AddListener(GameTutorialAction);
                    tutsCanvas.specialTiles.transform.Find("btnNext").transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                });
            // 18th, Game done, next Shop
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Go to main menu, shop tutorial");
                    GameManager.gameOver = false;
                    GameManager.paused = false;
                    Tile.move = false;
                    MechTapTileManager.ins.GetSpawner().Reset();
                    SoundManager.Instance.BGM.Stop();
                    queueTutorialActions.Clear();
                    queueTutorialText.Clear();
                    tutsCanvas.tutsPanel.SetActive(false);
                    tutsCanvas.specialTiles.SetActive(false);

                    tutsCanvas.tutorialButton.onClick.RemoveAllListeners();
                    tutsCanvas.mask.GetComponent<Button>().onClick.RemoveAllListeners();
                    tutsCanvas.specialTiles.transform.Find("btnNext").GetComponent<Button>().onClick.RemoveAllListeners();
                    tutsCanvas.mechParts.transform.Find("btnTryNo").GetComponent<Button>().onClick.RemoveAllListeners();

                    tutorialType = TutorialType.Shop;

                    ModeQuickGameManager.instance.enabled = true;

//                    PlayerPrefs.SetInt("LanguageSelect", 1);

                    MechUIManager.ins.StopAll();

                    UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
                });            
        }
    }

    void AddShopTutorialSequence()
    {
        tutsCanvas = objTutorialCanvas.GetComponent<TutorialCanvas>();

        // disable skip button
//        tutsCanvas.DisableSkipButton();

        if(queueTutorialActions == null)
        {
            Debug.Log("Shop, queue tutorial action is null");
        }
        else
        {
            Debug.Log("Shop, queue tutorial action is not null");
        }

        if(queueTutorialActions != null)
        {
            // focus on shop icon
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("focus on shop icon");

                    tutsCanvas.tutsPanel.SetActive(true);

                    // arrow
                    tutsCanvas.arrowGuide.SetActive(false);   

                    trShop.GetChild(0).gameObject.SetActive(true);
                    trShop.GetChild(0).GetComponent<ArrowGuide>().SetToUp();

                    tutsCanvas.arrowGuide.GetComponent<ArrowGuide>().SetToUp();
                    tutsCanvas.dialogBox.SetActive(true);
                    tutsCanvas.tutorialText.text = queueTutorialText.Dequeue();
                    tutsCanvas.tutorialButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                });           
            queueTutorialActions.Enqueue(() =>
                {
                    Debug.Log("Next clicked!");

                    // arrow
                    trShop.GetChild(0).gameObject.SetActive(false);

                    tutsCanvas.tutorialButton.onClick.RemoveAllListeners();

                    queueTutorialActions.Clear();
                    queueTutorialText.Clear();

                    tutorialType = TutorialType.Hangar;

                    SceneManager.LoadScene("Menu");
                });
        }
    }
        
    void AddHangarTutorialSequence()
    {
        Debug.Log("AddHangarTutorialSequence called, it's working");

        tutsCanvas = objTutorialCanvas.GetComponent<TutorialCanvas>();

        // enable skip button
//        tutsCanvas.EnableSkipButton();

        if (queueTutorialActions != null)
        {
            // focus on hangar
            queueTutorialActions.Enqueue(() =>
                {
                    // arrow
                    tutsCanvas.arrowGuide.SetActive(false);
                    trHangar.transform.GetChild(0).gameObject.SetActive(true);
                    trHangar.transform.GetChild(0).GetComponent<ArrowGuide>().SetToRight();

                    tutsCanvas.dialogBox.SetActive(true);
                    Vector3 pos = tutsCanvas.dialogBox.transform.localPosition;
                    pos.y = 175;
                    tutsCanvas.dialogBox.transform.localPosition = pos; 
                    tutsCanvas.tutorialText.text = queueTutorialText.Dequeue();
//                    tutsCanvas.tutorialButton.onClick.AddListener(HangarTutorialAction);
                    tutsCanvas.tutorialButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();
                });     
            // focus on mech 3
            queueTutorialActions.Enqueue(() =>
                {
                    trHangar.transform.GetChild(0).gameObject.SetActive(false);

                    Vector3 pos = tutsCanvas.dialogBox.transform.localPosition;
                    pos.y = 0;
                    tutsCanvas.dialogBox.transform.localPosition = pos;
                    tutsCanvas.arrowGuide.SetActive(false);
                    tutsCanvas.tutorialText.text = queueTutorialText.Dequeue();
                    tutsCanvas.tutorialButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = queueTutorialText.Dequeue();

                });
            // open mech 3 data
            queueTutorialActions.Enqueue(() =>
                {
//                    MessageSystem.ins.Show("Tutorial", "Tutorial Done!  Claim reward!", false, null, null);
                    TutorialManager.isReplay = false;
                    PlayerPrefs.SetInt("LanguageSelect", 1);
                    if(DataController.ins.MechData.mechList.FirstOrDefault(mech => mech.id == 2) == null)
                    {
                        Debug.Log("No Mech 3");
                        Mech mech = new Mech();
                        mech.id = 2;
                        mech.mechName = "Gareth";
                        mech.description = "Mech Gareth";
                        mech.mech = EnumMech.MECH_2;
                        mech.level = 1;
                        mech.numOfParts = 5;
                        mech.buildRewardQuick = 250;
                        mech.buildRewardEndless = 125;
                        mech.goldRewardIncrement = 25;
                        mech.upgradeCost = 6250;

                        DataController.ins.MechData.mechList.Add(mech);

                        MessageSystem.ins.Show(null, "You have unlocked Gareth!", false, null, null);
                    }

                    Debug.Log("tuts finished");
                    tutorialType = TutorialType.Done;

                    tutsCanvas.dialogBox.SetActive(false);
                    tutsCanvas.tutorialButton.onClick.RemoveAllListeners();
                    tutsCanvas.mask.GetComponent<Button>().onClick.RemoveAllListeners();
                    queueTutorialText.Clear();
                    queueTutorialActions.Clear();

                    RemoveTutorialPieceTile();

                    if(FindObjectOfType<MechTileSpawner2>() != null)
                        MechTapTileManager.ins.GetSpawner().DetermineFixedUpdate();                                          

                    // achievements
                    DataController.ins.AchievementsDataManager.CollectorChecker(DataController.ins.AchievementsDataManager.MechBluePrints);

                    Destroy(FindObjectOfType<TutorialManager>().gameObject);
                    Destroy(FindObjectOfType<TutorialCanvas>().gameObject);
                });                       
        }

    }

    #endregion
}

[System.Serializable]
public class TutorialText
{
    public List<TutorialTextItem> items = new List<TutorialTextItem>();
}

[System.Serializable]
public class TutorialTextItem
{    
    public string value;
}
