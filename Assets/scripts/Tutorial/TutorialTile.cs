﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TutorialTile : Tile, ITappable {

    // tuts
    public static int currentPieceCount = 0;
    public int pieceOrder;

    public EnumMech mechType;
    public EnumPartType partType;

    public static bool isScoreDeducted = false;

    public bool freePass = false; //if this tile is below or equal to Tapped Part y position this will not be added to missed part list

    static int missPenalty = 50;

    void Start ()
    {
        DontDestroyOnLoad(gameObject);

    }

    public override void Move ()
    {
        base.Move ();
        if(transform.position.y < GameManager.BotScreen() && move)
        {       
            if (MechTapTileManager.ins.GetMechPartToTap().Equals(partType) && MechTapTileManager.ins.mechToAssemble.Equals(mechType) && !freePass) //free if "part to type" appeared behind the mech to assemble part
            {

                #region Deduct Score
                // deduct score
                // Debug.Log("Deduct score");
                // print("part to tap=" + MechTapTileManager.ins.GetMechPartToTap() + " this=" + partType);
                //print("mech to assemble=" + MechTapTileManager.ins.mechToAssemble + " this=" + mechType);
                ModeUI.ins.SetGoldTextDescreased(missPenalty);
                PieceTile.isScoreDeducted = true;

                #endregion

            }

            //15072017 IF PLAYER MISSED TO TAP THIS TILE
            if (mechType == MechTapTileManager.ins.mechToAssemble && !freePass)
            {
                byte _partType = (byte)partType;
                byte _partToTap = (byte)MechTapTileManager.ins.GetMechPartToTap();

                // tile can only be missed in sequence. "if this tile is less than to tile to tap not missed"
                //if (_partType >= _partToTap && (_partType - MechTapTileManager.ins.currentMissedPartIndex) == 1 ||
                if (_partType >= _partToTap ||
                    _partType.Equals(0) && MechTapTileManager.ins.currentMissedPartIndex.Equals(0))
                {

                    if (!MechTapTileManager.ins.lstMissedPartIndex.Contains(_partType))
                    {
                        MechTapTileManager.ins.currentMissedPartIndex = _partType;

                        print("MISSED! = " + (byte)partType);

                        //SAVE THE PART TYPE AND MECH TYPE 

                        MechTapTileManager.ins.lstMissedPartIndex.Add(_partType);

                        if (mechType == EnumMech.MECH_0 && _partType == (byte)EnumPartType.PIECE_3)
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_1 && _partType == (byte)EnumPartType.PIECE_3)
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                        else if (mechType == EnumMech.MECH_2 && _partType == (byte)EnumPartType.PIECE_4)
                        {
                            MechTapTileManager.ins.currentMissedPartIndex = 0;
                        }
                    }
                }

            }

            //set position to top for reuse
            //transform.position = new Vector2 (transform.position.x, GameManager.TopScreen());
            gameObject.SetActive (false);
        }
    }

    // tuts
    bool isTimeStopped = false;

    void FixedUpdate()
    {
        if(!GameManager.paused || !GameManager.gameOver)
            Move();

        if (pieceOrder == 0)
        {
            // tuts
            if (transform.position.y <= -3.4f && !isTimeStopped)                
            {
//                TutorialTile[] tutorialTiles = FindObjectsOfType<TutorialTile>();
//
//                for (int i = 0; i < tutorialTiles.Length; i++)
//                {
//                    tutorialTiles[i].mo
//                }
                Tile.move = false;
                ModeQuickGameManager.instance.enabled = false;
                // Tuts
                TutorialManager.Instance.GameTutorialAction();
                isTimeStopped = true;
            }
        }
    }
    public void Set(EnumMech type, EnumPartType part, Sprite sprt)
    {
        mechType = type;
        partType = part;
        //temporary
        GetComponent<SpriteRenderer>().sprite = sprt;
        transform.GetChild (0).gameObject.SetActive (false);
    }

    public void Tap()
    {
        MechTapTileManager.ins.SetTappedMechPart(partType, mechType, transform.position);

        // tuts
        TutorialManager.Instance.GameTutorialAction();
        Debug.Log("Tapped");
    }

    public void OnMouseDown()
    {       
        // tuts
        Debug.Log("OnMouseDown, paused?" + !GameManager.paused + ", Game over? " + !GameManager.gameOver + ", current piece count " + currentPieceCount + ", piece order " + pieceOrder + ", is time stopped " + isTimeStopped);
        if (!GameManager.paused && !GameManager.gameOver && currentPieceCount == pieceOrder && !Tile.move)
        {
            Debug.Log("Time Stopped");
            //            Debug.Log("Gameobject pointer");
            MechTapTileManager.ins.FreePassTiles(transform);

            UI_FX_Manager.ins.SpawnSelect(transform.position, Color.green);
            Tap();
            currentPieceCount++;
            transform.position = new Vector2(transform.position.x, GameManager.TopScreen());
            gameObject.SetActive(false);

            if (currentPieceCount == 4)
                currentPieceCount = 0;
        }

    }

   

    void OnEnable()
    {
        freePass = false;
        PieceTile.isScoreDeducted = false;
    }

    bool IsLastPart()
    {
        return ((mechType == EnumMech.MECH_0 && (int)partType == 3) || (mechType == EnumMech.MECH_1 && (int)partType == 4) || (mechType == EnumMech.MECH_2 && (int)partType == 5)) ? true : false;
    }

    bool Missed()
    {
        //this only missed the first part of mech
        return (mechType == MechTapTileManager.ins.mechToAssemble && partType == MechTapTileManager.ins.GetMechPartToTap()) ? true : false;
    }
}
