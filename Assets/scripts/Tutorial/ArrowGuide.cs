﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ArrowGuide : MonoBehaviour {

    [SerializeField]
    Sprite horizontalArrowLeft, verticalArrowUp;

    Image arrowImage;

    Vector3 localPos;

    void Awake()
    {
        arrowImage = GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
//        arrowImage = GetComponent<Image>();
//        SetToUp();
        Debug.Log("Pos " + transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region Public Methods

    public void SetToLeft()
    {
        arrowImage.transform.localScale = new Vector3(1, 1, 1);
        arrowImage.sprite = horizontalArrowLeft;

        AnimateHorizontal();
    }

    public void SetToRight()
    {
        arrowImage.transform.localScale = new Vector3(-1, 1, 1);
        arrowImage.sprite = horizontalArrowLeft;

        AnimateHorizontal();
    }

    public void SetToUp()
    {
        arrowImage.transform.localScale = new Vector3(1, 1, 1);
        arrowImage.sprite = verticalArrowUp;

        AnimateVertical();
    }

    public void SetToDown()
    {
        arrowImage.transform.localScale = new Vector3(-1, 1, 1);
        arrowImage.sprite = verticalArrowUp;

        AnimateVertical();
    }

    #endregion

    #region Private Methods

    void AnimateHorizontal()
    {
        transform.DOKill(false);

        localPos = transform.localPosition;
        Vector3 currPos = transform.localPosition;
        currPos.x -= 5;
        transform.localPosition = currPos;
        transform.DOLocalMoveX(localPos.x + 5, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }

    void AnimateVertical()
    {
        transform.DOKill(false);

        localPos = transform.localPosition;
        Vector3 currPos = transform.localPosition;
        currPos.y -= 5;
        transform.localPosition = currPos;
        transform.DOLocalMoveY(localPos.y + 5, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }

    #endregion
}
