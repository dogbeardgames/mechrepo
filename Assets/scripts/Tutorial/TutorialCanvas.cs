﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialCanvas : MonoBehaviour {

    #region Publc Variables

    public TextMeshProUGUI tutorialText;
    public Button tutorialButton, skipButton;
    public Image mask;
    public GameObject dialogBox, mechParts, specialTiles, tutsPanel, arrowGuide;

//    public Transform trShop, trQuickMode, trHangar;

    [SerializeField]
    Sprite sprtQuickGame, sprtDefault;

    #endregion


    #region Mono

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(gameObject);
        skipButton.onClick.AddListener(SkipTutorial);
    }

    // Update is called once per frame
    //  void Update () {
    //      
    //  }

    void OnEnable()
    {
        SceneManager.sceneLoaded += SceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneLoaded;
    }

    #endregion
	
    #region Private Methods

    void SkipTutorial()
    {
        UnityEngine.Events.UnityAction skipTutorialEvent = () =>
        {
                PlayerPrefs.SetInt("LanguageSelect", 1);

                if(TutorialManager.Instance != null)
                {
                    TutorialManager.Instance.RemoveTutorialPieceTile();
                    if(FindObjectOfType<MechTileSpawner2>() != null)
                        MechTapTileManager.ins.GetSpawner().DetermineFixedUpdate();
                    Destroy(TutorialManager.Instance.gameObject);
                }

                if(FindObjectOfType<TutorialCanvas>() != null)
                {
                    Destroy(FindObjectOfType<TutorialCanvas>().gameObject);
                }
                    
                TutorialManager.isReplay = false;

                SceneManager.LoadScene("Menu");
        };
        MessageSystem.ins.Show("Mech Builder Tutorial", "Skip Tutorial?", true, skipTutorialEvent, null);
    }

    void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
    }

    #endregion

    #region Public Methods

    public void SetMaskImageToQuick()
    {
        mask.sprite = sprtQuickGame;
    }

    public void SetMaskImageToDefault()
    {
        mask.sprite = sprtDefault;
    }

    public void EnableSkipButton()
    {
        skipButton.gameObject.SetActive(true);
    }

    public void DisableSkipButton()
    {
        skipButton.gameObject.SetActive(false);
    }

    #endregion
}
