﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;

public class LeaderboardManager : MonoBehaviour
{            
    public static string userID;
    public static string userName;

    public static string 
        iosQuickLeaderboard = "MBQuickLeaderboard",
        iosEndlessLeaderboard = "MBEndlessLeaderboard",
        androidQuickLeaderboard = "CgkIr_qnpMsBEAIQBQ",// "CgkIr_qnpMsBEAIQAA",
        androidEndlessLeaderboard = "CgkIr_qnpMsBEAIQBg";//"CgkIr_qnpMsBEAIQAQ";
                    

    public static bool isLoggedIn = false;

    #region LEADERBOARDS

#if UNITY_ANDROID

    public static void InitilializeGoogleLeadeboard()
    {
        PlayGamesClientConfiguration configuration = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(configuration);
        // ** Should be disabled later **
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }

#endif

    public static void AuthenticateSocial()
    {
        Social.localUser.Authenticate((bool isSuccessful) =>
        {
           if (isSuccessful)
           {                
                SetUserInfo();  
                Debug.Log("Authenticate successful, id: " + userID + ", username: " + userName);

           }
           else
           {
               // failed authentication
               Debug.Log("Failed to authenticate");
           }
        });
    }

    // upload score to leaderboards, ios/google
    public static void ReportScore(long score, string leaderBoardId)
    {
        if (isLoggedIn)
        {
            Social.ReportScore(score, leaderBoardId.ToString(), isSuccessful =>
            {
                if (isSuccessful)
                {
                    Debug.Log("Score successfully uploaded");
                }
                else
                {
                    Debug.Log("Failed to upload score");
                }
            });
        }
    }

    public static void ShowLeaderboard()
    {
        if (isLoggedIn)
        {
            Social.ShowLeaderboardUI();
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
                {             
                    if(success)
                    {
                        SetUserInfo();
                        Social.ShowLeaderboardUI();
                    }
                });
        }
    }

    private static void SetUserInfo()
    {
        userID = Social.localUser.id;
        userName = Social.localUser.userName;
        isLoggedIn = true;
        DataController.ins.PlayerData.playerData.playerName = userName;
    }

#endregion
}
