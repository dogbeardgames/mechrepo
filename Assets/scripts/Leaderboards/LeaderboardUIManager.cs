﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardUIManager : MonoBehaviour
{

    #region LeaderboardUI

    public void ShowLeaderboard()
    {
        LeaderboardManager.ShowLeaderboard();
    }

    #endregion
}
