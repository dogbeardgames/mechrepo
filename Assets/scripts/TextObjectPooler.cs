﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextObjectPooler : MonoBehaviour {

    public List<GameObject> objLst;
    Color clrBlue = new Color32(4,255,193,255), clrRed = new Color32(255, 0, 0, 255);
    public void Pool(GameObject obj)
    {
        objLst.Add(obj);
    }

    public GameObject SpawnObject(string text)
    {
        for (int i = 0; i < objLst.Count; i++)
        {
            if (!objLst[i].activeInHierarchy)
            {
                objLst[i].GetComponent<TextMeshPro>().text = text;
                return objLst[i];
            }
        }

        return null;
    }

    public GameObject SpawnObject(string text, Color clr)
    {
        if (clr == Color.blue)
        {
            clr = clrBlue;
        }
        else if (clr == Color.red)
        {
            clr = clrRed;
        }
        for (int i = 0; i < objLst.Count; i++)
        {
            if (!objLst[i].activeInHierarchy)
            {
                objLst[i].GetComponent<TextMeshPro>().text = text;
                objLst[i].GetComponent<TextMeshPro>().color = clr;
                return objLst[i];
            }
        }

        return null;
    }

    public void PreLoad(int number, GameObject prefab, Vector3 pos )
    {
        for (int i = 0; i < number; i++)
        {
            GameObject obj = (GameObject)Instantiate(prefab, pos, Quaternion.identity);
            Pool(obj);
            // obj.SetActive(false);
        }
    }


}
