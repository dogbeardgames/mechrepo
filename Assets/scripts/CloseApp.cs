﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseApp : MonoBehaviour {

    bool escaped = false;
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.Escape) && !escaped)
        {
            escaped = true;
            MessageSystem.ins.Show("", "Are you sure you want to exit application?", true,
                delegate { Application.Quit(); },
                delegate { escaped = false; });
        }
	}
}
