﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

    #region public variable

    [SerializeField]
    Toggle bgmToggle, sfxToggle;

    [SerializeField]
    TMPro.TMP_Dropdown dropDownLanguage;

    [SerializeField]
    GameObject TutorialPanel, btnTutorial;

    #endregion

    #region MONO

    void Start()
    {
        bgmToggle.onValueChanged.AddListener(BgmToggle);
        sfxToggle.onValueChanged.AddListener(SfxToggle);
        dropDownLanguage.onValueChanged.AddListener(Language);

        if(ModeQuickGameManager.instance != null || ModeEndlessGameManager.instance != null)
        {
            btnTutorial.SetActive(false);
        }
    }

    // Use this for initialization
    void OnEnable () 
    {
        GetSettingsValue();
    }   

    #endregion

    #region Public Methods
    /// <summary>
    /// Save the settings value
    /// </summary>
//    public void Save()
//    {
//        SoundManager.Instance.PlaySFX("seTap");
//        // for audio
//        SoundManager.Instance.BGM.volume = bgmToggle.isOn ? 1 : 0;
//        SoundManager.Instance.SFX.volume = sfxToggle.isOn ? 1 : 0;
//
//        DataController.ins.PlayerData.playerData.isBGMOn = bgmToggle.isOn;
//        DataController.ins.PlayerData.playerData.isSFXON = sfxToggle.isOn;
//
//        // for language
//        DataController.ins.PlayerData.playerData.language = (EnumCollection.Language)dropDownLanguage.value;
//
//        LanguageManager.Instance.Load((EnumCollection.Language)dropDownLanguage.value);
//
//        Caption[] caption = FindObjectsOfType<Caption>();
//        // Call set apply method to apply changes
//        for (int i = 0; i < caption.Length; i++)
//        {
//            caption[i].SetCaption();
//        }
//
////        Destroy(gameObject);
//    }

    public void Tutorial()
    {
        GameObject objTuts = Instantiate(TutorialPanel);
        objTuts.name = "TutorialManager";
        TutorialManager.isReplay = true;
        SoundManager.Instance.PlaySFX("seTap");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }

    public void Close()
    {
        SoundManager.Instance.PlaySFX("seTap");
        Destroy(gameObject);
    }

    #endregion

    #region EVENTS

    void BgmToggle(bool isOn)
    {
        SoundManager.Instance.BGM.volume = isOn ? 1 : 0;
        DataController.ins.PlayerData.playerData.isBGMOn = isOn;
        SoundManager.Instance.PlaySFX("seTap");
    }

    void SfxToggle(bool isOn)
    {
        SoundManager.Instance.SFX.volume = isOn ? 1 : 0;
        DataController.ins.PlayerData.playerData.isSFXON = isOn;
        SoundManager.Instance.PlaySFX("seTap");
    }

    void Language(int itemIndex)
    {
        // for language
        DataController.ins.PlayerData.playerData.language = (EnumCollection.Language)itemIndex;

        LanguageManager.Instance.Load((EnumCollection.Language)itemIndex);

        Caption[] caption = FindObjectsOfType<Caption>();
        // Call set apply method to apply changes
        for (int i = 0; i < caption.Length; i++)
        {
            caption[i].SetCaption();
        }
        SoundManager.Instance.PlaySFX("seTap");
    }

    #endregion

    #region UI

    void GetSettingsValue()
    {
        // get value for slider
        bgmToggle.isOn = DataController.ins.PlayerData.playerData.isBGMOn;
        sfxToggle.isOn = DataController.ins.PlayerData.playerData.isSFXON;

        // get value for language drop down
        dropDownLanguage.value = (int)DataController.ins.PlayerData.playerData.language;
    }
        

    #endregion
}
