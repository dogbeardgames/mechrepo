﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using DG.Tweening;

public class VideoAdsButtonToggler : MonoBehaviour {

    #region Private Variables

    [SerializeField]
    Button btnWatchVideo, btnToggler;

    bool isOpen = false;

    const float distance = 510;

    float xLocalPos;

    #endregion

    #region Mono

    // Use this for initialization
    void Start () 
    {
        btnToggler.onClick.AddListener(Toggle);
        btnWatchVideo.onClick.AddListener(ShowRewardedAd);
        xLocalPos = transform.localPosition.x;
    }        

    #endregion	

    #region Private Methods

    private void Toggle()
    {
        if(isOpen)// close it
        {
            Close();
        }
        else// open it
        {
            Open();
        }
    }

    private void Open()// right to left
    {
        isOpen = true;
        xLocalPos -= distance;
        LocalMove(xLocalPos);
    }

    private void Close()// left to right
    {
        isOpen = false;
        xLocalPos += distance;
        LocalMove(xLocalPos);
    }

    private void LocalMove(float value)
    {
        transform.DOLocalMoveX(value, 0.5f);
    }

    public void ShowRewardedAd()
    {
        Close();// close toggle

        if(Advertisement.IsReady("rewardedVideo"))
        {            
            ShowOptions options = new ShowOptions{ resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch(result)
        {
            case ShowResult.Finished:
//                DataController.ins.PlayerData.playerData.credits += 2;
                GameCurrencyEventManager.Instance.AddCreditValue(2);
                MessageSystem.ins.Show("Workshop", "You've got 2 Credits", false, null, null);
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    #endregion
}
