﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class ResultAds : MonoBehaviour {

	#region ADS

    GameOver _gameOver;

    public void ShowRewardedAd(GameOver gameOver)
    {
        if(Advertisement.IsReady("rewardedVideo"))
        {
            _gameOver = gameOver;

            ShowOptions options = new ShowOptions{ resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch(result)
        {
            case ShowResult.Finished:
                if(_gameOver != null)
                {
                    _gameOver.DoubleReward();
                }
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    #endregion
}
