﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonStore : MonoBehaviour {

    [SerializeField]
    GameObject storePanel;
    GameObject temp;

    public void Show()
    {
        if (temp == null)
        {
            temp = (GameObject)Instantiate(storePanel, GameObject.Find("Canvas").transform, false);
        }
        else
        {
            temp.SetActive(true);
            temp.transform.SetAsLastSibling();
        }
    }
}
