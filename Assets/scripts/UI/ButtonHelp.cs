﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHelp : MonoBehaviour {

    [SerializeField]
    GameObject helpPanel;
    GameObject temp;

    public void Show()
    {
        if (temp == null)
        {
            temp = (GameObject)Instantiate(helpPanel, GameObject.Find("Canvas").transform, false);
        }
        else
        {
            temp.SetActive(true);
        }
    }
}
