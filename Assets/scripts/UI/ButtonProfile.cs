﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonProfile : MonoBehaviour {

    [SerializeField]
    GameObject profilePanel;
    GameObject temp;

    public void Show()
    {
        if (temp == null)
        {
            temp = (GameObject)Instantiate(profilePanel, GameObject.Find("Canvas").transform, false);
        }
        else
        {
            temp.SetActive(true);
        }
    }
}
