﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSettings : MonoBehaviour {

    [SerializeField]
    GameObject settingsPanel;
    GameObject temp;

    public void Show()
    {
        if (temp == null)
        {
            temp = (GameObject)Instantiate(settingsPanel, GameObject.Find("Canvas").transform, false);
        }
        else
        {
            temp.SetActive(true);
        }
    }
}
