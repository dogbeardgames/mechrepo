﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHangar : MonoBehaviour {

    [SerializeField]
    GameObject hangarPanel;
    GameObject temp;

    public void Show()
    {
        if (temp == null)
        {
            temp = (GameObject)Instantiate(hangarPanel, GameObject.Find("Canvas").transform, false);
        }
        else
        {
            temp.SetActive(true);
        }
    }
}
