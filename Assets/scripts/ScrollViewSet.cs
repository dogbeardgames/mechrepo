﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewSet : MonoBehaviour {

    ScrollRect scrollRect;
    [SerializeField]
    bool vertical = false, horizontal = false;
    [SerializeField]
    float vVal = 0, hVal = 0;
	void Start () {
        if (scrollRect == null)
            scrollRect = GetComponent<ScrollRect>();



	}

    void OnEnable()
    {
        if (scrollRect == null)
            scrollRect = GetComponent<ScrollRect>();

        if (scrollRect != null)
        {
            Set();
        }
       
       
    }

    void Set()
    {
        if (vertical)
        {
            scrollRect.verticalNormalizedPosition = vVal;
        }
        if (horizontal)
        {
            scrollRect.horizontalNormalizedPosition = hVal;
        }
    }
	
	
}
