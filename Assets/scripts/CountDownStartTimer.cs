﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDownStartTimer : MonoBehaviour {

    public void StartGame()
    {
        if (GameManager.mode == GAME_MODE.ENDLESS)
        {
            ModeEndlessGameManager.instance.StartEndlessGameMode();
        }
        else
        {
            ModeQuickGameManager.instance.StartQuickGameMode();
        }

        transform.parent.gameObject.SetActive(false);
    }
}
