﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechScriptable : ScriptableObject {

    public MechStore[] mechItems;
}
