﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class AchievementUnlockedUI : MonoBehaviour {

    #region PRIVATE VARIABLES

    float xPos;
    float animationSpeed = 1;

    #endregion

	#region MONO

    void Start()
    {
        xPos = transform.localPosition.x;
    }

    #endregion

    #region PUBLIC METHODS

    public void AchievementUnlocked()
    {        
        transform.DOLocalMoveX(xPos + 410, animationSpeed).OnComplete(() => 
            {
                Invoke("HideAchivementUI", 2);
            });
    }        
        
    #endregion

    #region PRIVATE METHODS

    private void HideAchivementUI()
    {
        transform.DOLocalMoveX(xPos, animationSpeed);
    }

    #endregion
}
