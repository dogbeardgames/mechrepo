﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using DG.Tweening;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour {

    #region Public Variables

    [SerializeField]
    TextMeshProUGUI txtTitle, txtDescription;
    [SerializeField]
    Button claimButton;

    [SerializeField]
    Image achievementComplete;

    int creditReward;      

    [SerializeField]
    int ID;
    [SerializeField]
    string AchievementName;
    [SerializeField]
    string Description;
    [SerializeField]
    Outline outline;
    [SerializeField]
    Image iconGlow;
    #endregion

//    #endregion

    #region Mono

    // Use this for initialization
    void Start () 
    {
        AchievementUI();          
    }        

    #endregion

    #region Private Variables

    void AchievementUI()
    {        
        // check first locked achievement
        AchievementSubItem achievementSubItem = null;

        if (DataController.ins.AchievementsDataManager.IsAllAchievementComplete(ID))
        {
            Description = "";           
            achievementComplete.gameObject.SetActive(true);
        }
        else
        {
            achievementSubItem = DataController.ins.AchievementsDataManager.GetAchievementItem(this.ID, true);

            if (achievementSubItem != null)
            {
                if (!achievementSubItem.isRewardClaimed)
                {
                    Description = string.Format(DataController.ins.AchievementsDataManager.achievementCollection.FirstOrDefault(item => item.ID == this.ID).Description, achievementSubItem.achieventCondition);
                }
                else
                {
                    achievementSubItem = DataController.ins.AchievementsDataManager.GetAchievementItem(this.ID, false);
                    Description = string.Format(DataController.ins.AchievementsDataManager.achievementCollection.FirstOrDefault(item => item.ID == this.ID).Description, achievementSubItem.achieventCondition);
                }
            }
            else
            {
                achievementSubItem = DataController.ins.AchievementsDataManager.GetAchievementItem(this.ID, false);
                Description = string.Format(DataController.ins.AchievementsDataManager.achievementCollection.FirstOrDefault(item => item.ID == this.ID).Description, achievementSubItem.achieventCondition);
            }
        }

        // get achievement
//        AchievementSubItem achievementSubItem = DataController.ins.AchievementsDataManager.GetAchievementItem(this.ID, false);
        AchievementName = DataController.ins.AchievementsDataManager.achievementCollection.FirstOrDefault(item => item.ID == this.ID).AchievementName;

        txtTitle.text = AchievementName;
        txtDescription.text = Description;

        // get credit reward if there is
        creditReward = DataController.ins.AchievementsDataManager.GetUnclaimedReward(this.ID);
        Debug.Log("Has a credit reward " + creditReward);

        claimButton.onClick.RemoveAllListeners();

        if(creditReward != 0 && creditReward != null)
        {            
            AnimateClaimButton();
            claimButton.onClick.AddListener(ClaimReward);
        } 
    }

    void AnimateClaimButton()
    {
        Debug.Log("Animate");
//        Color32 color = outline.effectColor;
        Color32 color = iconGlow.color;
        color.a = 0;
//        outline.effectColor = color;
        iconGlow.color = color;
        color.a = 255;
//        outline.enabled = true;
        iconGlow.enabled = true;
//        outline.DOFade(0, 1).SetLoops(-1, LoopType.Yoyo).id = 0;
//        outline.DOColor(color, 1).SetLoops(-1, LoopType.Yoyo).id = 0;
        iconGlow.DOColor(color, 1).SetLoops(-1, LoopType.Yoyo).id = this.ID;
    }

    void StopClaimButtonAnimation()
    {
        DOTween.Kill(this.ID, false);
//        outline.enabled = false;
        iconGlow.enabled = false;
        Debug.Log("Animation Stopped");
    }

    void ClaimReward()
    {
        Debug.Log("Reward Claimed");
//        StopClaimButtonAnimation();

        // stop tween
        DOTween.Kill(0, false);

        DataController.ins.AchievementsDataManager.RewardAcquired(this.ID);

        MessageSystem.ins.Show("Achievement", string.Format("You got {0} credits", creditReward), false, null, null);

//        DataController.ins.PlayerData.playerData.credits += (uint)creditReward;
        GameCurrencyEventManager.Instance.AddCreditValue((uint)creditReward);

        // get new credit reward
        creditReward = DataController.ins.AchievementsDataManager.GetUnclaimedReward(this.ID);

        AchievementUI();

        if(creditReward == 0)
        {
            Debug.Log("No more reward");
            StopClaimButtonAnimation();
            claimButton.onClick.RemoveListener(ClaimReward);
            claimButton.interactable = false;
        }
    }

    #endregion    	
}
