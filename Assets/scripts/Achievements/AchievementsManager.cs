﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class AchievementsManager : MonoBehaviour {
	
    #region Private Variables

    private string filename = "Achievement";

    [SerializeField]
    private int builtMech, 
        totalUpgrades,
        mechBlueprints,
        titaniumOres,
        totalGold,
        defusedTimeBombs,
        minutesInEndless;

    #endregion

    #region Public Variables
    [SerializeField]
    public List<AchievementCollection> achievementCollection;

    public int BuiltMech { get { return builtMech; } set { builtMech = value; }}
    public int TotalUpgrades { get{ return totalUpgrades; } set { totalUpgrades = value; }}
    public int MechBluePrints { get{ return DataController.ins.MechData.mechList.Count; }}
    public int TitaniumOres { get { return titaniumOres; } set { titaniumOres = value; }}
    public int TotalGold { get { return totalGold; } set { totalGold = value; }}
    public int DefusedTimeBombs { get { return defusedTimeBombs; } set { defusedTimeBombs = value; }}
    public int MinutesInEndless { get { return minutesInEndless; } set { minutesInEndless = value; }}

    #endregion

    #region Mono

    // Use this for initialization
    void Start () 
    {
        
    }
       
    #endregion

    #region Private Methods

    private void SetUpInitialValue()
    {
//        achievementCollection = new List<AchievementCollection>();

        #region Builder
        List<AchievementSubItem> BuilderSubItem = new List<AchievementSubItem>();

        AchievementSubItem builderSubItem1 = new AchievementSubItem();
        builderSubItem1.achieventCondition = 100;
        builderSubItem1.creditReward = 5;
        builderSubItem1.isAchieved = false;
        builderSubItem1.isRewardClaimed = false;

        BuilderSubItem.Add(builderSubItem1);

        AchievementSubItem builderSubItem2 = new AchievementSubItem();
        builderSubItem2.achieventCondition = 500;
        builderSubItem2.creditReward = 5;
        builderSubItem2.isAchieved = false;
        builderSubItem2.isRewardClaimed = false;

        BuilderSubItem.Add(builderSubItem2);

        AchievementSubItem builderSubItem3 = new AchievementSubItem();
        builderSubItem3.achieventCondition = 1000;
        builderSubItem3.creditReward = 5;
        builderSubItem3.isAchieved = false;
        builderSubItem3.isRewardClaimed = false;

        BuilderSubItem.Add(builderSubItem3);

        AchievementSubItem builderSubItem4 = new AchievementSubItem();
        builderSubItem4.achieventCondition = 3000;
        builderSubItem4.creditReward = 5;
        builderSubItem4.isAchieved = false;
        builderSubItem4.isRewardClaimed = false;

        BuilderSubItem.Add(builderSubItem4);

        AchievementSubItem builderSubItem5 = new AchievementSubItem();
        builderSubItem5.achieventCondition = 5000;
        builderSubItem5.creditReward = 5;
        builderSubItem5.isAchieved = false;
        builderSubItem5.isRewardClaimed = false;

        BuilderSubItem.Add(builderSubItem5);

        AchievementCollection Builder = new AchievementCollection();
        Builder.achievementSubItem = BuilderSubItem;
        Builder.ID = 0;
        Builder.AchievementName = "Builder";
        Builder.Description = "Build {0} Mechs";

        //Add to Collection
        achievementCollection.Add(Builder);
        #endregion

        #region Quality Builder
        List<AchievementSubItem> QualityBuilderSubItem = new List<AchievementSubItem>();

        AchievementSubItem qbSubItem1 = new AchievementSubItem();
        qbSubItem1.achieventCondition = 30;
        qbSubItem1.creditReward = 5;
        qbSubItem1.isAchieved = false;
        qbSubItem1.isRewardClaimed = false;

        QualityBuilderSubItem.Add(qbSubItem1);

        AchievementSubItem qbSubItem2 = new AchievementSubItem();
        qbSubItem2.achieventCondition = 50;
        qbSubItem2.creditReward = 5;
        qbSubItem2.isAchieved = false;
        qbSubItem2.isRewardClaimed = false;

        QualityBuilderSubItem.Add(qbSubItem2);

        AchievementSubItem qbSubItem3 = new AchievementSubItem();
        qbSubItem3.achieventCondition = 70;
        qbSubItem3.creditReward = 5;
        qbSubItem3.isAchieved = false;
        qbSubItem3.isRewardClaimed = false;

        QualityBuilderSubItem.Add(qbSubItem3);

        AchievementSubItem qbSubItem4 = new AchievementSubItem();
        qbSubItem4.achieventCondition = 100;
        qbSubItem4.creditReward = 5;
        qbSubItem4.isAchieved = false;
        qbSubItem4.isRewardClaimed = false;

        QualityBuilderSubItem.Add(qbSubItem4);

        AchievementCollection QualityBuilder = new AchievementCollection();
        QualityBuilder.achievementSubItem = QualityBuilderSubItem;
        QualityBuilder.ID = 1;
        QualityBuilder.AchievementName = "Quality Builder";
        QualityBuilder.Description = "Have a total {0} upgrades";

        //Add to Collection
        achievementCollection.Add(QualityBuilder);
        #endregion

        #region Collector
        List<AchievementSubItem> CollectorSubItem = new List<AchievementSubItem>();

        AchievementSubItem collectorSubItem1 = new AchievementSubItem();
        collectorSubItem1.achieventCondition = 5;
        collectorSubItem1.creditReward = 5;
        collectorSubItem1.isAchieved = false;
        collectorSubItem1.isRewardClaimed = false;

        CollectorSubItem.Add(collectorSubItem1);

        AchievementSubItem collectorSubItem2 = new AchievementSubItem();
        collectorSubItem2.achieventCondition = 8;
        collectorSubItem2.creditReward = 5;
        collectorSubItem2.isAchieved = false;
        collectorSubItem2.isRewardClaimed = false;

        CollectorSubItem.Add(collectorSubItem2);

        AchievementSubItem collectorSubItem3 = new AchievementSubItem();
        collectorSubItem3.achieventCondition = 10;
        collectorSubItem3.creditReward = 5;
        collectorSubItem3.isAchieved = false;
        collectorSubItem3.isRewardClaimed = false;

        CollectorSubItem.Add(collectorSubItem3);

        AchievementCollection Collector = new AchievementCollection();
        Collector.achievementSubItem = CollectorSubItem;
        Collector.ID = 2;
        Collector.AchievementName = "Collector";
        Collector.Description = "Collect {0} Mech blueprints";

        //Add to Collection
        achievementCollection.Add(Collector);
        #endregion

        #region Miner
        List<AchievementSubItem> MinerSubItem = new List<AchievementSubItem>();

        AchievementSubItem minerSubItem1 = new AchievementSubItem();
        minerSubItem1.achieventCondition = 50;
        minerSubItem1.creditReward = 5;
        minerSubItem1.isAchieved = false;
        minerSubItem1.isRewardClaimed = false;

        MinerSubItem.Add(minerSubItem1);

        AchievementSubItem minerSubItem2 = new AchievementSubItem();
        minerSubItem2.achieventCondition = 250;
        minerSubItem2.creditReward = 5;
        minerSubItem2.isAchieved = false;
        minerSubItem2.isRewardClaimed = false;

        MinerSubItem.Add(minerSubItem2);

        AchievementSubItem minerSubItem3 = new AchievementSubItem();
        minerSubItem3.achieventCondition = 750;
        minerSubItem3.creditReward = 5;
        minerSubItem3.isAchieved = false;
        minerSubItem3.isRewardClaimed = false;

        MinerSubItem.Add(minerSubItem3);

        AchievementSubItem minerSubItem4 = new AchievementSubItem();
        minerSubItem4.achieventCondition = 1500;
        minerSubItem4.creditReward = 5;
        minerSubItem4.isAchieved = false;
        minerSubItem4.isRewardClaimed = false;

        MinerSubItem.Add(minerSubItem4);

        AchievementSubItem minerSubItem5 = new AchievementSubItem();
        minerSubItem5.achieventCondition = 2500;
        minerSubItem5.creditReward = 5;
        minerSubItem5.isAchieved = false;
        minerSubItem5.isRewardClaimed = false;

        MinerSubItem.Add(minerSubItem5);

        AchievementCollection Miner = new AchievementCollection();
        Miner.achievementSubItem = MinerSubItem;
        Miner.ID = 3;
        Miner.AchievementName = "Miner";
        Miner.Description = "Refine {0} number of titanium ores";

        //Add to Collection
        achievementCollection.Add(Miner);
        #endregion

        #region Tycoon
        List<AchievementSubItem> TycoonSubItem = new List<AchievementSubItem>();

        AchievementSubItem tycoonSubItem1 = new AchievementSubItem();
        tycoonSubItem1.achieventCondition = 25000;
        tycoonSubItem1.creditReward = 5;
        tycoonSubItem1.isAchieved = false;
        tycoonSubItem1.isRewardClaimed = false;

        TycoonSubItem.Add(tycoonSubItem1);

        AchievementSubItem tycoonSubItem2 = new AchievementSubItem();
        tycoonSubItem2.achieventCondition = 50000;
        tycoonSubItem2.creditReward = 5;
        tycoonSubItem2.isAchieved = false;
        tycoonSubItem2.isRewardClaimed = false;

        TycoonSubItem.Add(tycoonSubItem2);

        AchievementSubItem tycoonSubItem3 = new AchievementSubItem();
        tycoonSubItem3.achieventCondition = 100000;
        tycoonSubItem3.creditReward = 5;
        tycoonSubItem3.isAchieved = false;
        tycoonSubItem3.isRewardClaimed = false;

        TycoonSubItem.Add(tycoonSubItem3);

        AchievementSubItem tycoonSubItem4 = new AchievementSubItem();
        tycoonSubItem4.achieventCondition = 500000;
        tycoonSubItem4.creditReward = 5;
        tycoonSubItem4.isAchieved = false;
        tycoonSubItem4.isRewardClaimed = false;

        TycoonSubItem.Add(tycoonSubItem4);

        AchievementSubItem tycoonSubItem5 = new AchievementSubItem();
        tycoonSubItem5.achieventCondition = 1000000;
        tycoonSubItem5.creditReward = 5;
        tycoonSubItem5.isAchieved = false;
        tycoonSubItem5.isRewardClaimed = false;

        TycoonSubItem.Add(tycoonSubItem5);

        AchievementCollection Tycoon = new AchievementCollection();
        Tycoon.achievementSubItem = TycoonSubItem;
        Tycoon.ID = 4;
        Tycoon.AchievementName = "Tycoon";
        Tycoon.Description = "Gain {0} total gold";

        //Add to Collection
        achievementCollection.Add(Tycoon);
        #endregion

        #region Explosive Expert
        List<AchievementSubItem> EESubItem = new List<AchievementSubItem>();

        AchievementSubItem eeSubItem1 = new AchievementSubItem();
        eeSubItem1.achieventCondition = 50;
        eeSubItem1.creditReward = 5;
        eeSubItem1.isAchieved = false;
        eeSubItem1.isRewardClaimed = false;

        EESubItem.Add(eeSubItem1);

        AchievementSubItem eeSubItem2 = new AchievementSubItem();
        eeSubItem2.achieventCondition = 250;
        eeSubItem2.creditReward = 5;
        eeSubItem2.isAchieved = false;
        eeSubItem2.isRewardClaimed = false;

        EESubItem.Add(eeSubItem2);

        AchievementSubItem eeSubItem3 = new AchievementSubItem();
        eeSubItem3.achieventCondition = 750;
        eeSubItem3.creditReward = 5;
        eeSubItem3.isAchieved = false;
        eeSubItem3.isRewardClaimed = false;

        EESubItem.Add(eeSubItem3);

        AchievementSubItem eeSubItem4 = new AchievementSubItem();
        eeSubItem4.achieventCondition = 1500;
        eeSubItem4.creditReward = 5;
        eeSubItem4.isAchieved = false;
        eeSubItem4.isRewardClaimed = false;

        EESubItem.Add(eeSubItem4);

        AchievementSubItem eeSubItem5 = new AchievementSubItem();
        eeSubItem5.achieventCondition = 2500;
        eeSubItem5.creditReward = 5;
        eeSubItem5.isAchieved = false;
        eeSubItem5.isRewardClaimed = false;

        EESubItem.Add(eeSubItem5);

        AchievementCollection ExplosiveExpert = new AchievementCollection();
        ExplosiveExpert.achievementSubItem = EESubItem;
        ExplosiveExpert.ID = 5;
        ExplosiveExpert.AchievementName = "Explosive Expert";
        ExplosiveExpert.Description = "Defuse {0} number of time bombs";

        //Add to Collection
        achievementCollection.Add(ExplosiveExpert);
        #endregion

        #region Tireless
        List<AchievementSubItem> TireLessSubItem = new List<AchievementSubItem>();

        AchievementSubItem tireLessSubItem1 = new AchievementSubItem();
        tireLessSubItem1.achieventCondition = 5;
        tireLessSubItem1.creditReward = 5;
        tireLessSubItem1.isAchieved = false;
        tireLessSubItem1.isRewardClaimed = false;

        TireLessSubItem.Add(tireLessSubItem1);

        AchievementSubItem tireLessSubItem2 = new AchievementSubItem();
        tireLessSubItem2.achieventCondition = 10;
        tireLessSubItem2.creditReward = 5;
        tireLessSubItem2.isAchieved = false;
        tireLessSubItem2.isRewardClaimed = false;

        TireLessSubItem.Add(tireLessSubItem2);

        AchievementSubItem tireLessSubItem3 = new AchievementSubItem();
        tireLessSubItem3.achieventCondition = 15;
        tireLessSubItem3.creditReward = 5;
        tireLessSubItem3.isAchieved = false;
        tireLessSubItem3.isRewardClaimed = false;

        TireLessSubItem.Add(tireLessSubItem3);

        AchievementSubItem tireLessSubItem4 = new AchievementSubItem();
        tireLessSubItem4.achieventCondition = 20;
        tireLessSubItem4.creditReward = 5;
        tireLessSubItem4.isAchieved = false;
        tireLessSubItem4.isRewardClaimed = false;

        TireLessSubItem.Add(tireLessSubItem4);

        AchievementSubItem tireLessSubItem5 = new AchievementSubItem();
        tireLessSubItem5.achieventCondition = 25;
        tireLessSubItem5.creditReward = 5;
        tireLessSubItem5.isAchieved = false;
        tireLessSubItem5.isRewardClaimed = false;

        TireLessSubItem.Add(tireLessSubItem5);

        AchievementCollection TireLess = new AchievementCollection();
        TireLess.achievementSubItem = TireLessSubItem;
        TireLess.ID = 6;
        TireLess.AchievementName = "Tireless";
        TireLess.Description = "Last {0} minutes in Endless mode";

        //Add to Collection
        achievementCollection.Add(TireLess);
        #endregion

        Debug.Log("SetUpInitialValue");
    }

    private void AchievementUI()
    {
        // UI
        if(FindObjectOfType<AchievementUnlockedUI>() != null)
        {
            FindObjectOfType<AchievementUnlockedUI>().AchievementUnlocked();
        }
    }

    #endregion

    #region Public Methods

    #region DATA
    public void Save()
    {
        Debug.Log("Save Achievements");
        Data<AchievementCollection>.SaveList(filename, achievementCollection);

        // p mech
        PlayerPrefs.SetInt("builtMech", builtMech);
        // p upgrades
        PlayerPrefs.SetInt("totalUpgrades", totalUpgrades);
        // p titanium ores
        PlayerPrefs.SetInt("titaniumOres", titaniumOres);
        // p time bombs
        PlayerPrefs.SetInt("defusedTimeBombs", defusedTimeBombs);
    }

    public void Load()
    {
        Debug.Log("Achievement Collection count " + achievementCollection.Count);

        achievementCollection = Data<AchievementCollection>.LoadList(filename, achievementCollection);

        if(achievementCollection == null | achievementCollection.Count == 0)
        {
            SetUpInitialValue();
        }            

        // retrieve item data per achievement
        currentBuilderItem = GetAchievementItem(0, false);
        currentQualityBuilderItem = GetAchievementItem(1, false);
        currentCollectorItem = GetAchievementItem(2, false);
        currentMinerItem = GetAchievementItem(3, false);
        currentTycoonItem = GetAchievementItem(4, false);
        currentExplosivesExpertItem = GetAchievementItem(5, false);
        currentTirelessItem = GetAchievementItem(6, false);

        // p mech
        builtMech = PlayerPrefs.GetInt("builtMech", 0);
        // p upgrades
        totalUpgrades = PlayerPrefs.GetInt("totalUpgrades", 0);
        // blueprint, mechdatacontroller
        mechBlueprints = DataController.ins.MechData.mechList.Count;
        // p titanium ores
        titaniumOres = PlayerPrefs.GetInt("titaniumOres", 0);
        // total gold, player data: gold
        totalGold = (int)DataController.ins.PlayerData.playerData.gold;
        // p time bombs
        defusedTimeBombs = PlayerPrefs.GetInt("defusedTimeBombs", 0);
        // endless mode time, player data: longest time
        minutesInEndless = (int)DataController.ins.PlayerData.playerData.longestTime;
    }       

    // Temp
    public void Delete()
    {
        Data.DeleteFile(filename);
    }
    #endregion
      
    #region AchievementMethods

    AchievementSubItem currentBuilderItem, 
    currentQualityBuilderItem,
    currentCollectorItem,
    currentMinerItem, 
    currentTycoonItem,
    currentExplosivesExpertItem,
    currentTirelessItem;

    public void BuilderChecker(int compareValue)
    {
        Debug.Log("Build! Compare Value " + compareValue + ", current value " + currentBuilderItem);

        if(currentBuilderItem != null && currentBuilderItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentBuilderItem.isAchieved = true;
            // get the next achievement condition
            currentBuilderItem = achievementCollection.FirstOrDefault(item => item.ID == 0).achievementSubItem.FirstOrDefault(subItem => subItem.isAchieved == false);

            AchievementUI();
        }
    }

    public void QualityBuilderChecker(int compareValue)
    {        
        if(currentQualityBuilderItem != null && currentQualityBuilderItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentQualityBuilderItem.isAchieved = true;
            // get the next achievement condition
            currentQualityBuilderItem = achievementCollection.FirstOrDefault(item => item.ID == 1).achievementSubItem.FirstOrDefault(subItem => subItem.isAchieved == false);   

            AchievementUI();
        }
    }

    public void CollectorChecker(int compareValue)
    {        
        if (currentCollectorItem != null && currentCollectorItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentCollectorItem.isAchieved = true;
            // get the next achievement condition
            currentCollectorItem = achievementCollection.FirstOrDefault(item => item.ID == 2).achievementSubItem.FirstOrDefault(subItem => subItem.isAchieved == false);

            AchievementUI();
        }
    }

    public void MinerChecker(int compareValue)
    {        
        if (currentMinerItem != null && currentMinerItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentMinerItem.isAchieved = true;
            // get the next achievement condition
            currentMinerItem = achievementCollection.FirstOrDefault(item => item.ID == 3).achievementSubItem.FirstOrDefault(subItem => subItem.isAchieved == false);

            AchievementUI();
        }
    }

    public void TycoonChecker(int compareValue)
    {        
        if (currentTycoonItem != null && currentTycoonItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentTycoonItem.isAchieved = true;
            // get the next achievement condition
            currentTycoonItem = GetAchievementItem(4, false);

            AchievementUI();
        }
    }

    public void ExplosivesExpertChecker(int compareValue)
    {        
        if (currentExplosivesExpertItem != null && currentExplosivesExpertItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentExplosivesExpertItem.isAchieved = true;
            // get the next achievement condition
            currentExplosivesExpertItem = GetAchievementItem(5, false);

            AchievementUI();
        }
    }

    public void TirelessChecker(int compareValue)
    {        
        if (currentTirelessItem != null && currentTirelessItem.achieventCondition == compareValue)
        {
            // set to true if achievement is achieved
            currentTirelessItem.isAchieved = true;
            // get the next achievement condition
            currentTirelessItem = GetAchievementItem(6, false);

            AchievementUI();
        }
    }

    #endregion

    #endregion

    public AchievementSubItem GetAchievementItem(int ID, bool isAchieved)
    {
        return achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.FirstOrDefault(subItem => subItem.isAchieved == isAchieved);
    }

    public int GetUnclaimedReward(int ID)
    {
        if (achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.FirstOrDefault(subItem => subItem.isRewardClaimed == false && subItem.isAchieved == true) != null)
            return achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.FirstOrDefault(subItem => subItem.isRewardClaimed == false && subItem.isAchieved == true).creditReward;
        else
            return 0;
    }

    public void RewardAcquired(int ID)
    {
        if (achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.FirstOrDefault(subItem => subItem.isRewardClaimed == false && subItem.isAchieved == true) != null)
        {
            achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.FirstOrDefault(subItem => subItem.isRewardClaimed == false && subItem.isAchieved == true).isRewardClaimed = true;
        }
    }

    public bool IsAllAchievementComplete(int ID)
    {       
        Debug.Log("Completed " + achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.Where(item => item.isAchieved == true).Count());
        Debug.Log("Total achievement " + achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.Count);

        return achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.Where(item => item.isAchieved == true).Count() ==
        achievementCollection.FirstOrDefault(item => item.ID == ID).achievementSubItem.Count ? true : false;
    }

    #if UNITY_EDITOR
    [ContextMenu("Delete Achievement Data")]
    public void DeleteAchievementData()
    {
        Data.DeleteFile(filename);
        PlayerPrefs.DeleteKey("builtMech");
        PlayerPrefs.DeleteKey("totalUpgrades");
        PlayerPrefs.DeleteKey("titaniumOres");
        PlayerPrefs.DeleteKey("defusedTimeBombs");
        Debug.Log("Achievements Data Deleted!");
    }
    #endif
}

[System.Serializable]
public abstract class AbstractAchievement
{
    public int ID;
    public string AchievementName;
    public string Description;
}
