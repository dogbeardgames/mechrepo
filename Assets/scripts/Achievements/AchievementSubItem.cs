﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AchievementSubItem {

	#region PubliC Variables

    public bool isAchieved, 
        isRewardClaimed;

    public int achieventCondition,
        creditReward;

    #endregion
}
