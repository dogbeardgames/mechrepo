﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AchievementCollection : AbstractAchievement {    

	#region Public Variables
    public List<AchievementSubItem> achievementSubItem;

//    public AchievementCollection()
//    {
//        achievementSubItem = new List<AchievementSubItem>();
//    }

    #endregion
}
