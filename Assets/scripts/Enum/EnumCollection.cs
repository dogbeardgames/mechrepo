﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumCollection : MonoBehaviour {

	#region Public Variables

    public enum Language
    {
        English = 0, Japanese = 1, Traditional_Chinese = 2, Korean = 3
    }

    public enum StoreCurrency 
    {
        Gold, Credits
    }

    #endregion
}
