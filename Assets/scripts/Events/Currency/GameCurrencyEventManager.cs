﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCurrencyEventManager : MonoBehaviour {

    #region MONO

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () 
    {

    }

    #endregion  

    #region Public Methods

    public static GameCurrencyEventManager Instance;

    public delegate void UpdateGold();
    public static event UpdateGold OnGoldUpdate;

    public delegate void UpdateCredit();
    public static event UpdateCredit OnCreditUpdate;

    // add gold
    public void AddGoldValue(uint value)
    {
        DataController.ins.PlayerData.playerData.gold += value;
        GoldEvent();
    }
    // deduct gold
    public void ReduceGoldValue(uint value)
    {
        DataController.ins.PlayerData.playerData.gold -= value;
        GoldEvent();
    }
    // add credit
    public void AddCreditValue(uint value)
    {
        DataController.ins.PlayerData.playerData.credits += value;
        CreditEvent();
    }

    // reduce credit
    public void ReduceCreditValue(uint value)
    {
        DataController.ins.PlayerData.playerData.credits -= value;
        CreditEvent();
    }
    #endregion

    #region Private Methods

    private void GoldEvent()
    {
        if (OnGoldUpdate != null)   
            OnGoldUpdate();
    }

    private void CreditEvent()
    {
        if (OnCreditUpdate != null)
            OnCreditUpdate();
    }

    #endregion
}
