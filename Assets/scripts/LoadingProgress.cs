﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public sealed class LoadingProgress : MonoBehaviour {
    public static LoadingProgress ins;
  
    [SerializeField]
    RectTransform progressBar;
    [SerializeField]
    Image progressImage;

    void Awake()
    {
        ins = this;
        DontDestroyOnLoad(gameObject);
    }

  

    public void LoadOperation(AsyncOperation op)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        progressBar.sizeDelta = new Vector2(0, progressBar.sizeDelta.y);
        StartCoroutine(IELoad(op));
        //StartCoroutine(IELoadTest());
      
    }
    IEnumerator IELoad(AsyncOperation op)
    {
        //float maxWidth = progressBar.parent.GetComponent<RectTransform>().rect.size.x;
        while (!op.isDone)
        {
            //progressBar.sizeDelta = new Vector2(maxWidth * op.progress, progressBar.sizeDelta.y);
            progressImage.fillAmount = op.progress / 0.9f;
            yield return op.isDone;
        }

        transform.GetChild(0).gameObject.SetActive(false);
    }

    IEnumerator IELoadTest()
    {
        float maxWidth = progressBar.parent.GetComponent<RectTransform>().rect.size.x;
        float progress = 0;
        while (progressBar.sizeDelta.x < maxWidth)
        {
           
            progressBar.sizeDelta = new Vector2(maxWidth * progress, progressBar.sizeDelta.y);
            progress += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }

        transform.GetChild(0).gameObject.SetActive(false);
    }
}
