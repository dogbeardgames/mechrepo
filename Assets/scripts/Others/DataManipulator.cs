﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManipulator : MonoBehaviour {

    [SerializeField]
    GameObject PanelTest;

	// Use this for initialization
	void Start () 
    {
		
	}


    public void Gold()
    {
//        DataController.ins.PlayerData.playerData.gold += 50000;
        GameCurrencyEventManager.Instance.AddGoldValue(50000);
    }

    public void Credits()
    {
//        DataController.ins.PlayerData.playerData.credits += 50000;
        GameCurrencyEventManager.Instance.AddCreditValue(50000);
    }

    public void Back()
    {
        PanelTest.SetActive(false);
    }
}
