﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteData : MonoBehaviour {

    [SerializeField]
    GameObject dataController;

	// Use this for initialization
	void Start () {
		
	}
	
//	// Update is called once per frame
//	void Update () {
//		
//	}

    public void Delete_Data()
    {
        PlayerPrefs.DeleteAll();
        DataController.ins.MechData.mechList.Clear();
    }

    [ContextMenu("Delete All Data")]
    public void DeleteDataContext()
    {
        PlayerPrefs.DeleteAll();
        DataController.ins.MechData.mechList.Clear();
    }

    [ContextMenu("Delete Mech")]
    public void Delete_Mech()
    {        
        dataController.GetComponent<MechDataController>().Delete();
    }

    [ContextMenu("Delete Player")]
    public void Delete_PlayerData()
    {        
        dataController.GetComponent<PlayerDataController>().Delete();
    }

    [ContextMenu("Delete Achievement")]
    public void Delete_Achievement()
    {        
        dataController.GetComponent<AchievementsManager>().Delete();
        PlayerPrefs.DeleteKey("builtMech");
        PlayerPrefs.DeleteKey("totalUpgrades");
        PlayerPrefs.DeleteKey("titaniumOres");
        PlayerPrefs.DeleteKey("defusedTimeBombs");
        Debug.Log("Deleted Achievement Data");
    }
}
