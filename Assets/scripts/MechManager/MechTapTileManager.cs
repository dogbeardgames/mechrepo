﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum EnumPartType {PIECE_0 = 0, PIECE_1 = 1, PIECE_2 = 2, PIECE_3 = 3, PIECE_4 = 4, PIECE_5 = 5, PIECE_6 = 6, PIECE_7 = 7, PIECE_8 = 8, PIECE_9 = 9};
public enum EnumMech {MECH_0 = 0, MECH_1 = 1, MECH_2 = 2, MECH_3 = 3, MECH_4 = 4, MECH_5 = 5, MECH_6 = 6, MECH_7 = 7, MECH_8 = 8, MECH_9 = 9 };
/*
currently static to 9:18 aspect ratio
*/

public class MechTapTileManager : MonoBehaviour {
	public static MechTapTileManager ins;

    static float speed = 1f;
    static float tileSpeed = 0.0286258f;
    static float spawnTime = 1f;
    const float spritePixelSize = 146f;
    public static float originalSpeed;

    [SerializeField]
	MechTileSpawner2 spawner;

    public EnumMech mechToAssemble;
	public EnumMech nextMechToAssemble;
	public EnumMech[] arrMechToAssemble;
	public List<byte> lstMissedPartIndex; //store by index
	public byte currentMissedPartIndex;

	[SerializeField]
	EnumPartType mechPartToTap;
	static int numOfMechParts;
	static EnumPartType tappedMechPart;

	[SerializeField]
	List<Sprite> lstMech_0_Sprite;
	[SerializeField]
	List<Sprite> lstMech_1_Sprite;
	[SerializeField]
	List<Sprite> lstMech_2_Sprite;
    [SerializeField]
    List<Sprite> lstMech_3_Sprite;
    [SerializeField]
    List<Sprite> lstMech_4_Sprite;
    [SerializeField]
    List<Sprite> lstMech_5_Sprite;
    [SerializeField]
    List<Sprite> lstMech_6_Sprite;
    [SerializeField]
    List<Sprite> lstMech_7_Sprite;
    [SerializeField]
    List<Sprite> lstMech_8_Sprite;
    [SerializeField]
    List<Sprite> lstMech_9_Sprite;

	[SerializeField]
	byte mechSequence = 0; //0,1,2,3,4,5,6,7 ,torso,legR,legL,armR,armL,head
	static byte arrMechIndex = 0;
    private int comboCntr = 0;
    //[SerializeField]
    //List<Mech> lstOwnedMech;

    public static float Speed{
		get{
				return speed;
		}
		set{ 
			speed = value;

            //spawnTime -= spawnTime * (speed/100f); July 31, 2017
            spawnTime = (speed / 100f);
            tileSpeed = MonoExtension.GetSpeed(spritePixelSize / 100, spawnTime);
        }
	}

	public static float GetSpawnTime(){
		return spawnTime;
	}

	public static float GetTileSpeed(){
        return tileSpeed;
	}
	
	public EnumPartType GetMechPartToTap()
	{
			return mechPartToTap;
	}

	public MechTileSpawner2 GetSpawner(){
        return spawner;
	}

	public byte GetTapSequence()
	{
			return mechSequence;
	}

	void Awake()
	{
		ins = this;
       
	}

    void Start()
    {
        //LOAD MECHS
        //lstOwnedMech = DataController.ins.MechData.mechList;
        UpdateMechToAssebleList();
        spawner.PreLoad(24);
        spawner.SupplyBoxManager().PreLoad(2);
        spawner.SpecialTilesManager().PreLoadBomb(3);
        spawner.SpecialTilesManager().PreLoadOre(2);
    }

    void OnEnable()
    {
        arrMechToAssemble = new EnumMech[DataController.ins.MechData.mechList.Count];
        for (int i = 0; i < DataController.ins.MechData.mechList.Count; i++)
        {
            arrMechToAssemble[i] = DataController.ins.MechData.mechList[i].mech;
        }
    }

    void UpdateMechToAssebleList()
    {
        arrMechToAssemble = new EnumMech[DataController.ins.MechData.mechList.Count];
        for (int i=0; i< DataController.ins.MechData.mechList.Count; i++)
        {
            arrMechToAssemble[i] = DataController.ins.MechData.mechList[i].mech;
        }
    }

    public void Reset()
    {
        mechToAssemble = GetRandomMech();//EnumMech.MECH_0;
        currentMissedPartIndex = 0;
        numOfMechParts = 0;
        mechSequence = 0;
        arrMechIndex = 0;
        lstMissedPartIndex.Clear();
        currentMissedPartIndex = 0;

        mechPartToTap = GetPartBySequence();
       // ModeUI.ins.SetMechToAssembleText(mechToAssemble.ToString());

        SetNumOfMechParts(mechToAssemble);
        ModeUI.ins.SetMech((int)mechToAssemble);
      //  ModeUI.ins.SetMechToAssembleText(mechToAssemble.ToString());
        //spawner.Reset(); NOT HERE BECAUSE OF BOMB

        SpecialTilesManager.isGoldMultiplierActive = false;
        PlayerGameModeData.numOfCombo = 0;
        // ModeUI.ins.SetComboText(PlayerGameModeData.numOfCombo.ToString() + " build combo");
        ModeUI.ins.SetComboText("");
    }

    GAME_MODE Mode()
    {
        return GameManager.mode == GAME_MODE.QUICK ? GAME_MODE.QUICK : GAME_MODE.ENDLESS;
    }

	public void StartSpawning()
	{
        //mechPartToTap = EnumPartType.PIECE_1;
        //mechToAssemble = arrMechToAssemble [0];//EnumMechType.MECH_1;
        //mechToAssemble = EnumMech.MECH_0;

        UpdateMechToAssebleList();

        ShuffleMechCollection();
        Reset();
        spawner.Reset();

        // if tutorial
        if(!PlayerPrefs.HasKey("LanguageSelect") || TutorialManager.isReplay)
        {
            Debug.Log("Tutorial Set to Vestar");
            mechToAssemble = EnumMech.MECH_0;
            ModeUI.ins.SetMech(0);
        }
        // if game
        else
        {
            ModeUI.ins.SetMech((int)mechToAssemble);
        }            

       // ModeUI.ins.SetMechToAssembleText(mechToAssemble.ToString());

		SetNumOfMechParts (mechToAssemble);
		
		spawner.StartSpawning (true);

    }

	public void SetNumOfMechParts(EnumMech type)
	{
			switch(type)
			{
			case EnumMech.MECH_0:
				numOfMechParts = lstMech_0_Sprite.Count;
				break;
			case EnumMech.MECH_1:
				numOfMechParts = lstMech_1_Sprite.Count;
				break;
			case EnumMech.MECH_2:
				numOfMechParts =lstMech_2_Sprite.Count;
				break;
            case EnumMech.MECH_3:
                numOfMechParts = lstMech_3_Sprite.Count;
                break;
            case EnumMech.MECH_4:
                numOfMechParts = lstMech_4_Sprite.Count;
                break;
            case EnumMech.MECH_5:
                numOfMechParts = lstMech_5_Sprite.Count;
                break;
            case EnumMech.MECH_6:
                numOfMechParts = lstMech_6_Sprite.Count;
                break;
            case EnumMech.MECH_7:
                numOfMechParts = lstMech_7_Sprite.Count;
                break;
            case EnumMech.MECH_8:
                numOfMechParts = lstMech_8_Sprite.Count;
                break;
            case EnumMech.MECH_9:
                numOfMechParts = lstMech_9_Sprite.Count;
                break;
        }
	}

	public void SetTappedMechPart(EnumPartType part, EnumMech type, Vector2 tilePosition)
	{

        Debug.Log("Mech part to tap: " + mechPartToTap.ToString() + ", my part " + part.ToString() + "type: " + mechToAssemble.ToString() + ", my type: " + type.ToString());

		tappedMechPart = part;
		if (tappedMechPart != mechPartToTap || type != mechToAssemble) {
            ModeUI.ins.BuildDestroy();
            DestroyCurrentMech();
            PlayerGameModeData.numOfMechDestroyed++;
            SoundManager.Instance.PlaySFX ("seXPiece");

			if (GameManager.mode == GAME_MODE.ENDLESS) 
			{
					ModeUI.ins.SetLifeTextDecreased (1);
			}

            PlayerGameModeData.numOfCombo = 0;

            // ModeUI.ins.SetComboText(PlayerGameModeData.numOfCombo.ToString() + " build combo");
            ModeUI.ins.SetComboText("");

            // Reset Supply Box Gold Multiplier
            //SupplyBoxManager.isGoldMultiplierActive = false;
            SpecialTilesManager.isGoldMultiplierActive = false;
            print ("BAD!");
            //spawner.ResetAlgo3();
            //spawner.SetMechToAssemble();
        }
        else
        {
            Correct(tilePosition);
        }
	}

    public void Correct(Vector2 tilePosition)
    {
        if (mechSequence < GetMechSpriteList(mechToAssemble).Count)
        {
            mechSequence++;
            SoundManager.Instance.PlaySFX("seOkPiece");
        }
        mechPartToTap = GetPartBySequence();
        ModeUI.ins.BuildMech(GetMechToAssembleIndex());

        print("GOOD!");
        //spawner.ResetAlgo3();
       
        // mech is assembled
        if (mechSequence >= GetMechSpriteList(mechToAssemble).Count)
        {
            ModeUI.ins.BuildComplete();
            // Check Supply box gold mutiplier if enabled
            if(SpecialTilesManager.isGoldMultiplierActive)
            {
                // multiplier value temporary
               
                ModeUI.ins.SetGoldTextIncreased(GetScore(mechToAssemble) * SpecialTilesManager.GetGoldMultiplier());
                UI_FX_Manager.ins.SpawnScore(tilePosition, ("+" + GetScore(mechToAssemble) * SpecialTilesManager.GetGoldMultiplier()).ToString(), Color.blue);
                Debug.Log("Gold mutiplier used! Will reset to false");

                // achievements
                DataController.ins.AchievementsDataManager.TycoonChecker(DataController.ins.AchievementsDataManager.TotalGold);
                //
              
            }
            else
            {
                ModeUI.ins.SetGoldTextIncreased(GetScore(mechToAssemble));
                
                UI_FX_Manager.ins.SpawnScore(tilePosition, "+" + GetScore(mechToAssemble).ToString(), Color.blue);
              
              
            }

          
            
            PlayerGameModeData.numOfCombo++;
            PlayerGameModeData.numOfMechCompleted++;

            if (PlayerGameModeData.numOfCombo > 1)
            {
                if (GameManager.mode == GAME_MODE.QUICK)
                {
                    ModeUI.ins.SetGoldTextIncreased(GameManager.quickComboBonus);
                    UI_FX_Manager.ins.SpawnScore(new Vector2(tilePosition.x, tilePosition.y - 0.4f), "+" + GameManager.quickComboBonus.ToString(), Color.blue);
                }
                else
                {
                    ModeUI.ins.SetGoldTextIncreased(GameManager.endlessComboBonus);
                    UI_FX_Manager.ins.SpawnScore(new Vector2(tilePosition.x, tilePosition.y - 0.4f), "+" + GameManager.endlessComboBonus.ToString(), Color.blue);
                }
            }

            // Achievements
            DataController.ins.AchievementsDataManager.BuiltMech++;

            Debug.Log("MechTapTileManager, Built Mech Count " + DataController.ins.AchievementsDataManager.BuiltMech); 

            DataController.ins.AchievementsDataManager.BuilderChecker(DataController.ins.AchievementsDataManager.BuiltMech);
            //

            if (PlayerGameModeData.numOfCombo >= 2)
            {
                if (PlayerGameModeData.numOfCombo >= PlayerGameModeData.numOfMaxComboMade)
                {
                    PlayerGameModeData.numOfMaxComboMade = PlayerGameModeData.numOfCombo;

                    //if(GameManager.mode == GAME_MODE.ENDLESS)
                    //    DataController.ins.PlayerData.playerData.endlessMaxCombo = PlayerGameModeData.numOfMaxComboMade;
                    //else
                    //    DataController.ins.PlayerData.playerData.quickMaxCombo = PlayerGameModeData.numOfMaxComboMade;
                }
            
               
                ModeUI.ins.SetComboText(PlayerGameModeData.numOfCombo.ToString() + " build combo");
                
                ModeUI.ins.Combo();
            }
           // SoundManager.Instance.PlaySFX("Pickup_Coin");
            DestroyCurrentMech();
        }
    }
  
    public void ShuffleMechCollection()
    {
        //for (int i = 0; i < arrMechToAssemble.Length; i++)
        //{
        //    arrMechToAssemble[i] = GetRandomMech();
        //}

        int _rnd = 0;
        EnumMech _temp;

        for (int i = 0; i < arrMechToAssemble.Length; i++)
        {
            _temp = arrMechToAssemble[i];
            _rnd = Random.Range(0, arrMechToAssemble.Length);
            arrMechToAssemble[i] = arrMechToAssemble[_rnd];
            arrMechToAssemble[_rnd] = _temp;
        }
    }

    public void DestroyCurrentMech()
	{
        mechSequence = 0;

        mechToAssemble = spawner.GetMechToAssemble(); //arrMechToAssemble[++arrMechIndex < arrMechToAssemble.Length ? arrMechIndex : (arrMechIndex -= arrMechIndex)]; //spawner.GetMechToAssemble(); //GetRandomMech(); // arrMechToAssemble[++arrMechIndex < arrMechToAssemble.Length ? arrMechIndex : (arrMechIndex -= arrMechIndex)];
        mechPartToTap = GetPartBySequence();

        //ModeUI.ins.SetMechToAssembleText(mechToAssemble.ToString());


        SetNumOfMechParts(mechToAssemble);
        ModeUI.ins.SetMech((int)mechToAssemble);

        //spawner.ResetAlgo3();
        //ShuffleMechCollection();

        // reset gold multiplier

        SpecialTilesManager.isGoldMultiplierActive = false;

        /*
		//MechTileSpawner3.ins.DisableAllTiles ();
		//MechTileSpawner3.ins.SpawnMechPuzzle (mechToAssemble);
		*/
    }

#region sprite getters

    public Sprite GetMech_0_Sprite(int index)
	{
			//returns index out of range on change mech--> workaround below
			if (index >= lstMech_0_Sprite.Count)
					index = 0;
			return lstMech_0_Sprite [index];
	}

	public Sprite GetMech_1_Sprite(int index)
	{
			if (index >= lstMech_1_Sprite.Count)
					index = 0;
			return lstMech_1_Sprite [index];
	}

	public Sprite GetMech_2_Sprite(int index)
	{
			if (index >= lstMech_2_Sprite.Count)
					index = 0;
			return lstMech_2_Sprite[index];
	}

    public Sprite GetMech_3_Sprite(int index)
    {
        if (index >= lstMech_3_Sprite.Count)
            index = 0;
        return lstMech_3_Sprite[index];
    }

    public Sprite GetMech_4_Sprite(int index)
    {
        if (index >= lstMech_4_Sprite.Count)
            index = 0;
        return lstMech_4_Sprite[index];
    }

    public Sprite GetMech_5_Sprite(int index)
    {
        if (index >= lstMech_5_Sprite.Count)
            index = 0;
        return lstMech_5_Sprite[index];
    }

    public Sprite GetMech_6_Sprite(int index)
    {
        if (index >= lstMech_6_Sprite.Count)
            index = 0;
        return lstMech_6_Sprite[index];
    }

    public Sprite GetMech_7_Sprite(int index)
    {
        if (index >= lstMech_7_Sprite.Count)
            index = 0;
        return lstMech_7_Sprite[index];
    }

    public Sprite GetMech_8_Sprite(int index)
    {
        if (index >= lstMech_8_Sprite.Count)
            index = 0;
        return lstMech_8_Sprite[index];
    }

    public Sprite GetMech_9_Sprite(int index)
    {
        if (index >= lstMech_9_Sprite.Count)
            index = 0;
        return lstMech_9_Sprite[index];
    }

    #endregion


    public float GetScore(EnumMech mech)
    {
        Mech _m = DataController.ins.MechData.mechList.Where(mData => mData.mech == mech).Single();
       
        return Mode() == 0 ? _m.buildRewardQuick : _m.buildRewardEndless;

        //do not return if list is not in sequential
        //switch (mech)
        //{
        //    case EnumMech.MECH_0:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[0].buildRewardQuick : DataController.ins.MechData.mechList[0].buildRewardEndless;
        //    case EnumMech.MECH_1:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[1].buildRewardQuick : DataController.ins.MechData.mechList[1].buildRewardEndless;
        //    case EnumMech.MECH_2:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[2].buildRewardQuick : DataController.ins.MechData.mechList[2].buildRewardEndless;
        //    case EnumMech.MECH_3:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[3].buildRewardQuick : DataController.ins.MechData.mechList[3].buildRewardEndless;
        //    case EnumMech.MECH_4:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[4].buildRewardQuick : DataController.ins.MechData.mechList[4].buildRewardEndless;
        //    case EnumMech.MECH_5:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[5].buildRewardQuick : DataController.ins.MechData.mechList[5].buildRewardEndless;
        //    case EnumMech.MECH_6:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[6].buildRewardQuick : DataController.ins.MechData.mechList[6].buildRewardEndless;
        //    case EnumMech.MECH_7:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[7].buildRewardQuick : DataController.ins.MechData.mechList[7].buildRewardEndless;
        //    case EnumMech.MECH_8:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[8].buildRewardQuick : DataController.ins.MechData.mechList[8].buildRewardEndless;
        //    case EnumMech.MECH_9:
        //        return Mode() == 0 ? DataController.ins.MechData.mechList[9].buildRewardQuick : DataController.ins.MechData.mechList[9].buildRewardEndless;

        //}
        //return 0;
    }

    public int GetMechToAssembleIndex()
    {
        switch (mechToAssemble)
        {
            case EnumMech.MECH_0:
                return 0;
            case EnumMech.MECH_1:
                return 1;
            case EnumMech.MECH_2:
                return 2;
            case EnumMech.MECH_3:
                return 3;
            case EnumMech.MECH_4:
                return 4;
            case EnumMech.MECH_5:
                return 5;
            case EnumMech.MECH_6:
                return 6;
            case EnumMech.MECH_7:
                return 7;
            case EnumMech.MECH_8:
                return 8;
            case EnumMech.MECH_9:
                return 9;
            default:
                return 0;
        }
    }

    public EnumMech GetRandomMech()
	{
        //return (EnumMech)Random.Range (0, System.Enum.GetValues (typeof(EnumMech)).Length);
        return arrMechToAssemble[Random.Range(0,arrMechToAssemble.Length)];
    }

	public List<Sprite> GetMechSpriteList(EnumMech mechType)
	{
		switch(mechType)
		{
	        case EnumMech.MECH_0:
		        return lstMech_0_Sprite;
	        case EnumMech.MECH_1:
		        return lstMech_1_Sprite;
	        case EnumMech.MECH_2:
                return lstMech_2_Sprite;
            case EnumMech.MECH_3:
                return lstMech_3_Sprite;
            case EnumMech.MECH_4:
                return lstMech_4_Sprite;
            case EnumMech.MECH_5:
                return lstMech_5_Sprite;
            case EnumMech.MECH_6:
                return lstMech_6_Sprite;
            case EnumMech.MECH_7:
                return lstMech_7_Sprite;
            case EnumMech.MECH_8:
                return lstMech_8_Sprite;
            case EnumMech.MECH_9:
                return lstMech_9_Sprite;

        }
		return null;
	}

	EnumPartType GetPartBySequence()
	{
		if (mechSequence == 0) {
			return EnumPartType.PIECE_0;
		} else if (mechSequence == 1) {
			return EnumPartType.PIECE_1;
		} else if (mechSequence == 2) {
			return EnumPartType.PIECE_2;
		} else if (mechSequence == 3) {
			return EnumPartType.PIECE_3;
		} else if (mechSequence == 4) {
			return EnumPartType.PIECE_4;
		} else if (mechSequence == 5) {
			return EnumPartType.PIECE_5;
        }else if (mechSequence == 6){
            return EnumPartType.PIECE_6;
        }else if(mechSequence == 7) {
			return EnumPartType.PIECE_7;
		}else if(mechSequence == 8) {
			return EnumPartType.PIECE_8;
		}else if (mechSequence == 9){
            return EnumPartType.PIECE_9;
        }
        return EnumPartType.PIECE_0;
	}

    public EnumPartType GetPartBySequence(int index)
    {
        if (index == 0)
        {
            return EnumPartType.PIECE_0;
        }
        else if (index == 1)
        {
            return EnumPartType.PIECE_1;
        }
        else if (index == 2)
        {
            return EnumPartType.PIECE_2;
        }
        else if (index == 3)
        {
            return EnumPartType.PIECE_3;
        }
        else if (index == 4)
        {
            return EnumPartType.PIECE_4;
        }
        else if (index == 5)
        {
            return EnumPartType.PIECE_5;
        }
        else if (index == 6)
        {
            return EnumPartType.PIECE_6;
        }
        else if (index == 7)
        {
            return EnumPartType.PIECE_7;
        }
        else if (index == 8)
        {
            return EnumPartType.PIECE_8;
        }
        else if (index == 9)
        {
            return EnumPartType.PIECE_9;
        }
        return EnumPartType.PIECE_0;
    }

    public byte GetMissedPartIndex()
	{
		byte part = 0;
		if(lstMissedPartIndex.Count > 0){
				part = lstMissedPartIndex [0];
		}
		lstMissedPartIndex.RemoveAt(0);
        //print("RETURN MISSED! = " + part);
		return part;

	}

	public void FreePassTiles(Transform tile)
	{
		foreach(GameObject g in spawner.GetTilePooler().objLst)
		{			
            if(g.GetComponent<PieceTile>())
            {
                g.GetComponent<PieceTile> ().freePass = g.transform.position.y <= tile.position.y ? true : false;
            }
            else
            {
                g.GetComponent<TutorialTile> ().freePass = g.transform.position.y <= tile.position.y ? true : false;
            }
		}
	}


}
