﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeSelector : MonoBehaviour {

	[SerializeField]
	GameObject quickUI, endlessUI;
	void Start () {
				if (GameManager.mode == GAME_MODE.QUICK)
						Instantiate (quickUI, GameObject.Find("Canvas").transform);
				else
						Instantiate (endlessUI, GameObject.Find("Canvas").transform);
	}
	
	
}
