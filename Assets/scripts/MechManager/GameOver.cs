﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{

    [SerializeField]
    TextMeshProUGUI txtGold, txtCombo, txtAssembled, txtTime;
    [SerializeField]
    GameObject crownGold, crownCombo, crownNumAssemble, crownTime;

    [SerializeField]
    Button btnAd;

    static long goldScore = 100;
    static string _combo = "30", _assembled = "31", _time = "32";

    void OnEnable()
    {
        crownGold.SetActive(false);
        crownCombo.SetActive(false);
        crownNumAssemble.SetActive(false);
        crownTime.SetActive(false);
    }

    public void SetResults(string gold, string combo, string assembled, string time)
    {
        NewRecord();
        _combo = combo;
        _assembled = assembled;
        _time = time;

        SoundManager.Instance.BGM.Stop();
        SoundManager.Instance.PlaySFX("seTap");
        txtGold.text = "gold: <color=#6CFD00FF>" + gold + "</color>";
        txtCombo.text = "max combo: <color=#6CFD00FF>" + combo + "</color>";
        txtAssembled.text = "mech assembled: <color=#6CFD00FF>" + assembled + "</color>";
        txtTime.text = GameManager.mode == GAME_MODE.QUICK ? "time(quick): " + time : "time(endless): " + "<color=#6CFD00FF>" + time + "</color>";

        long.TryParse(gold, out goldScore);

        // save gold
//        DataController.ins.PlayerData.playerData.gold += (uint)goldScore;
        GameCurrencyEventManager.Instance.AddGoldValue((uint)goldScore);


#if UNITY_IOS
        // check game type
        if (GameManager.mode == GAME_MODE.QUICK)
        {
            // Quick game mode
            // Post score to google/ios
            if(LeaderboardManager.isLoggedIn)
                LeaderboardManager.ReportScore(goldScore, LeaderboardManager.iosQuickLeaderboard);
        }
        else
        {
            // Endless game mode
            // Post score to google/ios
            if(LeaderboardManager.isLoggedIn)
                LeaderboardManager.ReportScore(goldScore, LeaderboardManager.iosEndlessLeaderboard);
		}
#endif

#if UNITY_ANDROID
		// check game type
		if (GameManager.mode == GAME_MODE.QUICK)
		{
			// Quick game mode
			// Post score to google/ios
			LeaderboardManager.ReportScore(goldScore, LeaderboardManager.androidQuickLeaderboard);
		}
		else
		{
			// Endless game mode
			// Post score to google/ios
			LeaderboardManager.ReportScore(goldScore, LeaderboardManager.androidEndlessLeaderboard);
		}
#endif
    }

    public void SetResults2X(string gold, string combo, string assembled, string time)
    {
        _combo = combo;
        _assembled = assembled;
        _time = time;


        NewRecord();

        SoundManager.Instance.BGM.Stop();
        SoundManager.Instance.PlaySFX("seTap");
        txtGold.text = "gold: <color=#6CFD00FF>" + gold + "</color>";
        txtCombo.text = "max combo: <color=#6CFD00FF>" + combo + "</color>";
        txtAssembled.text = "mech assembled: <color=#6CFD00FF>" + assembled + "</color>";
        txtTime.text = GameManager.mode == GAME_MODE.QUICK ? "time(quick): " + time : "time(endless): " + "<color=#6CFD00FF>" + time + "</color>";

//        long.TryParse(gold, out goldScore);
    }

    public void PlayAgain()
    {
        SoundManager.Instance.PlaySFX("seTap");
        MechTapTileManager.ins.Reset();
        MechTapTileManager.ins.GetSpawner().Reset();
        //PlayerGameModeData.ResetGameModeData();
        GameManager.gameOver = false;
        ModeUI.ins.startMode = true;
        if (GameManager.mode == GAME_MODE.QUICK)
        {
            ModeQuickGameManager.instance.StartQuickGameMode();
            SoundManager.Instance.BGM.Stop();
            SoundManager.Instance.PlayBGM("bgmQuick", true);
        }
        else
        {
            ModeEndlessGameManager.instance.StartEndlessGameMode();
            SoundManager.Instance.BGM.Stop();
            SoundManager.Instance.PlayBGM("bgmEndless", true);
        }
    }

    public void ShowMenu()
    {
        SoundManager.Instance.PlayBGM("bgmStandard", true);
        SoundManager.Instance.PlaySFX("seTap");
        GameManager.gameOver = false;
        GameManager.paused = false;
        Tile.move = false;
        MechTapTileManager.ins.GetSpawner().Reset();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        SoundManager.Instance.BGM.Stop();
    }

    void NewRecord()
    {
        if (GameManager.mode == GAME_MODE.QUICK)
        {
            if (DataController.ins.PlayerData.playerData.quickHighestGoldEarned < PlayerGameModeData.gold)
            {
                crownGold.SetActive(true);
            }
            if (DataController.ins.PlayerData.playerData.quickMaxCombo < PlayerGameModeData.numOfMaxComboMade)
            {
                DataController.ins.PlayerData.playerData.quickMaxCombo = PlayerGameModeData.numOfMaxComboMade;
                crownCombo.gameObject.SetActive(true);

            }

            if (DataController.ins.PlayerData.playerData.quickMechAssembled < PlayerGameModeData.numOfMechCompleted)
            {
                crownNumAssemble.SetActive(true);
            }

            //if (DataController.ins.PlayerData.playerData.longestTime > ModeEndlessGameManager.GetTime())
            //{
            //    crownTime.SetActive(true);
            //}
        }
        else
        {
            if (DataController.ins.PlayerData.playerData.endlessHighestGoldEarned < PlayerGameModeData.gold)
            {
                crownGold.SetActive(true);
            }
            if (DataController.ins.PlayerData.playerData.endlessMaxCombo < PlayerGameModeData.numOfMaxComboMade)
            {
                crownCombo.gameObject.SetActive(true);
                DataController.ins.PlayerData.playerData.endlessMaxCombo = PlayerGameModeData.numOfMaxComboMade;
            }

            if (DataController.ins.PlayerData.playerData.endlessMechAssembled < PlayerGameModeData.numOfMechCompleted)
            {
                crownNumAssemble.SetActive(true);
            }

            if (DataController.ins.PlayerData.playerData.longestTime > ModeEndlessGameManager.GetTime())
            {
                crownTime.SetActive(true);
            }
        }


    }

    public void DoubleReward()
    {
        // disable double reward button
        btnAd.gameObject.SetActive(false);

        GameCurrencyEventManager.Instance.AddGoldValue((uint)goldScore);

        goldScore *= 2;

        SetResults2X(goldScore.ToString(), _combo, _assembled, _time);
    }
}
