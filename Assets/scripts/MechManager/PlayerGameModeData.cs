﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//only used during game mode
public static class PlayerGameModeData {

	public static GAME_MODE mode;
	public static float gold;
	public static uint numOfWrongTap;
	public static uint numOfCorrectTap;
	public static uint numOfMechCompleted;
	public static uint numOfMechDestroyed;
	public static uint numOfTileMissed;
	public static uint numOfCombo;
 
    public static uint numOfMaxComboMade;

    public static void ResetGameModeData()
    {
        gold = 0;
        numOfCombo = 0;
        numOfCorrectTap = 0;
        numOfMechCompleted = 0;
        numOfMechDestroyed = 0;
        numOfTileMissed = 0;
        numOfWrongTap = 0;
     
        numOfMaxComboMade = 0;
    }

	public static void PrintData()
	{
		//print (mode);
		//print (gold);
		//print (numOfWrongTap);
		//print (numOfCorrectTap);
		//print (numOfMechCompleted);
		//print (numOfMechDestroyed);
		//print (numOfTileMissed);
		//print (numOfCombo);
	}



}
