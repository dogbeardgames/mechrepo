﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [SerializeField]
    ButtonSettings settings;
    [SerializeField]
    ButtonStore store;
    [SerializeField]
    ButtonHangar hangar;
    [SerializeField]
    ButtonProfile profile;
    [SerializeField]
    ButtonHelp help;

    [SerializeField]
    GameObject LanguageSelect;

    public static MenuManager Instance;

    #region MONO

    void Awake()
    {
        Instance = this;
    }


	void Start () 
    {
        SoundManager.Instance.PlayBGM("bgmStandard", true);

        // new install, select language
//        if(TutorialManager.Instance != null)
//        {
//            if(TutorialManager.Instance.tutorialType == TutorialManager.TutorialType.Game && !PlayerPrefs.HasKey("LanguageSelect"))
//            {
//                GameObject objLanguageSelect = Instantiate(LanguageSelect, GameObject.Find("Canvas").transform, false);
//                objLanguageSelect.GetComponent<LanguageSelect>().btnOk.onClick.AddListener(() => LanguageSelected(objLanguageSelect));
//            }
//            else
//            {
//                Destroy(TutorialManager.Instance.gameObject);
//            }
//        }

//        if(!PlayerPrefs.HasKey("LanguageSelect"))
//        {
//            GameObject objLanguageSelect = Instantiate(LanguageSelect, GameObject.Find("Canvas").transform, false);
//            objLanguageSelect.GetComponent<LanguageSelect>().btnOk.onClick.AddListener(() => LanguageSelected(objLanguageSelect));
//        }
//        else
//        {
//            if (TutorialManager.Instance == null)
//            {
//                // delete tutorial panel
//                Destroy(TutorialManager.Instance.gameObject);
//            }            
//        }

//         temp, for shop tutorial
//        if(TutorialManager.Instance != null)
//        {
//            if(TutorialManager.Instance.tutorialType == TutorialManager.TutorialType.Shop)
//            {
//                TutorialManager.Instance.CreateShopTutorial();       
//            }
//            else if(TutorialManager.Instance.tutorialType == TutorialManager.TutorialType.Hangar)
//            {
//                TutorialManager.Instance.CreateHangarTutorial();
//            }
//        }
	}
	
    #endregion

    void LanguageSelected(GameObject obj)
    {
        SoundManager.Instance.PlaySFX("seTap");
        //PlayerPrefs.SetInt("LanguageSelect", 1);

        // dropDown value
        EnumCollection.Language enumLanguage = (EnumCollection.Language)obj.GetComponent<LanguageSelect>().dropDownLanguage.value;

        DataController.ins.PlayerData.playerData.language = enumLanguage;

        // Load selected language
        LanguageManager.Instance.Load(enumLanguage);

        // Create Tutorial Manager
        TutorialManager.Instance.CreateGameTutorial();

        Destroy(obj);
    }

	public void SelectQuickGame()
	{
      
        SoundManager.Instance.PlaySFX("seTap");
        GameManager.mode = GAME_MODE.QUICK;
		LoadingProgress.ins.LoadOperation(SceneManager.LoadSceneAsync ("Mech_test"));

        //SoundManager.Instance.PlayBGM("bgmQuick", true);
        SoundManager.Instance.BGM.Stop();
	}
	public void SelectEndlessGame()
	{
        SoundManager.Instance.PlaySFX("seTap");
        GameManager.mode = GAME_MODE.ENDLESS;
        //SoundManager.Instance.PlayBGM("bgmEndless", true);
        SoundManager.Instance.BGM.Stop();
        LoadingProgress.ins.LoadOperation(SceneManager.LoadSceneAsync("Mech_test"));
	}
	public void SelectHangar()
	{
        SoundManager.Instance.PlaySFX("seTap");
        hangar.Show();
	}
	public void SelectSettings()
	{
        SoundManager.Instance.PlaySFX("seTap");
        settings.Show();
	}

    public void SelectProfile()
    {
        SoundManager.Instance.PlaySFX("seTap");
        profile.Show();
    }

    public void SelectStore()
    {
        //SoundManager.Instance.PlaySFX("seTap");
        store.Show();
    }

    public void SelectHelp()
    {
        SoundManager.Instance.PlaySFX("seTap");
        help.Show();
    }
}
