﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupplyBoxManager : MonoBehaviour {
    public static SupplyBoxManager ins;
   
    [SerializeField]
    GameObject supplyBox;

    [SerializeField]
    ObjectPooler supplyPooler;

    [SerializeField]
    List<Sprite> lstSprite;

    static float speedUpDuration = 1.5f;
    static float speedUpTimer = 0;
    static float speedUpValue = 20; //20%
   

    static float slowDownDuration = 1.5f;
    static float slowDownTimer = 0;
    static float slowDownValue = 120; //120%

    public static float tempGameSpeed; //update this when GameSpeed level increased

    public static bool isSpeedBoxActive = false; 

    // checker for Gold multiplier supply box.
    //public static bool isGoldMultiplierActive = false;

    public static float GetSpeedUpTimer()
    {
        return speedUpTimer;
    }

    public static float GetSlowDownTimer()
    {
        return slowDownTimer;
    }

    public Sprite GetSprite(int index)
    {
        return lstSprite[index];
    }

    void Start()
    {
        ins = this;
        
    }

    public void PreLoad(int num)
    {
        supplyPooler.PreLoad(num, supplyBox, new Vector3(0, GameManager.TopScreen(), 0));
    }

    public ObjectPooler SupplyPooler() {
        return supplyPooler;
    }

    void FixedUpdate()
    {
       // print(MechTapTileManager.Speed);
    }

    #region Gold Mutiplier

    public void GoldMutiplier()
    {
        
    }

    #endregion

    #region speed--------------------------------------------------------------------

    public void SlownDown()
    {
        isSpeedBoxActive = true;
        if (speedUpTimer != 0){
            //reset to zero to continue effect
            speedUpTimer = 0;
        
        }
        else{
            StartCoroutine(IESlowDown());
        }
       
    }

    public void SpeedUp()
    {
        isSpeedBoxActive = true;
        if (slowDownTimer != 0) {
            //reset to zero to continue effect
            slowDownTimer = 0;
          
        }
        else{
            StartCoroutine(IESpeedUp());
           
        }
       
    }
   
    IEnumerator IESlowDown()
    {
        //tempGameSpeed = MechTapTileManager.Speed;
       
        while (MechTapTileManager.ins.GetSpawner().GetTimerValue() > 0){
            yield return new WaitForSeconds(0.0000001f);
        }

        //stop slow down if active
        if (slowDownTimer != 0)
        {
            MechTapTileManager.Speed = tempGameSpeed;
          
            StopCoroutine(IESpeedUp());
            slowDownTimer = 0;
         
        }

        MechTapTileManager.Speed += 20f;//speedUpValue;
      
        while (speedUpTimer < speedUpDuration)
        {
            yield return new WaitForSeconds(0.001f);

            if (!GameManager.paused || !GameManager.gameOver)
            {
                speedUpTimer += Time.fixedDeltaTime;
              
            }

        }
        
        while (MechTapTileManager.ins.GetSpawner().GetTimerValue() > 0){
            yield return new WaitForSeconds(0.0000001f);
        }

        isSpeedBoxActive = false;
        speedUpTimer = 0;
        MechTapTileManager.Speed = tempGameSpeed;
    }

    IEnumerator IESpeedUp()
    {

        //tempGameSpeed = MechTapTileManager.Speed;
      
        while (MechTapTileManager.ins.GetSpawner().GetTimerValue() > 0)
        {
            yield return new WaitForSeconds(0.0000001f);
        }

        //stop speed up if active
        if (speedUpTimer != 0)
        {
            MechTapTileManager.Speed = tempGameSpeed;
            StopCoroutine(IESlowDown());
            speedUpTimer = 0;
          
        }


        MechTapTileManager.Speed -= 20f; //slowDownValue;
       
        while (slowDownTimer < slowDownDuration)
        {

            yield return new WaitForSeconds(0.001f);
           
            if (!GameManager.paused || !GameManager.gameOver)
            {
                slowDownTimer += Time.fixedDeltaTime;
              
            }
          
        }
      
        while (MechTapTileManager.ins.GetSpawner().GetTimerValue() > 0){
            yield return new WaitForSeconds(0.0000001f);
        }

        isSpeedBoxActive = false;
        slowDownTimer = 0;
        MechTapTileManager.Speed = tempGameSpeed; //return speed 
    }

    #endregion speed

    public ObjectPooler GetSupplyPooler()
    {
        return supplyPooler;
    }

    public void SpawnSupply(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = supplyPooler.SpawnObject()) == null)
        {
            obj = Instantiate(supplyBox, pos, Quaternion.identity) as GameObject;
            supplyPooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

    public void Reset()
    {
        StopCoroutine(IESlowDown());
        StopCoroutine(IESpeedUp());
        slowDownTimer = 0;
        speedUpTimer = 0;
        isSpeedBoxActive = false;
        //isGoldMultiplierActive = false;
    }


}
