﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialTilesManager : MonoBehaviour {

    public static bool isGoldMultiplierActive = false;

    [SerializeField]
    GameObject bombTile;
    [SerializeField]
    GameObject oreTile;

    [SerializeField]
    ObjectPooler bombPooler;
    [SerializeField]
    ObjectPooler orePooler;

    [SerializeField]
    Sprite refinedOre;

    public Sprite GetRefinedOre()
    {
        return refinedOre;
    }

    public static float GetGoldMultiplier()
    {
        return 1.1f; //replace to 1.1 8-7-2017
    }

    public ObjectPooler GetBombPooler() {
            return bombPooler;
    }

    public ObjectPooler GetOrePooler() {
            return orePooler;
    }

    public void PreLoadBomb(int num)
    {
        bombPooler.PreLoad(num, bombTile, new Vector3(0, GameManager.TopScreen(), 0));
    }

    public void PreLoadOre(int num)
    {
        orePooler.PreLoad(num, oreTile, new Vector3(0, GameManager.TopScreen(), 0));
    }

    public void SpawnRandomTile(Vector2 pos)
    {

        if (Random.Range(0, 100) <= 10)
        {
            SpawnBomb(pos);
        }
        else
        {
            SpawnOre(pos);
        }
    }

    public void SpawnBomb(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = bombPooler.SpawnObject()) == null)
        {
            obj = Instantiate(bombTile, pos, Quaternion.identity) as GameObject;
            bombPooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

    public void SpawnOre(Vector2 pos)
    {
        GameObject obj = null;
        if ((obj = orePooler.SpawnObject()) == null)
        {
            obj = Instantiate(oreTile, pos, Quaternion.identity) as GameObject;
            orePooler.Pool(obj);
        }
        else
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }

    }

   

   

}
