﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechTileSpawner3 : MonoBehaviour {
	public static MechTileSpawner3 ins;
	// Use this for initialization
	float x = -2.196f;
	float y = 1.8f; //starting Y position
	float xSpace = 1.462f;
	[SerializeField]
	GameObject mechTile;
	[SerializeField]
	ObjectPooler tilePooler;
	
	[SerializeField]
	List<Vector2> lstPosition;

	void Awake()
	{
		ins = this;		
	}

	void Start () {
		InitTilePositions ();
		//lstPosition = MonoExtension<Vector2>.ShuffleList (lstPosition);
		lstPosition.ShuffleList();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void InitTilePositions()
	{
		int cntr = 0;
		float tempX = x;
		for(int i=0; i<16; i++)
		{
			//SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(), new Vector2 (x, y));
			lstPosition.Add (new Vector2(x,y));
			x += xSpace;
			cntr++;
			if(cntr >=4)
			{
					cntr = 0;
					x = tempX;
					y -= xSpace;
			}

		}		
	}
	
	
	public void SpawnMechPuzzle(EnumMech mech)
	{
			switch (mech) {
			case EnumMech.MECH_0:
					RedMech ();
					break;
			case EnumMech.MECH_1:
					BlueMech ();
					break;
			case EnumMech.MECH_2:
					GreenMech ();
					break;
			}
	}

	
	void RedMech()
	{
			//MechTapTileManager.ins.SetNumOfMechParts (EnumMechType.MECH_1);
			for(int i=0; i<lstPosition.Count; i++)
			{
					if (i == 0)
							SpawnTile (EnumMech.MECH_0, EnumPartType.PIECE_0, lstPosition [i], MechTapTileManager.ins.GetMech_0_Sprite (0));
					else if (i == 1)
							SpawnTile (EnumMech.MECH_0, EnumPartType.PIECE_1, lstPosition [i], MechTapTileManager.ins.GetMech_0_Sprite (1));
					else if (i == 2)
							SpawnTile (EnumMech.MECH_0, EnumPartType.PIECE_2, lstPosition [i], MechTapTileManager.ins.GetMech_0_Sprite (2));
					else if (i == 3)
							SpawnTile (EnumMech.MECH_0, EnumPartType.PIECE_3, lstPosition [i], MechTapTileManager.ins.GetMech_0_Sprite (3));
					else
							SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(),lstPosition[i]);
			}
	}
	
	void BlueMech()
	{
			//MechTapTileManager.ins.SetNumOfMechParts (EnumMechType.MECH_2);
			for(int i=0; i<lstPosition.Count; i++)
			{
					if (i == 0)
							SpawnTile (EnumMech.MECH_1, EnumPartType.PIECE_0, lstPosition [i], MechTapTileManager.ins.GetMech_1_Sprite (0));
					else if (i == 1)
							SpawnTile (EnumMech.MECH_1, EnumPartType.PIECE_1, lstPosition [i], MechTapTileManager.ins.GetMech_1_Sprite (1));
					else if (i == 2)
							SpawnTile (EnumMech.MECH_1, EnumPartType.PIECE_2, lstPosition [i], MechTapTileManager.ins.GetMech_1_Sprite (2));
					else if (i == 3)
							SpawnTile (EnumMech.MECH_1, EnumPartType.PIECE_3, lstPosition [i], MechTapTileManager.ins.GetMech_1_Sprite (3));
					else if (i == 4)
							SpawnTile (EnumMech.MECH_1, EnumPartType.PIECE_4, lstPosition [i], MechTapTileManager.ins.GetMech_1_Sprite (4));
					else
							SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(),lstPosition[i]);
			}
	}

	void GreenMech()
	{
			//MechTapTileManager.ins.SetNumOfMechParts (EnumMechType.MECH_3);
			for(int i=0; i<lstPosition.Count; i++)
			{
					if (i == 0)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_0, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(0));
					else if (i == 1)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_1, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(1));
					else if (i == 2)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_2, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(2));
					else if (i == 3)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_3, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(3));
					else if (i == 4)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_4, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(4));
					else if (i == 5)
							SpawnTile (EnumMech.MECH_2, EnumPartType.PIECE_5, lstPosition [i], MechTapTileManager.ins.GetMech_2_Sprite(5));
					else
							SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(),lstPosition[i]);
			}
	}
	
	
	
	public void DisableAllTiles()
	{
		for(int i=0; i<tilePooler.objLst.Count; i++)
		{
				tilePooler.objLst [i].SetActive (false);
		}		
	}
	#region spawners

	void SpawnTile(EnumMech mechType, EnumPartType part, Vector2 pos, Sprite sprt)
	{
			GameObject obj = null;
			if ((obj = tilePooler.SpawnObject ()) == null) {
					obj = Instantiate (mechTile, pos, Quaternion.identity) as GameObject;
					tilePooler.Pool (obj);
			} else {
					obj.SetActive (true);
					obj.transform.position = pos;
			}
			obj.GetComponent<PieceTile>().Set (mechType, part, sprt);

	}
	
	void SpawnTorso(EnumMech mechType, Vector2 pos)
	{
			switch (mechType) 
			{
			case EnumMech.MECH_0:
					SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_0_Sprite(0));
					break;
			case EnumMech.MECH_1:
					SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_1_Sprite(0));
					break;
			case EnumMech.MECH_2:
					SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_2_Sprite(0));
					break;
			}
	}

	void SpawnLegRight(EnumMech mechType, Vector2 pos)
	{
			switch (mechType) 
			{
			case EnumMech.MECH_0:
					SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_0_Sprite(1));
					break;
			case EnumMech.MECH_1:
					SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_1_Sprite(1));
					break;
			case EnumMech.MECH_2:
					SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_2_Sprite(1));
					break;
			}
	}

	void SpawnLegLeft(EnumMech mechType, Vector2 pos)
	{
			switch (mechType) 
			{
			case EnumMech.MECH_0:
					SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_0_Sprite(2));
					break;
			case EnumMech.MECH_1:
					SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_1_Sprite(2));
					break;
			case EnumMech.MECH_2:
					SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_2_Sprite(2));
					break;
			}
	}

	void SpawnArmRight(EnumMech mechType, Vector2 pos)
	{
			switch (mechType) 
			{
			case EnumMech.MECH_0:
					SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_0_Sprite(3));
					break;
			case EnumMech.MECH_1:
					SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_1_Sprite(3));
					break;
			case EnumMech.MECH_2:
					SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_2_Sprite(3));
					break;
			}
	}

	void SpawnArmLeft(EnumMech mechType, Vector2 pos)
	{
			switch (mechType) 
			{
			case EnumMech.MECH_0:
					SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_0_Sprite(4));
					break;
			case EnumMech.MECH_1:
					SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_1_Sprite(4));
					break;
			case EnumMech.MECH_2:
					SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_2_Sprite(4));
					break;
			}
	}

		void SpawnHead(EnumMech mechType, Vector2 pos)
		{
				switch (mechType) 
				{
				case EnumMech.MECH_0:
						SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_0_Sprite(5));
						break;
				case EnumMech.MECH_1:
						SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_1_Sprite(5));
						break;
				case EnumMech.MECH_2:
						SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_2_Sprite(5));
						break;
				}
		}
	
	void SpawnRandomPart(EnumMech mechType, Vector2 pos)
	{
			int _rnd = 0; // Random.Range (0, MechTapTileManager.ins.numOfMechParts);
			_rnd = Random.Range (0, MechTapTileManager.ins.GetMechSpriteList(mechType).Count);

			switch (_rnd) 
			{
			case 0:
					SpawnTorso(mechType, pos);
					break;
			case 1:
					SpawnLegRight(mechType, pos);
					break;
			case 2:
					SpawnLegLeft(mechType, pos);
					break;
			case 3:
					SpawnArmRight(mechType, pos);
					break;
			case 4:
					SpawnArmLeft (mechType, pos);
					break;
			case 5:
					SpawnHead (mechType, pos);
					break;
			default:
					break;
			}
	}
	
	#endregion spawners
}
