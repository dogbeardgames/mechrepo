﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MechUI : MonoBehaviour {

    public List<Image> lstParts;
    public List<Image> lstPartsGlow;

    

    [SerializeField]
    int n = 0;
  
    void Start()
    {

        //for (int i=0; i<lstParts.Count; i++)
        //{
        //    lstParts[i].material = MechUIManager.ins.GetGrayscale();

        //}
        for (int i = 0; i < lstParts.Count; i++)
        {
            //lstParts[i].material = MechUIManager.ins.GetGrayscale();
            lstParts[i].color = Color.white;
        }

    }
    public void EnablePartN()
    {

        MechUIManager.ins.StopAll();
        lstParts[n].material = MechUIManager.ins.GetDefault();
        lstParts[n].color = Color.white;
        if (lstPartsGlow.Count > 0)
        {
            // enable glow
            lstPartsGlow[n].enabled = false;
        }            

        n++;
        try {
            lstParts[n].material = MechUIManager.ins.GetDefault();

            MechUIManager.ins.Glow(lstParts[n]);
        }
        catch (Exception ex)
        {
        }
       
        // check if last index
        if (n < lstPartsGlow.Count && lstPartsGlow.Count > 0)
        {
            lstPartsGlow[n].enabled = true;
        }
    }        

    void OnEnable()
    {
        n = 0;
        for (int i = 0; i < lstParts.Count; i++)
        {
            lstParts[i].material = MechUIManager.ins.GetGrayscale();
            lstParts[i].color = Color.white;
        }

        lstParts[n].material = MechUIManager.ins.GetDefault();
        MechUIManager.ins.StopAll();
        MechUIManager.ins.Glow(lstParts[n]);

       
        // set first part to glow
        if (lstPartsGlow.Count > 0)
        {
            lstPartsGlow[0].enabled = true;
        }
    }

    

}
