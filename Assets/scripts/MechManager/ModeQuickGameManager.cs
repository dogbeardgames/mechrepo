﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public sealed class ModeQuickGameManager : ModeUI {

    public static ModeQuickGameManager instance;
	public double timeCntr;

    // temp
    [SerializeField]
    GameObject Panel_OPT;

	void Awake()
	{
		ins = this;
        instance = this;
	}

    // temo
    void Start()
    {
        if(!PlayerPrefs.HasKey("LanguageSelect") || TutorialManager.isReplay)
        {
            // temp
            Debug.Log("Start Quick Mode");
            Panel_OPT.SetActive(false);
//            StartQuickGameMode();
            //
        }
    }

	public void StartQuickGameMode()
	{
        SoundManager.Instance.PlayBGM("bgmQuick", true);
        GameManager.SetInitialGameModeSpeed(100);
        PlayerGameModeData.ResetGameModeData();
        timeCntr = GameManager.ins.GetQuickModeTimeLimit();
        base.SetGoldText(0.ToString("000000"));
        MechTapTileManager.ins.StartSpawning();
        startMode = true;
        SetComboText("0 build combo");
        //StartCoroutine (IEStartTimer());

        Tile.move = true;
    }

	public override void SetTimerText (string val)
	{
		base.SetTimerText (val);
	}

    void FixedUpdate()
    {
        if (!GameManager.paused && !GameManager.gameOver && startMode)
        {

            timeCntr -= 0.02;
            SetTimerText(timeCntr.ToString("#.##"));
            if (timeCntr <= 0)
            {
                startMode = false;
                GameManager.gameOver = true;
                
                ShowResults(PlayerGameModeData.gold.ToString(), PlayerGameModeData.numOfMaxComboMade.ToString(), PlayerGameModeData.numOfMechCompleted.ToString(), "60 s");

                // Set data
                // gold earned
                if(DataController.ins.PlayerData.playerData.quickHighestGoldEarned < PlayerGameModeData.gold)
                {
                    DataController.ins.PlayerData.playerData.quickHighestGoldEarned = PlayerGameModeData.gold;   
                }
                //Total mech assembled
                DataController.ins.PlayerData.playerData.quickMechAssembled += PlayerGameModeData.numOfMechCompleted;
                // highest number of mech assembled per round
                if(DataController.ins.PlayerData.playerData.quickNumberOfMech < PlayerGameModeData.numOfMechCompleted)
                {
                    DataController.ins.PlayerData.playerData.quickNumberOfMech = PlayerGameModeData.numOfMechCompleted;
                }
            }
        }
    }

 //   IEnumerator IEStartTimer()
	//{
	//		while(timeCntr > 0 && !GameManager.paused)
	//		{
	//				yield return new WaitForFixedUpdate ();
	//				timeCntr -= 0.02;
	//				SetTimerText (timeCntr.ToString("#.##"));
	//		}
 //       GameManager.gameOver = true;
 //       ShowResults(PlayerGameModeData.gold.ToString(), PlayerGameModeData.numOfCombo.ToString(), PlayerGameModeData.numOfMechCompleted.ToString(), timeCntr.ToString());

 //       // Set data
 //       // gold earned
 //       if(DataController.ins.PlayerData.playerData.quickHighestGoldEarned < PlayerGameModeData.gold)
 //       {
 //           DataController.ins.PlayerData.playerData.quickHighestGoldEarned = PlayerGameModeData.gold;   
 //       }
 //       //Total mech assembled
 //       DataController.ins.PlayerData.playerData.quickMechAssembled += PlayerGameModeData.numOfMechCompleted;
 //       // highest number of mech assembled per round
 //       if(DataController.ins.PlayerData.playerData.quickNumberOfMech < PlayerGameModeData.numOfMechCompleted)
 //       {
 //           DataController.ins.PlayerData.playerData.quickNumberOfMech = PlayerGameModeData.numOfMechCompleted;
 //       }
	//}
	
}
