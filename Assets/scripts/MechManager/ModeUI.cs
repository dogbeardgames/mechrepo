﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public abstract class ModeUI : MonoBehaviour {
	public static ModeUI ins;

	[SerializeField]
	TextMeshProUGUI txtGold, txtCombo, txtTimer, txtMech, txtMechToAssemble;
    [SerializeField]
    GameObject gameOver;
    GameObject tempGameOver;

    [SerializeField]
    GameObject fx_ok, fx_fail;
    [SerializeField]
    GameObject fx_combo;

    public bool startMode;
    public List<GameObject> mechUI;

    public void BuildMech(int mechIndex)
    {
        mechUI[mechIndex].GetComponent<MechUI>().EnablePartN();
    }

    public void BuildComplete()
    {
        fx_fail.SetActive(false);
        fx_ok.SetActive(true);
    }

    public void BuildDestroy()
    {
        fx_ok.SetActive(false);
        fx_fail.SetActive(true);
        
    }

    public void Combo()
    {
        fx_combo.SetActive(true);
    }

    public void SetMech(int mechIndex)
    {
        for (int i = 0; i < mechUI.Count; i++)
        {
            mechUI[i].SetActive(false);          
        }
        
        mechUI[mechIndex].SetActive(true);
        switch (mechIndex)
        {
            case 0:
                SetMechText("Vestar");
                break;
            case 1:
                SetMechText("Iris");
                break;
            case 2:
                SetMechText("Gareth");
                break;
            case 3:
                SetMechText("RTA24");
                break;
            case 4:
                SetMechText("Unishot");
                break;
            case 5:
                SetMechText("Onyx Jaeger");
                break;
            case 6:
                SetMechText("Warhavoc");
                break;
            case 7:
                SetMechText("Quadro-M");
                break;
            case 8:
                SetMechText("Virhea");
                break;
            case 9:
                SetMechText("KBL-21");
                break;
            default:
                break;
        }
    }

    public void ShowResults(string gold, string combo, string assembled, string time)
    {
        startMode = false;
        if (tempGameOver == null)
        {
            tempGameOver = (GameObject)Instantiate(gameOver, GameObject.Find("Canvas").transform);
        }
        else
        {
            tempGameOver.SetActive(true);
        }

        tempGameOver.GetComponent<GameOver>().SetResults(gold, combo, assembled, time);
    }

	public void SetGoldText(string val)
	{
			txtGold.text = val;
	}

	public void SetComboText(string val)
	{
		txtCombo.text = val;
      
	}

	public void SetTimeText(string val)
	{
			txtTimer.text = val;
	}

	public void SetMechText(string val)
	{
			txtMech.text = val;
	}

	public virtual void SetTimerText(string val)
	{
			txtTimer.text = val;
	}

	public virtual void SetLifeTextIncreased(sbyte val)
	{
			//PlayerGameModeData.endlessLife += val;
			//txtLife.text = "Life " + val;
	}

	public virtual void SetLifeTextDecreased(sbyte val)
	{
			//PlayerGameModeData.endlessLife -= val;
			//txtLife.text = "Life " + val;
	}

	public virtual void SetGoldTextIncreased(float value)
	{
        
		PlayerGameModeData.gold += value;
        PlayerGameModeData.gold = Convert.ToInt32(PlayerGameModeData.gold);
        txtGold.text = PlayerGameModeData.gold.ToString("000000");
    }        

	public void SetGoldTextDescreased(float value)
	{
      
        PlayerGameModeData.gold = PlayerGameModeData.gold - value < 0 ? 0 : PlayerGameModeData.gold - value;
        PlayerGameModeData.gold = Convert.ToInt32(PlayerGameModeData.gold);
        txtGold.text = PlayerGameModeData.gold.ToString("000000");
	}

    public void SetMechToAssembleText(string value)
    {
        txtMechToAssemble.text = value;
    }
}
