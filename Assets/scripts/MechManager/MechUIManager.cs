﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MechUIManager : MonoBehaviour {
    public static MechUIManager ins;

    [SerializeField]
    Material grayScale, spritesDefault;
    public Image image;
    public static bool playing;

    public Material GetGrayscale()
    {
        return grayScale;
    }

    public Material GetDefault()
    {
        return spritesDefault;
    }

    void Awake()
    {
        ins = this;
    }

    // Use this for initialization
    void Start ()
    {
//        ins = this;
	}

    //void FixedUpdate()
    //{
    //    while (true)
    //    {
    //        float n = 0.2f;
    //        image.color = Color.Lerp(image.color, Color.red, 0.1f);
    //        n -= 0.02f;
         
    //        if (n <= 0)
    //        {
    //            n = 0.2f;
    //            image.color = Color.white;
    //        }
    //    }
    //}

    public void StopAll()
    {
        StopAllCoroutines();
    }

    public void Glow(Image img)
    {
        StartCoroutine(IEGlow(img));
    }        

    public IEnumerator IEGlow(Image img)
    {
        float n = 0.2f;
        //playing = true;
        while (true)
        {
            try {
                img.color = Color.Lerp(img.color, Color.red, 0.1f);
               
            }
            catch (Exception ex)
            {
                StopAllCoroutines();
            }

            n -= 0.02f;
            yield return new WaitForSeconds(0.02f);
            if (n <= 0)
            {
                n = 0.2f;
                if(img != null)
                img.color = Color.white;
            }
        }               
    }
	
}
