﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ModeEndlessGameManager : ModeUI {

    public static ModeEndlessGameManager instance;
    static sbyte life = 10;
    static float timeCntr = 0;

    static float speedLvlMultplier = 0.05f;
    public static float initialGameSpeed = 120f;
    static byte speedUpCntr = 0;
    static byte speedUpAtCount = 5;
    // total time in game
    static float startTime, endTime;
    [SerializeField]
    GameObject[] healthBar;
    [SerializeField]
    GameObject fx_reject;
    string time;

    void Awake()
	{
        instance = this;
		ins = this;
	}

    public static float GetTime()
    {
        return timeCntr;
    }
	
	public void StartEndlessGameMode()
	{
        SoundManager.Instance.PlayBGM("bgmEndless", true);
        GameManager.SetInitialGameModeSpeed(120);
        PlayerGameModeData.ResetGameModeData();
        life = 10;
        base.SetLifeTextIncreased(GameManager.ins.GetEndlessModeMaxLife());
        base.SetGoldText(0.ToString("000000"));
        SetComboText("0 combo build");
        MechTapTileManager.ins.StartSpawning();
        startMode = true;
        for(int i=0; i<healthBar.Length; i++)
        {
            healthBar[i].SetActive(true);
        }
        timeCntr = 0;
        speedUpCntr = 0;
        // get start time
        startTime = Time.time;
        Tile.move = true;
    }

    public void ShowRejectAnimation()
    {
        fx_reject.SetActive(true);
    }

	public override void SetLifeTextDecreased (sbyte val)
	{
		life -= val;
		base.SetLifeTextDecreased (life);
        healthBar[life].SetActive(false);
        fx_reject.SetActive(true);

        if (life <= 0)
        {
            // get end time
            endTime = Time.time;

            GameManager.gameOver = true;
            ShowResults(PlayerGameModeData.gold.ToString(), PlayerGameModeData.numOfMaxComboMade.ToString(), PlayerGameModeData.numOfMechCompleted.ToString(), time);
            // Set data
            // gold earned
            if(DataController.ins.PlayerData.playerData.endlessHighestGoldEarned < PlayerGameModeData.gold)
            {
                DataController.ins.PlayerData.playerData.endlessHighestGoldEarned = PlayerGameModeData.gold;   
            }
            //Total mech assembled
            DataController.ins.PlayerData.playerData.endlessMechAssembled += PlayerGameModeData.numOfMechCompleted;
            // highest number of mech assembled per round
            if(DataController.ins.PlayerData.playerData.endlessNumberOfMech < PlayerGameModeData.numOfMechCompleted)
            {
                DataController.ins.PlayerData.playerData.endlessNumberOfMech = PlayerGameModeData.numOfMechCompleted;
            }
            // total time
            if(DataController.ins.PlayerData.playerData.longestTime < (uint)(endTime - startTime))
            {
                DataController.ins.PlayerData.playerData.longestTime = (uint)(endTime - startTime);
            }
        }
	}

	public override void SetLifeTextIncreased (sbyte val)
	{
		life += val;
		base.SetLifeTextIncreased (life);
	}

    public override void SetGoldTextIncreased(float value)
    {
        base.SetGoldTextIncreased(value);

        if(++speedUpCntr >= speedUpAtCount)
        {
            MechTapTileManager.originalSpeed = MechTapTileManager.originalSpeed - (initialGameSpeed * speedLvlMultplier) <= 30f ? 30f : MechTapTileManager.originalSpeed - (initialGameSpeed * speedLvlMultplier);

            SupplyBoxManager.tempGameSpeed = MechTapTileManager.originalSpeed;

            //NOTE: wait for supply speed to end if active
            StartCoroutine(IESpeedUp());
            speedUpCntr = 0;
        }


    }

    IEnumerator IESpeedUp()
    {
        while (SupplyBoxManager.GetSpeedUpTimer() != 0)
        {
            yield return new WaitForSeconds(0.0000001f);
        }

        while (SupplyBoxManager.GetSlowDownTimer() != 0)
        {
            yield return new WaitForSeconds(0.0000001f);
        }

        while (MechTapTileManager.ins.GetSpawner().GetTimerValue() > 0)
        {
            yield return new WaitForSeconds(0.0000001f);
        }
        
        MechTapTileManager.Speed = MechTapTileManager.originalSpeed;
    }

    void FixedUpdate()
    {
        if (!GameManager.paused && !GameManager.gameOver && startMode)
        {
            timeCntr += 0.02F;
            

            if(timeCntr >= 3600)
            {
                time = string.Format("{0:##}:{1:0#}:{2:0#}.{3:0#}", Mathf.FloorToInt(timeCntr / 3600), Mathf.FloorToInt(timeCntr / 60), Mathf.FloorToInt(timeCntr % 60));
            }
            else
            {
                time = string.Format("{0:0#}:{1:0#}.{2:0#}", Mathf.FloorToInt(timeCntr / 60), Mathf.FloorToInt(timeCntr % 60), Mathf.FloorToInt(((timeCntr % 60) % 1) * 100));
              //  Debug.Log("minutes " + Mathf.FloorToInt(timeCntr / 60) + ", seconds " + (timeCntr % 60) + ", milli " + ((timeCntr % 60) % 1) * 100);
               // Debug.Log("Less than an hour");
            }
//            SetTimerText(timeCntr.ToString("#.##"));
            SetTimerText(time);

            // achievements
            if(timeCntr % 60 == 0)
            {
                DataController.ins.AchievementsDataManager.TirelessChecker((int)timeCntr / 60);
            }
        }
    }
    
}
