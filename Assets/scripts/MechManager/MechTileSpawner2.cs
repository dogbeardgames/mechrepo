﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MechTileSpawner2 : MonoBehaviour {
	//Dont create an instance of this... this must be only controlled in MechTapTileManager as possible
	static bool spawning = false;
	static float timer=0;
  
	static int xPosIndexCnt = -1;
   
	//[SerializeField]
	static List<float> lstXPos = new List<float>(){ -2.196f, -0.736f, 0.736f, 2.196f};
	
	[SerializeField]
	GameObject mechTile;

	[SerializeField]
	ObjectPooler tilePooler;
    //[SerializeField]
    static int mechPartsSequenceCntr = 0;
    //[SerializeField]
    static int arrMechIndex = 0, mechPartIndex_algo3 = 0;
	[SerializeField]
	EnumMech mechToAssemble; //mech to spawn, this is sequence based

    [SerializeField]
    SpecialTilesManager specialTilesMan;
    [SerializeField]
    SupplyBoxManager supplyBoxMan;

    System.Action FixedUpdateAction;

    public SpecialTilesManager SpecialTilesManager()
    {
        return specialTilesMan;
    }

    public SupplyBoxManager SupplyBoxManager()
    {
        return supplyBoxMan;
    }

    public void SetTimerValue(float time)
    {
        timer = time;
    }

    public float GetTimerValue()
    {
        return timer;
    }

    public void Bomb()
    {
        //on bomb explode restart game spawning
        xPosIndexCnt = -1;
        mechPartsSequenceCntr = 0;
        mechToAssemble = MechTapTileManager.ins.mechToAssemble;
        arrMechIndex = 0;
    }

    public void SetMechToAssemble()
    {
        mechToAssemble = MechTapTileManager.ins.mechToAssemble;
    }

    public EnumMech GetMechToAssemble()
    {
        return mechToAssemble;
    }
   

    public void Reset()
    {
        spawning = false;
        timer = 0;
        xPosIndexCnt = -1;
        mechPartsSequenceCntr = 0;
        mechToAssemble = EnumMech.MECH_0;
        arrMechIndex = 0;
        mechPartIndex_algo3 = 0;

        foreach (GameObject item in tilePooler.objLst)
        {
            item.SetActive(false);
            item.transform.position = new Vector2(0, GameManager.TopScreen());
        }
        foreach (GameObject tile in specialTilesMan.GetBombPooler().objLst)
        {
            tile.SetActive(false);
            tile.transform.position = new Vector2(0, GameManager.TopScreen());
        }
        foreach (GameObject tile in specialTilesMan.GetOrePooler().objLst)
        {
            tile.SetActive(false);
            tile.transform.position = new Vector2(0, GameManager.TopScreen());
        }
        foreach (GameObject tile in supplyBoxMan.GetSupplyPooler().objLst)
        {
            tile.SetActive(false);
            tile.transform.position = new Vector2(0, GameManager.TopScreen());
        }

        supplyBoxMan.Reset();

    }

	public EnumMech GetNextMech()
	{
			return mechToAssemble;
	}

	public void ZeroPosIndexCntr()
	{
			xPosIndexCnt = 0;
	}

	public ObjectPooler GetTilePooler(){
        return tilePooler;
	}

    public void PreLoad(int n)
    {
        tilePooler.PreLoad(n, mechTile, new Vector3(0, GameManager.TopScreen(), 0));
    }

	void FixedUpdate()
	{
        if (FixedUpdateAction != null)
            FixedUpdateAction();
	}
	
    void OnEnable()
    {
//        SceneManager.sceneLoaded += OnSceneLoaded;
        DetermineFixedUpdate();
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    #region Action

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {            
        Debug.Log("Scene Loaded!");
        DetermineFixedUpdate();
    }

    void GameFixedUpdate()
    {
        //if (ScoreBoardManager.startGame) {
        if (!GameManager.paused)
        {
            timer += Time.fixedDeltaTime;
        }

        //spawnTime = manager.ins.getSpawnSpd();
        if(timer > MechTapTileManager.GetSpawnTime() && spawning && !GameManager.paused && !GameManager.gameOver)
        {
           
            SpawnTiles ();
           
            //SpawnTutorialTiles();
            timer = 0;
            mechPartsSequenceCntr++;
            if(mechPartsSequenceCntr >= MechTapTileManager.ins.GetMechSpriteList(mechToAssemble).Count)
            {
                mechPartsSequenceCntr = 0;

                arrMechIndex++;
                if (arrMechIndex >= MechTapTileManager.ins.arrMechToAssemble.Length) {
                    arrMechIndex = 0;
                    MechTapTileManager.ins.ShuffleMechCollection();
                    print("SHUFFLE");
                }
                mechToAssemble = MechTapTileManager.ins.arrMechToAssemble[arrMechIndex];
            }
           
        }
        //}
    }

    void TutorialFixedUpdate()
    {
        //if (ScoreBoardManager.startGame) {
        if (!GameManager.paused)
        {
            timer += Time.fixedDeltaTime;
        }

        //spawnTime = manager.ins.getSpawnSpd();
        if(timer > MechTapTileManager.GetSpawnTime() && spawning && !GameManager.paused && !GameManager.gameOver && Tile.move)
        {

          
            SpawnTutorialTiles();
            timer = 0;
            mechPartsSequenceCntr++;
            if(mechPartsSequenceCntr >= MechTapTileManager.ins.GetMechSpriteList(mechToAssemble).Count)
            {
                mechPartsSequenceCntr = 0;

                arrMechIndex++;
                if (arrMechIndex >= MechTapTileManager.ins.arrMechToAssemble.Length) {
                    arrMechIndex = 0;
                }
                mechToAssemble = MechTapTileManager.ins.arrMechToAssemble[arrMechIndex];
            }
        }
        //}
    }

    #endregion


    // Determine if update if for tutorial or not
    public void DetermineFixedUpdate()
    {
        FixedUpdateAction = GameFixedUpdate;
        Debug.Log("Has key? " + PlayerPrefs.HasKey("LanguageSelect") + ", is relplay? " + TutorialManager.isReplay);
        if (PlayerPrefs.HasKey("LanguageSelect") && !TutorialManager.isReplay)
        {
//            FixedUpdateAction = GameFixedUpdate;
            FixedUpdateAction = null;
            Debug.Log("<color=red>GameFixedUpdate</color>");
        }
        else
        {
            FixedUpdateAction = TutorialFixedUpdate;
            Debug.Log("<color=red>TutorialFixedUpdate</color>");
        }            
    }

	public void StartSpawning(bool b)
	{
		spawning = b;
        //mechToAssemble = MechTapTileManager.ins.mechToAssemble;
        mechToAssemble = EnumMech.MECH_0;

        // temp
        if(PlayerPrefs.HasKey("LanguageSelect") && !TutorialManager.isReplay)
            FixedUpdateAction = GameFixedUpdate;
    }

	void SpawnTile(EnumMech mechType, EnumPartType part, Vector2 pos, Sprite sprt)
	{
		GameObject obj = null;
		if ((obj = tilePooler.SpawnObject ()) == null) {
			obj = Instantiate (mechTile, pos, Quaternion.identity) as GameObject;
			tilePooler.Pool (obj);
		} else {
				obj.SetActive (true);
				obj.transform.position = pos;
		}
			
		obj.GetComponent<PieceTile>().Set (mechType, part, sprt);
					
	}

    void SpawnTutorialTile(EnumMech mechType, EnumPartType part, Vector2 pos, Sprite sprt, int pieceOrder)
    {
        GameObject obj = null;
        if ((obj = tilePooler.SpawnObject ()) == null) {
            obj = Instantiate (mechTile, pos, Quaternion.identity) as GameObject;
            tilePooler.Pool (obj);
        } else {
            obj.SetActive (true);
            obj.transform.position = pos;
        }                  

        Destroy(obj.GetComponent<PieceTile>());

        TutorialTile tutTile = obj.AddComponent<TutorialTile>();

        tutTile.pieceOrder = pieceOrder;
        tutTile.Set(mechType, part, sprt);

        Debug.Log("Piece order " + pieceOrder + ", pos" + pos);
    }

	#region parts

    #region Tutorial

    void SpawnPiece_1Tutorial(EnumMech mechType, Vector2 pos, int pieceOrder)
    {        
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_0_Sprite(0), pieceOrder);
                break;
            case EnumMech.MECH_1:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_1_Sprite(0), pieceOrder);
                break;
        }
    }

    void SpawnPiece_2Tutorial(EnumMech mechType, Vector2 pos, int pieceOrder)
    {
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_0_Sprite(1), pieceOrder);
                break;
            case EnumMech.MECH_1:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_1_Sprite(1), pieceOrder);
                break;
        }
    }

    void SpawnPiece_3Tutorial(EnumMech mechType, Vector2 pos, int pieceOrder)
    {
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_0_Sprite(2), pieceOrder);
                break;
            case EnumMech.MECH_1:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_1_Sprite(2), pieceOrder);
                break;
        }
    }

    void SpawnPiece_4Tutorial(EnumMech mechType, Vector2 pos, int pieceOrder)
    {
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_0_Sprite(3), pieceOrder);
                break;
            case EnumMech.MECH_1:
                SpawnTutorialTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_1_Sprite(3), pieceOrder);
                break;
        }
    }

    #endregion

	void SpawnPiece_1(EnumMech mechType, Vector2 pos)
	{
		switch (mechType) 
		{
			case EnumMech.MECH_0:
				SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_0_Sprite(0));
				break;
			case EnumMech.MECH_1:
				SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_1_Sprite(0));
				break;
			case EnumMech.MECH_2:
				SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_2_Sprite(0));
				break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_3_Sprite(0));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_4_Sprite(0));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_5_Sprite(0));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_6_Sprite(0));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_7_Sprite(0));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_8_Sprite(0));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_0, pos, MechTapTileManager.ins.GetMech_9_Sprite(0));
                break;
        }
	}

	void SpawnPiece_2(EnumMech mechType, Vector2 pos)
	{
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_0_Sprite(1));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_1_Sprite(1));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_2_Sprite(1));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_3_Sprite(1));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_4_Sprite(1));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_5_Sprite(1));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_6_Sprite(1));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_7_Sprite(1));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_8_Sprite(1));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_1, pos, MechTapTileManager.ins.GetMech_9_Sprite(1));
                break;
        }
    }
	
	void SpawnPiece_3(EnumMech mechType, Vector2 pos)
	{
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_0_Sprite(2));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_1_Sprite(2));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_2_Sprite(2));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_3_Sprite(2));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_4_Sprite(2));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_5_Sprite(2));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_6_Sprite(2));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_7_Sprite(2));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_8_Sprite(2));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_2, pos, MechTapTileManager.ins.GetMech_9_Sprite(2));
                break;
        }
    }

	void SpawnPiece_4(EnumMech mechType, Vector2 pos)
	{
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_0_Sprite(3));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_1_Sprite(3));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_2_Sprite(3));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_3_Sprite(3));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_4_Sprite(3));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_5_Sprite(3));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_6_Sprite(3));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_7_Sprite(3));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_8_Sprite(3));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_3, pos, MechTapTileManager.ins.GetMech_9_Sprite(3));
                break;
        }
    }
	
	void SpawnPiece_5(EnumMech mechType, Vector2 pos)
	{
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_0_Sprite(4));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_1_Sprite(4));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_2_Sprite(4));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_3_Sprite(4));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_4_Sprite(4));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_5_Sprite(4));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_6_Sprite(4));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_7_Sprite(4));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_8_Sprite(4));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_4, pos, MechTapTileManager.ins.GetMech_9_Sprite(4));
                break;
        }
    }

	void SpawnPiece_6(EnumMech mechType, Vector2 pos)
	{
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_0_Sprite(5));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_1_Sprite(5));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_2_Sprite(5));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_3_Sprite(5));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_4_Sprite(5));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_5_Sprite(5));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_6_Sprite(5));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_7_Sprite(5));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_8_Sprite(5));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_5, pos, MechTapTileManager.ins.GetMech_9_Sprite(5));
                break;
        }
    }

    void SpawnPiece_7(EnumMech mechType, Vector2 pos)
    {
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_0_Sprite(6));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_1_Sprite(6));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_2_Sprite(6));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_3_Sprite(6));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_4_Sprite(6));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_5_Sprite(6));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_6_Sprite(6));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_7_Sprite(6));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_8_Sprite(6));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_6, pos, MechTapTileManager.ins.GetMech_9_Sprite(6));
                break;
        }
    }

    void SpawnPiece_8(EnumMech mechType, Vector2 pos)
    {
        switch (mechType)
        {
            case EnumMech.MECH_0:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_0_Sprite(7));
                break;
            case EnumMech.MECH_1:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_1_Sprite(7));
                break;
            case EnumMech.MECH_2:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_2_Sprite(7));
                break;
            case EnumMech.MECH_3:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_3_Sprite(7));
                break;
            case EnumMech.MECH_4:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_4_Sprite(7));
                break;
            case EnumMech.MECH_5:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_5_Sprite(7));
                break;
            case EnumMech.MECH_6:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_6_Sprite(7));
                break;
            case EnumMech.MECH_7:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_7_Sprite(7));
                break;
            case EnumMech.MECH_8:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_8_Sprite(7));
                break;
            case EnumMech.MECH_9:
                SpawnTile(mechType, EnumPartType.PIECE_7, pos, MechTapTileManager.ins.GetMech_9_Sprite(7));
                break;
        }
    }



    #endregion parts

    void SpawnPart(EnumMech mechType, EnumPartType part, Vector2 pos)
    {
        switch (part)
        {
            case EnumPartType.PIECE_0:
                SpawnPiece_1(mechType, pos);
                break;
            case EnumPartType.PIECE_1:
                SpawnPiece_2(mechType, pos);
                break;
            case EnumPartType.PIECE_2:
                SpawnPiece_3(mechType, pos);
                break;
            case EnumPartType.PIECE_3:
                SpawnPiece_4(mechType, pos);
                break;
            case EnumPartType.PIECE_4:
                SpawnPiece_5(mechType, pos);
                break;
            case EnumPartType.PIECE_5:
                SpawnPiece_6(mechType, pos);
                break;
            case EnumPartType.PIECE_6:
                SpawnPiece_7(mechType, pos);
                break;
            case EnumPartType.PIECE_7:
                SpawnPiece_8(mechType, pos);
                break;              
            default:
                SpawnPiece_1(mechType, pos);
                break;
        }

       
    }

    void SpawnMissedPart(EnumMech mechType, byte missedPartIndex, Vector2 pos)
    {

        switch (missedPartIndex)
        {
            case 0:
                SpawnPiece_1(mechType, pos);
                break;
            case 1:
                SpawnPiece_2(mechType, pos);
                break;
            case 2:
                SpawnPiece_3(mechType, pos);
                break;
            case 3:
                SpawnPiece_4(mechType, pos);
                break;
            case 4:
                SpawnPiece_5(mechType, pos);
                break;
            case 5:
                SpawnPiece_6(mechType, pos);
                break;
            case 6:
                SpawnPiece_7(mechType, pos);
                break;
            case 7:
                SpawnPiece_8(mechType, pos);
                break;
            default:
                SpawnRandomPart(mechType, pos);
                break;
        }
    }

    void SpawnRandomTile(Vector2 pos)
    {
        //Random with special

        int rnd = Random.Range(0, 100);
        if (rnd >= 0 && rnd < 75)
        {
            SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), pos);
        }
        else
        {
            if (TutorialManager.Instance == null)
            {
                //-- orginal part
                rnd = Random.Range(0, 100);
                if (rnd >= 0 && rnd < 40)
                {
                    specialTilesMan.SpawnBomb(pos);
                }
                else if (rnd >= 40 && rnd < 70) //TEST 80 dapat
                {
                    specialTilesMan.SpawnOre(pos);
                }
                else
                {
                    supplyBoxMan.SpawnSupply(pos);
                }
                //--
            }
            else
            {
                SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), pos);
            }
        }
        // supplyBoxMan.SpawnSupply(pos);

    }

    void SpawnRandomPart(EnumMech mechType, Vector2 pos)
    {
        int _rnd = 0; // Random.Range (0, MechTapTileManager.ins.numOfMechParts);
        _rnd = Random.Range(0, MechTapTileManager.ins.GetMechSpriteList(mechType).Count);

        switch (_rnd)
        {
            case 0:
                if (mechPartsSequenceCntr == 0)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_1(mechType, pos);
                break;
            case 1:
                if (mechPartsSequenceCntr == 1)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_2(mechType, pos);
                break;
            case 2:
                if (mechPartsSequenceCntr == 2)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_3(mechType, pos);
                break;
            case 3:
                if (mechPartsSequenceCntr == 3)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_4(mechType, pos);
                break;
            case 4:
                if (mechPartsSequenceCntr == 4)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_5(mechType, pos);
                break;
            case 5:
                if (mechPartsSequenceCntr == 5)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_6(mechType, pos);
                break;
            case 6:
                if (mechPartsSequenceCntr == 6)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_7(mechType, pos);
                break;
            case 7:
                if (mechPartsSequenceCntr == 7)
                    SpawnRandomPart(mechType, pos);
                else
                    SpawnPiece_8(mechType, pos);
                break;
            default:
                Debug.LogError("Mech Type" + "NOT FOUND");
                break;
        }

    }

    public void SpawnTutorialTiles()
    {// -2.196f, -0.736f, 0.736f, 2.196f
        if (mechPartsSequenceCntr == 0)
        {
            SpawnPiece_1Tutorial(EnumMech.MECH_0, new Vector2(2.196f, GameManager.TopScreen()), 0);
            Debug.Log("Piece 1 " + lstXPos[3]);
            SpawnPiece_2Tutorial(EnumMech.MECH_1, new Vector2(0.736f, GameManager.TopScreen()), -1);
            SpawnPiece_3Tutorial(EnumMech.MECH_0, new Vector2(-0.736f, GameManager.TopScreen()), -1);
            SpawnPiece_4Tutorial(EnumMech.MECH_1, new Vector2(-2.196f, GameManager.TopScreen()), -1);

//            SpawnPiece_1(mechToAssemble, new Vector2(lstXPos[3], GameManager.TopScreen()));
//
//            SpawnPiece_2(EnumMech.MECH_1, new Vector2(lstXPos[2], GameManager.TopScreen()));
//            SpawnPiece_3(EnumMech.MECH_0, new Vector2(lstXPos[1], GameManager.TopScreen()));
//            SpawnPiece_4(EnumMech.MECH_1, new Vector2(lstXPos[0], GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 1)
        {
            SpawnPiece_2Tutorial(EnumMech.MECH_0, new Vector2(-2.196f, GameManager.TopScreen()), 1);
            Debug.Log("Piece 2 " + lstXPos[0]);
            SpawnPiece_1Tutorial(EnumMech.MECH_1, new Vector2(0.736f, GameManager.TopScreen()), -1);
            SpawnPiece_3Tutorial(EnumMech.MECH_0, new Vector2(-0.736f, GameManager.TopScreen()), -1);
            SpawnPiece_4Tutorial(EnumMech.MECH_0, new Vector2(2.196f, GameManager.TopScreen()), -1);

//            SpawnPiece_2(mechToAssemble, new Vector2(lstXPos[0], GameManager.TopScreen()));
//
//            SpawnPiece_1(EnumMech.MECH_1, new Vector2(lstXPos[2], GameManager.TopScreen()));
//            SpawnPiece_3(EnumMech.MECH_0, new Vector2(lstXPos[1], GameManager.TopScreen()));
//            SpawnPiece_4(EnumMech.MECH_0, new Vector2(lstXPos[3], GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 2)
        {
            SpawnPiece_3Tutorial(EnumMech.MECH_0, new Vector2(-0.736f, GameManager.TopScreen()), 2);
            Debug.Log("Piece 3 " + lstXPos[1]);
            SpawnPiece_2Tutorial(EnumMech.MECH_1, new Vector2(2.196f, GameManager.TopScreen()), -1);
            SpawnPiece_4Tutorial(EnumMech.MECH_1, new Vector2(0.736f, GameManager.TopScreen()), -1);
            SpawnPiece_4Tutorial(EnumMech.MECH_1, new Vector2(-2.196f, GameManager.TopScreen()), -1);

//            SpawnPiece_3(mechToAssemble, new Vector2(lstXPos[1], GameManager.TopScreen()));
//
//            SpawnPiece_2(EnumMech.MECH_1, new Vector2(lstXPos[3], GameManager.TopScreen()));
//            SpawnPiece_4(EnumMech.MECH_1, new Vector2(lstXPos[2], GameManager.TopScreen()));
//            SpawnPiece_4(EnumMech.MECH_1, new Vector2(lstXPos[0], GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 3)
        {
            SpawnPiece_4Tutorial(EnumMech.MECH_0, new Vector2(0.736f, GameManager.TopScreen()), 3);
            Debug.Log("Piece 4 " + lstXPos[2]);
            SpawnPiece_1Tutorial(EnumMech.MECH_1, new Vector2(2.196f, GameManager.TopScreen()), -1);
            SpawnPiece_3Tutorial(EnumMech.MECH_1, new Vector2(-2.196f, GameManager.TopScreen()), -1);
            SpawnPiece_2Tutorial(EnumMech.MECH_1, new Vector2(-0.736f, GameManager.TopScreen()), -1);

//            SpawnPiece_4(mechToAssemble, new Vector2(lstXPos[2], GameManager.TopScreen()));
//
//            SpawnPiece_1(EnumMech.MECH_2, new Vector2(lstXPos[3], GameManager.TopScreen()));
//            SpawnPiece_3(EnumMech.MECH_1, new Vector2(lstXPos[0], GameManager.TopScreen()));
//            SpawnPiece_2(EnumMech.MECH_1, new Vector2(lstXPos[1], GameManager.TopScreen()));
        }
    }

    //this spawn in mech part sequence
    void SpawnPartsInSequence()
    {
        if (mechPartsSequenceCntr == 0)
        {
            SpawnPiece_1(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 1)
        {
            SpawnPiece_2(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 2)
        {
            SpawnPiece_3(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 3)
        {
            SpawnPiece_4(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 4)
        {
            SpawnPiece_5(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 5)
        {
            SpawnPiece_6(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 6)
        {
            SpawnPiece_7(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else if (mechPartsSequenceCntr == 7)
        {
            SpawnPiece_8(mechToAssemble, new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
        else
        {
            Debug.LogError("mechPartsSequenceCntr " + "NOT IN RANGE");
        }
    }

    void SpawnTiles()
	{

        SpawnPartsInSequence();
       
        Algo1();

        xPosIndexCnt = -1;
		lstXPos.ShuffleList ();
	
	}

   


    void Algo1()
	{
        //SUPER RANDOM THREEE
        if (mechToAssemble != MechTapTileManager.ins.mechToAssemble)
        {
           
            SpawnPart(MechTapTileManager.ins.mechToAssemble, MechTapTileManager.ins.GetPartBySequence(mechPartIndex_algo3), new Vector2(ShuffledXpos(), GameManager.TopScreen()));
            mechPartIndex_algo3 = mechPartIndex_algo3 < MechTapTileManager.ins.GetMechSpriteList(MechTapTileManager.ins.mechToAssemble).Count ? ++mechPartIndex_algo3 : 0;
           
        }
        else
        {
            mechPartIndex_algo3 = 0;
            SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        }
           
        
        SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(), new Vector2 (ShuffledXpos(), GameManager.TopScreen()));
        
        SpawnRandomTile(new Vector2(ShuffledXpos(), GameManager.TopScreen()));
     
	}
	
	void Algo2()
	{
				
		//Spawn 1 missed part
		SpawnMissedPart(MechTapTileManager.ins.mechToAssemble, MechTapTileManager.ins.GetMissedPartIndex() ,new Vector2 (ShuffledXpos(), GameManager.TopScreen()));

        SpawnRandomPart (MechTapTileManager.ins.GetRandomMech(), new Vector2 (ShuffledXpos(), GameManager.TopScreen()));
       
        SpawnRandomTile(new Vector2(ShuffledXpos(), GameManager.TopScreen()));

       // SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));

    }

    void Algo3()
    {
        //every 4 spawn, spawn need part

        SpawnPart(MechTapTileManager.ins.mechToAssemble, MechTapTileManager.ins.GetMechPartToTap(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));

        SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));

        SpawnRandomTile(new Vector2(ShuffledXpos(), GameManager.TopScreen()));
    }

    void Algo3_1()
    {
        //every 4 spawn, spawn need part

        //SpawnPart(MechTapTileManager.ins.mechToAssemble, MechTapTileManager.ins.GetMechPartToTap(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));
        SpawnPartsInSequence();

        SpawnRandomPart(MechTapTileManager.ins.GetRandomMech(), new Vector2(ShuffledXpos(), GameManager.TopScreen()));

        SpawnRandomTile(new Vector2(ShuffledXpos(), GameManager.TopScreen()));
    }



    float ShuffledXpos()
    {
        //xpos index cntr starts from -1

        if (xPosIndexCnt >= lstXPos.Count)
            xPosIndexCnt = -1;
        xPosIndexCnt++;
        return lstXPos[xPosIndexCnt];
    }
}
