﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GAME_MODE{ QUICK, ENDLESS}

public class GameManager : MonoBehaviour {

	public static GameManager ins;
	
	public static GAME_MODE mode;
	public static bool paused;
    public static bool gameOver;
    static float topStartPos;
    static float botEndPos;
    public const int quickComboBonus = 20;
    public const int endlessComboBonus = 10;


    //static byte damagedPartMaxDurability = 4; //constant

    [SerializeField]
	float quickModeTimeLimit = 60f;
	[SerializeField]
	sbyte endlessModeLife = 5;

    [SerializeField]
    GameObject pausePanel;
    GameObject tempPause;

    public static void SetInitialGameModeSpeed(float value)
    {
        MechTapTileManager.Speed = value;
        MechTapTileManager.originalSpeed = value;
        ModeEndlessGameManager.initialGameSpeed = value;
        SupplyBoxManager.tempGameSpeed = value;
    }

	public float GetQuickModeTimeLimit(){
			return quickModeTimeLimit;	
	}
	public sbyte GetEndlessModeMaxLife()
	{
			return endlessModeLife;
	}

    public static float TopScreen() {
        return 2.455f; // topStartPos;
    }
    public static float BotScreen()
    {
        return -4.851f; // botEndPos;
    }

    void Awake()
	{
		if (ins == null) {
				ins = this;
				DontDestroyOnLoad (gameObject);
		} else {
				Destroy (gameObject);
		}		
	}

    void Start()
    {
        /*
         *  get top edge and bottom edge Y pos
            topStartPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height,0)).y + ((spritePixelSize/2) * 0.01f);
            botEndPos = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + ((spritePixelSize / 2) * 0.01f);
         */

    }

    // Update is called once per frame
 //   void Update () {
	//	if(Input.GetKey(KeyCode.Escape) && !paused && !gameOver)
	//	{
 //           PauseGame();
	//	}
	//}

    public void PauseGame()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "Menu")
        {
            paused = true;
            ModeUI.ins.startMode = false;
            //Instantiate(pausePanel, GameObject.Find("Canvas").transform, false);
            SoundManager.Instance.BGM.Pause();
            if (tempPause == null)
            {
                tempPause = (GameObject)Instantiate(pausePanel, GameObject.Find("Canvas").transform, false);
               
               
            }
            else
            {
                tempPause.SetActive(true);
            }
        }

    }
}
