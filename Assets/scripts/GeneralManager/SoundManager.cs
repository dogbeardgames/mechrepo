﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


  
public class SoundManager : MonoBehaviour
{
	private static SoundManager s_instance = null;
	public static SoundManager Instance
	{
		get { return s_instance; }
	}            

	void Start()
	{
	    Init();
        // set audio value
        bgmSrc.volume = DataController.ins.PlayerData.playerData.isBGMOn ? 1 : 0;
        sfxSrc.volume = DataController.ins.PlayerData.playerData.isSFXON ? 1 : 0;

        //bgmSrc.mute = true;
        //sfxSrc.mute = true;
			//DontDestroyOnLoad(gameObject);
	}

	[SerializeField] AudioClip[] sfxClip, bgmClip;
	//[SerializeField] AudioSource bgmInside, bgmOutside, sfx;
	[SerializeField] AudioSource bgmSrc, sfxSrc;
	Dictionary<string, AudioClip> dictSFX = new Dictionary<string, AudioClip>();
	Dictionary<string, AudioClip> dictBGM = new Dictionary<string, AudioClip>();        	


		public AudioSource BGM{
				get{ return bgmSrc; }
		}
//		public AudioSource BGMOutside{
//			get{ return bgmOutside; }
//		}

		public AudioSource SFX
		{
			get{ return sfxSrc;}
		}
		

	// Use this for initialization
	private void Init()
	{
        //if(s_instance != null)
        //  {
        //      Destroy(gameObject);
        //  }
        //  else
        //  {
        //      s_instance = this;
        //     // DontDestroyOnLoad(gameObject);
        //  }            
        s_instance = this;
        if (sfxClip != null) 
			{
				foreach (AudioClip sfx in sfxClip)// sfx dictionary
				{
					dictSFX.Add(sfx.name.ToLower(), sfx);
				}
			}

			if (bgmClip != null) 
			{
				foreach (AudioClip bgm in bgmClip)// bgm dictionary
				{
					dictBGM.Add(bgm.name.ToLower(), bgm);
				}
			}
	}

	#region METHODS
	public void MuteInsideBGM()
	{
	    bgmSrc.mute = true;
	}

//	public void MuteOutsideBGM()
//	{            
//	    bgmOutside.mute = true;
//	}

//	public void FadeOutOutside()
//	{
//	    bgmOutside.DOFade(0, 2);
//	}      

	public void FadeOutInside()
	{
		bgmSrc.DOFade (0, 2);
	}

//	public void FadeInOutside()
//	{
//			//bgmOutside.DOFade(DataController.Instance.settings.volume, 2);
//	}
//
//	public void FadeInInside()
//	{
//		//bgmInside.DOFade(DataController.Instance.settings.volume, 2);
//	}

	public void UnmuteInsideBGM()
	{
	    bgmSrc.mute = false;
	}

//	public void UnmuteOutsideBGM()
//	{            
//	    bgmOutside.mute = false;
//	}

	public void MuteSFX()
	{
	    sfxSrc.mute = true;
	}

	public void UnmuteSFX()
	{
	    sfxSrc.mute = false;
	}

	/// <summary>
	/// set bgm volume, min = 0, max = 1
	/// </summary>
	/// <param name="volume"></param>
	public void BGMVolume(float volume)
	{
	    bgmSrc.volume = volume;
	    //bgmOutside.volume = volume;
	}

	/// <summary>
	/// set sfx volume, min = 0, max = 1
	/// </summary>
	/// <param name="volume"></param>
	public void SFXVolume(float volume)
	{
	    sfxSrc.volume = volume;
	}

	public void PlaySFX(string name)
	{
	    AudioClip clip;
	    sfxSrc.PlayOneShot(dictSFX[name.ToLower()]);
	    dictSFX.TryGetValue(name.ToLower(), out clip);
	    sfxSrc.PlayOneShot(clip);            
	}

		public void PlaySFX(string name, bool loop)
		{
			AudioClip clip;			
			dictSFX.TryGetValue(name.ToLower(), out clip);
		    sfxSrc.clip = clip;
		    sfxSrc.Play();
			sfxSrc.loop = loop;
		}

		public void SetSFXAndPlay(string name, bool loop)
		{
			AudioClip clip;			
			dictSFX.TryGetValue(name.ToLower(), out clip);
	    	sfxSrc.clip = clip;
			//sfx.PlayOneShot(clip);          
			sfxSrc.Play();
			sfxSrc.loop = loop;
		}

	public void StopSFX()
	{         
	    sfxSrc.DOFade(0, 0.5f).OnComplete(() =>
	        {
	            sfxSrc.Stop();
	            //sfx.volume = DataController.Instance.settings.volume;
	        });            
	}

	public void PlayBGM(string name, bool isLooping)
	{
	    //Debug.Log("<color=red>Inside Sound Manager!!!</color>");
	    AudioClip clip;
	    dictBGM.TryGetValue(name.ToLower(), out clip);
	    bgmSrc.clip = clip;
	    bgmSrc.loop = isLooping;
	    bgmSrc.Play();
	}

//	public void PlayOutsideBGM(string name, bool isLooping)
//	{
//	    AudioClip clip;
//	    dictBGM.TryGetValue(name.ToLower(), out clip);
//	    bgmOutside.clip = clip;
//	    bgmOutside.loop = isLooping;
//	    bgmOutside.Play();
//	}

	public bool isBGMPlaying(string clipName)
	{            
	    if(bgmSrc.clip == null)
	    {
	        return false;
	    }
	    else if(bgmSrc.clip.name.ToLower() == clipName.ToLower())
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
#endregion
}

