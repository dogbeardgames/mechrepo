﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    public void OnEnable()
    {
        SoundManager.Instance.PlaySFX("seTap");
    }

    public void Continue()
    {
        SoundManager.Instance.BGM.Play();
        SoundManager.Instance.PlaySFX("seTap");
        GameManager.paused = false;
        ModeUI.ins.startMode = true;
        gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        SoundManager.Instance.PlaySFX("seTap");
        MessageSystem.ins.Show("Message","Back to main menu?", true,
            delegate { ShowMenu(); },
            delegate { SoundManager.Instance.PlaySFX("seTap"); });
    }

    public void ShowMenu()
    {
        SoundManager.Instance.PlayBGM("bgmStandard", true);
        SoundManager.Instance.PlaySFX("seTap");
        GameManager.gameOver = false;
        GameManager.paused = false;
        Tile.move = false;
        MechTapTileManager.ins.GetSpawner().Reset();
        SoundManager.Instance.BGM.Stop();
        //UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        gameObject.SetActive(false);
        LoadingProgress.ins.LoadOperation(UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Menu"));
    }

    public void Retry()
    {
        
        SoundManager.Instance.PlaySFX("seTap");
        MechTapTileManager.ins.Reset();
        MechTapTileManager.ins.GetSpawner().Reset();
        //PlayerGameModeData.ResetGameModeData();
        GameManager.gameOver = false;
        GameManager.paused = false;
        ModeUI.ins.startMode = true;
        if (GameManager.mode == GAME_MODE.QUICK)
        {
            ModeQuickGameManager.instance.StartQuickGameMode();
            SoundManager.Instance.BGM.Stop();
            SoundManager.Instance.PlayBGM("bgmQuick", true);
        }
        else
        {
            ModeEndlessGameManager.instance.StartEndlessGameMode();
            SoundManager.Instance.BGM.Stop();
            SoundManager.Instance.PlayBGM("bgmEndless", true);
        }
    }
}
