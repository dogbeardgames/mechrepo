﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
    public static Manager ins;

    [SerializeField]
    GameObject tapManager;

    void Start()
    {
        if (ins == null)
        {
            ins = this;
            Instantiate(tapManager, transform);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	
}
