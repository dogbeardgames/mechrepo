﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ScriptableObjectGenerator : MonoBehaviour {

    [MenuItem("Assets/Create/Mech database")]
	public static void CreateMechDB()
    {
        MechScriptable objects = ScriptableObject.CreateInstance<MechScriptable>();

        AssetDatabase.CreateAsset(objects, "Assets/scripts/ScriptableObjects/SOCollections/MechDB.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = objects;
    }
}
